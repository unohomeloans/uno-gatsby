# Installation

```sh
git clone git clone https://gatsby-sanity@bitbucket.org/unohomeloans/uno-gatsby.git
cd uno-gatsby
npm install or yarn

# Install or upgrade the Sanity CLI to
# make sure you are on v0.140.0 or higher
npm install -g @sanity/cli or yarn global add @sanity/cli

```

## Getting started with development
Ensure that you have `.env.development`, `.env.production` and `.env.uat` files inside the `web/` directory.

Ensure that you have `.env.development`, `.env.production` and `.env.staging` files inside the `studio/` directory 

Please contact [John Antonios](mailto:john.antonios@unohomeloans.com.au) for these.

### Namings
There is currently an interesting naming convention:

- Development dataset: "dev"
- QA / UAT dataset: "production"
- Production dataset: "staging"

### Running workflow:

Option 1 - run the `dev.sh` script.
Do you want to enable a GraphQL playground? - n.

Option 2 - manual:

Update the "dev" dataset
```
cd studio
sanity graphql deploy --dataset dev
Do you want to enable a GraphQL playground? - n.
```
Run the dev script:
```
cd ..
yarn dev
```

Three URLs will now be available:
http://localhost:3333/ - Sanity studio
http://localhost:8000 - Gatsby webiste
http://localhost:8000/__graphql - Gatsby GraphQL IDE.

## ToDo

update the build script with TOKEN authentication, e.g.

``
"build-web": "lerna bootstrap && (cd studio && SANITY_AUTH_TOKEN=$SANITY_DEPLOY_STUDIO_TOKEN npm run graphql-deploy) && (cd web && npm run build)",
``

## Enable Gatsby watch mode for drafts

Default for now
We have enabled the watch mode in the `gatsby-source-sanity` plugin, which means that your frontend will automatically update with content changes whenever you publish them. If you want the frontend to show content changes in real time, you must do the following:

* Go to [manage.sanity.io](https://manage.sanity.io) and find your project (or run the command `sanity manage` in the studio folder)
* Navigate to Settings->API and scroll down to the **Tokens** section
* Add a new token and give it **read** privileges.
* Copy the `.env-example` file to a file called `.env` in the `/web` folder
* Add your new token to the key: `SANITY_TOKEN="<token here>"`

If you restart the local development server, it will now show unpublished changes from the Studio. Note that the `.env` file is ignored by Git, because the token gives access to unpublished content in the API.

## Development setup

### Run it

```sh
npm start or yarn start
# Studio at http://localhost:3333
# Web frontend at http://localhost:8000
# GraphiQL explorer at http://localhost:8000/___graphql
```

## Development workflow

* The Sanity Studio keeps its schemas in `./studio/schemas`. We will hot reload the editor when you edit them so just start experimenting. [Read more about our schemas here](https://www.sanity.io/docs/content-studio/the-schema).
* We followed Gatsby conventions and [you can read all about them here](https://www.gatsbyjs.org/tutorial/).
* If you want Gatsby to not throw errors on missing fields for unpopulated data you need to redeploy the GraphQL API so we can generate schemas – `npm run graphql-deploy`

## Deployment

* WIP
