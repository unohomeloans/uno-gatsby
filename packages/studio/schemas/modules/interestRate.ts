export default {
  name: 'interestRate',
  title: 'Interest Rate',
  type: 'object',
  fields: [
    {
      name: 'title',
      title: 'Title',
      type: 'string',
      hidden: true,
    },
  ],
};
