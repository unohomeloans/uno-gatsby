export default {
  name: 'brokerCtas',
  type: 'object',
  title: 'Broker CTAs',
  fields: [
    {
      name: 'modalCta',
      type: 'string',
      title: 'Modal CTA',
    },
    {
      name: 'listCta',
      type: 'string',
      title: 'List CTA',
    },
  ],
};
