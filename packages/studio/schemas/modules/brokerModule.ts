export default {
  title: 'Broker Content',
  name: 'brokerModule',
  type: 'object',
  hidden: false,
  fieldsets: [
    {
      name: 'bio',
      title: 'Bio',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
    {
      name: 'image',
      title: `Broker's Image`,
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
    {
      name: 'review',
      title: `Customer's Review`,
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
    {
      name: 'ctas',
      title: `Broker CTAs`,
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
    {
      name: 'links',
      title: `Broker Links`,
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
  ],
  fields: [
    {
      title: 'Name',
      name: 'name',
      type: 'string',
    },
    {
      name: 'slug',
      title: 'Slug',
      type: 'slug',
      options: {
        source: 'content.main.name',
        maxLength: 96,
      },
      validation: (Rule: any): any => Rule.required(),
    },
    {
      title: 'Email',
      name: 'email',
      type: 'string',
    },
    {
      title: 'Phone Number',
      name: 'tel',
      type: 'string',
    },
    {
      title: 'Location',
      name: 'location',
      type: 'string',
    },
    {
      name: 'bio',
      type: 'simpleBlockContent',
      fieldset: 'bio',
    },
    {
      name: 'image',
      type: 'mainImage',
      fieldset: 'image',
    },
    {
      name: 'review',
      type: 'customerReview',
      fieldset: 'review',
    },
    {
      name: 'ctas',
      type: 'brokerCtas',
      fieldset: 'ctas',
    },
    {
      name: 'links',
      type: 'brokerLinks',
      fieldset: 'links',
    },
  ],
};
