export default {
  name: 'comparisonRate',
  title: 'Comparison Rate',
  type: 'object',
  fields: [
    {
      name: 'title',
      title: 'Title',
      type: 'string',
      hidden: true,
    },
  ],
};
