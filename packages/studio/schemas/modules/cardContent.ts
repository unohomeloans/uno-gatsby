import { PrepareAlignmentArgs, PrepareReturn } from '../../typings/Prepare';

export default {
  type: 'object',
  name: 'cardContent',
  title: 'Card Contentt',
  description: 'Yes we always need a description.',
  fields: [
    {
      name: 'alignment',
      type: 'cardAlignment',
    },
    {
      title: 'Text',
      name: 'text',
      type: 'simpleBlockContent',
    },
  ],
  preview: {
    select: {
      title: 'title',
      alignment: 'alignment',
    },
    prepare({ alignment }: PrepareAlignmentArgs): PrepareReturn {
      return {
        title: `Card content - ${alignment} aligned`,
      };
    },
  },
};
