export default {
  title: 'Module Content',
  name: 'moduleContent',
  type: 'array',
  of: [
    {
      title: 'Hero',
      name: 'hero',
      type: 'hero',
    },
    {
      title: '3 columns card',
      name: 'threeColumnsCard',
      type: 'threeColumnsCard',
    },
    {
      title: '3 columns basic',
      name: 'threeColumnsBasic',
      type: 'threeColumnsBasic',
    },
    {
      title: '2 columns basic',
      name: 'twoColumnsBasic',
      type: 'twoColumnsBasic',
    },
    {
      title: 'Alert',
      name: 'alert',
      type: 'alert',
    },
    {
      title: 'Disclaimer',
      name: 'disclaimer',
      type: 'disclaimer',
    },
    {
      title: 'Article',
      name: 'article',
      type: 'article',
    },
    {
      title: 'Customer Feedback',
      name: 'customerFeedback',
      type: 'customerFeedback',
    },
    {
      title: 'Rates and Lenders',
      name: 'ratesLenders',
      type: 'ratesLenders',
    },
    {
      title: 'Add Table',
      name: 'oldTable',
      type: 'oldTable',
    },
    {
      title: 'Best Home Loan Rates',
      name: 'bestRates',
      type: 'bestRates',
    },
    {
      title: 'Brokers List',
      name: 'brokersList',
      type: 'brokersList',
    },
    {
      title: 'House Prices',
      name: 'housePrices',
      type: 'housePrices',
    },
    {
      title: 'Calculators',
      name: 'calculator',
      type: 'calculator',
    },
  ],
};
