export default {
  title: 'Page Content',
  name: 'pageModule',
  type: 'object',
  hidden: false,
  fieldsets: [
    {
      name: 'modules',
      title: 'Page Builder',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
  ],
  fields: [
    {
      name: 'title',
      title: 'Title',
      type: 'string',
    },
    {
      name: 'slug',
      title: 'Slug',
      type: 'slug',
      options: {
        source: 'content.main.title',
        maxLength: 96,
      },
      validation: (Rule: any): any => Rule.required(),
    },
    {
      name: 'modules',
      title: 'Modules',
      description: 'Add, edit, and reorder sections',
      type: 'moduleContent',
      fieldset: 'modules',
    },
  ],
};
