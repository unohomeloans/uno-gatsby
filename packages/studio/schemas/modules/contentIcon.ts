export default {
  title: 'Content Icon',
  name: 'contentIcon',
  type: 'image',
  fields: [
    {
      name: 'caption',
      title: 'Image Caption',
      type: 'string',
    },
    {
      name: 'alt',
      type: 'string',
      title: 'Alternative text',
      description: 'Important for SEO and accessibility.',
      validation: (Rule: any): any =>
        Rule.error('You have to fill out the alternative text.').required(),
    },
  ],
  preview: {
    select: {
      imageUrl: 'asset.url',
      title: 'caption',
    },
  },
};
