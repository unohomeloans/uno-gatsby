export default {
  title: 'Customer Review',
  name: 'customerReview',
  type: 'object',
  fields: [
    {
      name: 'title',
      title: 'Review Title',
      type: 'string',
    },
    {
      name: 'content',
      title: 'Review Body',
      type: 'simpleBlockContent',
    },
  ],
};
