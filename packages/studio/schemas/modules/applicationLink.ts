export default {
  name: 'applicationLink',
  title: 'applicationLink',
  type: 'object',
  hidden: false,
  fields: [
    {
      name: 'title',
      title: 'Title',
      type: 'string',
    },
    {
      name: 'select',
      title: 'Application',
      type: 'string',
      options: {
        list: [
          { title: 'CV2', value: 'CV2' },
          { title: 'LS', value: 'LS' },
        ],
        layout: 'radio',
      },
    },
    {
      name: 'append',
      title: 'Append',
      type: 'string',
      description:
        'Add route if require, e.g. existing-loan, * no need the "/"',
    },
  ],
};
