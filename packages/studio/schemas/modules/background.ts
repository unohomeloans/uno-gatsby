export default {
  name: 'chooseBackground',
  title: 'Options',
  type: 'string',
  options: {
    layout: 'radio',
    list: ['default', 'white', 'accent', 'image'],
  },
};
