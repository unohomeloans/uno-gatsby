export default {
  title: 'Content Image Full-Width',
  name: 'contentImageFull',
  type: 'image',
  options: {
    hotspot: true,
  },
  fields: [
    {
      name: 'caption',
      title: 'Image Caption',
      type: 'string',
    },
    {
      name: 'alt',
      type: 'string',
      title: 'Alternative text',
      description: 'Important for SEO and accessibility.',
      validation: (Rule: any): any =>
        Rule.error('You have to fill out the alternative text.').required(),
    },
    {
      name: 'embeddedText',
      title: 'Embed Text',
      type: 'embeddedText',
    },
  ],
  preview: {
    select: {
      imageUrl: 'asset.url',
      title: 'caption',
    },
  },
};
