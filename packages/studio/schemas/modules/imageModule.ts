export default {
  title: 'Image Module',
  name: 'imageModule',
  type: 'image',
  options: {
    hotspot: true,
  },
  fields: [
    {
      name: 'caption',
      title: 'Image Caption',
      type: 'string',
    },
    {
      name: 'alt',
      type: 'string',
      title: 'Alternative text',
      description: 'Important for SEO and accessibility.',
      validation: (Rule: any): any =>
        Rule.error('You have to fill out the alternative text.').required(),
    },
    {
      name: 'layout',
      title: 'Layout',
      type: 'string',
      of: [{ type: 'string' }],
      options: {
        list: [
          { title: 'Default', value: 'default' },
          { title: 'Full', value: 'full' },
        ],
        layout: 'dropdown',
      },
    },
  ],
};
