import {
  PrepareCTACollectionArgs,
  PrepareReturnCTACollection,
} from '../../typings/Prepare';

export default {
  type: 'object',
  name: 'ctaCollectionPlug',
  title: 'Call To Action Collection',
  description: 'A plug that shows call to actions',
  fields: [
    {
      title: 'Call to action collection',
      name: 'ctaCollection',
      type: 'array',
      of: [{ type: 'ctaItem' }],
    },
  ],
  preview: {
    select: {
      ctas: 'ctaCollection',
    },
    prepare({ ctas }: PrepareCTACollectionArgs): PrepareReturnCTACollection {
      return {
        title: 'CTAs',
        subtitle: ctas && ctas.ctas.map((cta: any) => cta.title).join(', '),
      };
    },
  },
};
