import { PrepareAlignmentArgs, PrepareReturn } from '../../typings/Prepare';

export default {
  type: 'object',
  name: 'simpleContent',
  title: 'Simple Contentt',
  description: 'Yes we always need a description.',
  fields: [
    {
      name: 'alignment',
      type: 'cardAlignment',
    },
    {
      title: 'Text',
      name: 'text',
      type: 'simpleBlockContent',
    },
  ],
  preview: {
    select: {
      title: 'title',
      alignment: 'alignment',
    },
    prepare({ alignment }: PrepareAlignmentArgs): PrepareReturn {
      return {
        title: `Simple content - ${alignment} aligned`,
      };
    },
  },
};
