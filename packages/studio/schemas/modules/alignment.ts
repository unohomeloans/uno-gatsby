export default {
  title: 'Options',
  name: 'alignment',
  type: 'string',
  options: {
    layout: 'radio',
    list: ['center', 'left', 'right'],
  },
};
