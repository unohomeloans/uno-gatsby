export default {
  name: 'brokerLinks',
  type: 'object',
  title: 'Broker Links',
  fields: [
    {
      name: 'referrer',
      type: 'string',
      title: 'Referrer',
    },
    {
      name: 'calendly',
      type: 'string',
      title: 'Calendly',
    },
  ],
};
