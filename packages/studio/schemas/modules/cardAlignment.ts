export default {
  title: 'Options',
  name: 'cardAlignment',
  type: 'string',
  options: {
    layout: 'radio',
    list: ['center', 'left'],
  },
};
