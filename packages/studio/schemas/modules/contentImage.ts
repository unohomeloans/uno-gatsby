const COLOR = [
  { title: 'primary', value: 'primary' },
  { title: 'secondary', value: 'secondary' },
];

export default {
  title: 'Content Image',
  name: 'contentImage',
  type: 'image',
  options: {
    hotspot: true,
  },
  fields: [
    {
      name: 'caption',
      title: 'Image Caption',
      type: 'string',
    },
    {
      name: 'alt',
      type: 'string',
      title: 'Alternative text',
      description: 'Important for SEO and accessibility.',
      validation: (Rule: any): any =>
        Rule.error('You have to fill out the alternative text.').required(),
    },
    {
      name: 'embeddedText',
      title: 'Embed Text',
      type: 'embeddedText',
    },
    {
      name: 'roundedImageAccent',
      title: 'Rounded Image with Accent Background',
      type: 'boolean',
    },
  ],
  preview: {
    select: {
      imageUrl: 'asset.url',
      title: 'caption',
    },
  },
};
