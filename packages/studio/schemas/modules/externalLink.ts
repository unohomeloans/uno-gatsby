export default {
  title: 'External Link',
  name: 'externalLink',
  type: 'object',
  hidden: false,
  fields: [
    {
      name: 'title',
      title: 'Link CTA',
      type: 'string',
    },
    {
      name: 'link',
      title: 'Link',
      type: 'string',
      description:
        'There is no `link` validation on this so please type accurate urls with https://, mailto:, tel: etc.',
    },
    {
      title: 'Open in new tab',
      name: 'blank',
      description:
        'Only works with the Kind == Link selected, more infor about link target read https://css-tricks.com/use-target_blank/, ',
      type: 'boolean',
    },
  ],
};
