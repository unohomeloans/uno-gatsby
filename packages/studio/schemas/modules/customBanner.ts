const TYPES = [{ title: "testimonial", value: "testimonial" }];

export default {
  type: "object",
  name: "customBanner",
  title: "Custom Banner",
  description: "A simple banner with layout options",
  fields: [
    {
      name: "type",
      title: "Banner Type",
      type: "string",
      options: { list: TYPES },
    },
    {
      name: "testimonial",
      type: "testimonial",
      title: "Testimonail",
      hidden: ({ document }: any) => document?.type == "testimonial",
    },
  ],
  initialValue: { type: "testimonial" },
};
