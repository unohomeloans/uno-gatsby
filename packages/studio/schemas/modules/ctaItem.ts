import { PrepareReturn } from '../../typings/Prepare';

export default {
  title: 'CTA Item',
  name: 'ctaItem',
  type: 'object',
  fieldsets: [
    {
      name: 'ctaIcon',
      title: 'Choose Icon',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
    {
      name: 'internalLinkGroup',
      title: 'Internal Link',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
    {
      name: 'externalLinkGroup',
      title: 'External Link',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
    {
      title: 'Application Link',
      name: 'applicationLink',
      description: 'This will handle qa/prod links to our applications',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
  ],
  fields: [
    {
      name: 'ctaIcon',
      title: 'CTA Icon',
      type: 'ctaIcon',
      fieldset: 'ctaIcon',
    },
    {
      name: 'internalLink',
      title: 'Internal Link',
      type: 'internalLink',
      fieldset: 'internalLinkGroup',
    },
    {
      name: 'externalLink',
      title: 'External Link',
      type: 'externalLink',
      fieldset: 'externalLinkGroup',
    },
    {
      title: 'Link',
      name: 'applink',
      type: 'applicationLink',
      fieldset: 'applicationLink',
    },
  ],
  preview: {
    prepare(): PrepareReturn {
      return {
        title: 'CTA Item',
      };
    },
  },
};
