import PreviewYouTube from '../../components/Preview/PreviewYoutube';
export default {
  name: 'youtube',
  type: 'object',
  title: 'YouTube Embed',
  fields: [
    {
      name: 'url',
      type: 'url',
      title: 'YouTube video URL',
    },
  ],
  components: { preview: PreviewYouTube },
  preview: {
    select: {
      url: 'url',
    },
  },
};
