import blockContent from './objects/blockContent';
import category from './objects/category';
import post from './objects/post';
import author from './objects/author';
import logo from './objects/logo';
import simpleBlockContent from './objects/simpleBlockContent';
import cta from './objects/cta';
import ctaIcon from './objects/ctaIcon';
import links from './objects/links';
import list from './objects/list';
import listContent from './objects/listContent';
import embeddedText from './objects/embeddedText';
import brokerCtas from './modules/brokerCtas';
import brokerLinks from './modules/brokerLinks';
import calendlyEmbed from './objects/calendlyEmbed';
import calendlyWidget from './objects/calendlyWidget';
import testimonial from './objects/testimonial';
import podcast from './objects/podcast';
import menuExtraText from './types/menu-extra-text';
import menuExtraTextDetails from './types/menu-extra-text-details';

import siteSettings from './documents/siteSettings';

import page from './types/page';
import menus from './types/menus';
import menuItems from './types/menu-items';
import menuItemsDetails from './types/menu-items-details';
import submenus from './types/submenus';
import submenusItems from './types/submenu-items';
import route from './types/route';
import lenders from './types/lenders';
import redirects from './types/redirects';
import broker from './types/broker';

import metaCard from './modules/metaCard';
import pageModule from './modules/pageModule';
import pageItem from './modules/pageItem';
import ctaItem from './modules/ctaItem';
import nestedPages from './modules/nestedPages';
import moduleContent from './modules/moduleContent';
import externalLink from './modules/externalLink';
import internallLink from './modules/internalLink';
import applicationLink from './modules/applicationLink';
import imageModule from './modules/imageModule';
import youtube from './modules/youtube';
import landingImageModule from './modules/landingImageModule';
import contentImage from './modules/contentImage';
import contentImageFull from './modules/contentImageFull';
import contentIcon from './modules/contentIcon';
import ctaCollection from './modules/ctaCollection';
import textColor from './modules/textColor';
import background from './modules/background';
import cardContent from './modules/cardContent';
import cardAligment from './modules/cardAlignment';
import alignment from './modules/alignment';
import comparisonRate from './modules/comparisonRate';
import interestRate from './modules/interestRate';
import brokerModule from './modules/brokerModule';
import reviewModule from './modules/reviewModule';
import customBanner from './modules/customBanner';

import twoColumnsBasic from './page-builder/2-columns-basic';
import threeColumnsBasic from './page-builder/3-columns-basic';
import threeColumnsCard from './page-builder/3-columns-card';
import alert from './page-builder/alert';
import bestRates from './page-builder/best-rates';
import customerFeedback from './page-builder/customer-feedback';
import disclaimer from './page-builder/disclaimer';
import article from './page-builder/article';
import hero from './page-builder/hero';
import ratesLenders from './page-builder/rates-lenders';
import oldTable from './page-builder/oldTable';
import brokersList from './page-builder/brokers-list';
import housePrices from './page-builder/house-prices';
import calculator from './page-builder/calculator';
import brokerModal from './objects/brokerModal';

import pageContent from './tabs/pageContent';
import brokerContent from './tabs/brokerContent';
import titleBlockContent from './objects/titleBlockContent';

export const schemaTypes = [
  page,
  menus,
  menuItems,
  menuItemsDetails,
  submenus,
  submenusItems,
  route,
  lenders,
  redirects,
  broker,
  brokerCtas,
  brokersList,
  brokerModule,
  brokerContent,
  brokerLinks,
  calendlyEmbed,
  calendlyWidget,
  testimonial,
  podcast,
  menuExtraText,
  menuExtraTextDetails,
  housePrices,
  calculator,
  brokerModal,
  reviewModule,
  customBanner,
  post,
  author,
  category,
  pageModule,
  pageContent,
  pageItem,
  ctaItem,
  moduleContent,
  externalLink,
  internallLink,
  applicationLink,
  imageModule,
  youtube,
  landingImageModule,
  contentImage,
  contentImageFull,
  contentIcon,
  nestedPages,
  hero,
  threeColumnsCard,
  threeColumnsBasic,
  twoColumnsBasic,
  disclaimer,
  article,
  alert,
  customerFeedback,
  ratesLenders,
  oldTable,
  cardContent,
  cardAligment,
  logo,
  simpleBlockContent,
  titleBlockContent,
  cta,
  ctaIcon,
  links,
  list,
  listContent,
  embeddedText,
  textColor,
  ctaCollection,
  background,
  alignment,
  comparisonRate,
  interestRate,
  bestRates,
  metaCard,
  blockContent,
  siteSettings,
];
