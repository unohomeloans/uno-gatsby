export default {
  name: 'siteSettings',
  type: 'document',
  title: 'Site Settings',
  fieldsets: [
    {
      name: 'title',
      title: 'Title',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
    {
      name: 'description',
      title: 'Description',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
    {
      name: 'logo',
      title: 'Brand Logo',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
    {
      name: 'keywords',
      title: 'Keywords',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
    {
      name: 'author',
      title: 'Author',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
    {
      name: 'rates',
      title: 'Rates',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
  ],
  __experimental_actions: ['update', 'publish'],
  fields: [
    {
      name: 'title',
      type: 'string',
      title: 'Title',
      fieldset: 'title',
    },
    {
      name: 'description',
      type: 'text',
      title: 'Description',
      description: 'Describe your site for search engines and social media.',
      fieldset: 'description',
    },
    {
      title: 'Brand logo',
      description:
        'Best choice is to use an SVG where the color are set with currentColor',
      name: 'logo',
      type: 'logo',
      fieldset: 'logo',
    },
    {
      name: 'keywords',
      type: 'array',
      title: 'Keywords',
      description: 'Add keywords that describes your site.',
      of: [{ type: 'string' }],
      options: {
        layout: 'tags',
      },
      fieldset: 'keywords',
    },
    {
      name: 'author',
      type: 'reference',
      description: 'Publish an author and set a reference to them here.',
      title: 'Author',
      to: [{ type: 'author' }],
      fieldset: 'author',
    },
    {
      name: 'interestRate',
      type: 'string',
      fieldset: 'rates',
    },
    {
      name: 'comparisonRate',
      type: 'string',
      title: 'Comparison rate',
      fieldset: 'rates',
    },
    {
      name: 'disclaimer',
      type: 'simpleBlockContent',
      title: 'Disclaimer',
      fieldset: 'rates',
    },
  ],
};
