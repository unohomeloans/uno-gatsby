// import Tabs from 'sanity-plugin-tabs';

export default {
  name: 'brokerContent',
  title: 'Broker Content',
  type: 'object',
  // inputComponent: Tabs,
  fieldsets: [
    { name: 'main', title: 'Main' },
    { name: 'defaultMeta', title: 'Meta' },
  ],
  fields: [
    {
      type: 'brokerModule',
      name: 'main',
      fieldset: 'main',
    },
    {
      type: 'metaCard',
      name: 'meta',
      fieldset: 'defaultMeta',
    },
  ],
};
