import { PrepareReturn } from '../../typings/Prepare';

export default {
  type: 'object',
  name: 'brokersList',
  title: 'Brokers List',
  fieldsets: [
    {
      name: 'title',
      title: 'Title',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
  ],
  fields: [
    {
      name: 'title',
      type: 'string',
      title: 'Title',
      fieldset: 'title',
    },
  ],
  initialValue: { title: '' },
  preview: {
    select: {
      title: 'title',
    },
    prepare(): PrepareReturn {
      return {
        title: `Brokers List`,
      };
    },
  },
};
