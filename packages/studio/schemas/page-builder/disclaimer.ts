import { PrepareAlignmentArgs, PrepareReturn } from '../../typings/Prepare';

export default {
  type: 'object',
  name: 'disclaimer',
  title: 'Disclaimer',
  description: 'Yeahhhhh disclaimer',
  fields: [
    {
      name: 'alignment',
      type: 'cardAlignment',
    },
    {
      title: 'Text',
      name: 'text',
      type: 'simpleBlockContent',
    },
  ],
  preview: {
    select: {
      alignment: 'alignment',
    },
    prepare({ alignment }: PrepareAlignmentArgs): PrepareReturn {
      return {
        title: `Disclaimer content: ${alignment} aligned`,
      };
    },
  },
};
