import { PrepareTitleDisabledArgs, PrepareReturn } from '../../typings/Prepare';
import { prepareTitle } from '../../utils/prepareObject';

export default {
  type: 'object',
  name: 'threeColumnsCard',
  title: '3 Columns Card',
  fieldsets: [
    {
      name: 'background',
      title: 'Background',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
    {
      name: 'title',
      title: 'Title',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
    {
      name: 'content',
      title: 'Content',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
  ],
  fields: [
    {
      name: 'chooseBackground',
      type: 'chooseBackground',
      fieldset: 'background',
    },
    {
      name: 'title',
      type: 'string',
      title: 'Title',
      fieldset: 'title',
    },
    {
      name: 'cardContent',
      title: 'Items',
      type: 'array',
      of: [{ type: 'cardContent' }],
      fieldset: 'content',
    },
  ],
  preview: {
    select: {
      title: 'title',
    },
    prepare(selection: PrepareTitleDisabledArgs): PrepareReturn {
      return prepareTitle(selection, '3 Columns card');
    },
  },
};
