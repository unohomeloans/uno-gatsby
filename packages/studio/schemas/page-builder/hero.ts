import { PrepareTitleDisabledArgs, PrepareReturn } from '../../typings/Prepare';
import { prepareTitle } from '../../utils/prepareObject';

export default {
  type: 'object',
  name: 'hero',
  title: 'Hero',
  fieldsets: [
    {
      name: 'alignment',
      title: 'Aligment',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
    {
      name: 'blockTitle',
      title: 'Title',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
    {
      name: 'embeddedTitle',
      title: 'Embedded Title',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
    {
      name: 'content',
      title: 'Content',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
    {
      name: 'background',
      title: 'Background',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
    {
      name: 'backgroundImage',
      title: 'Background Image',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
    {
      name: 'image',
      title: 'Image',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
    {
      name: 'youtubeVideo',
      title: 'Video',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
    {
      name: 'mainList',
      title: 'List',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
    {
      name: 'cta',
      title: 'CTAs',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
    {
      name: 'disclaimer',
      title: 'Disclamers',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
  ],
  fields: [
    {
      name: 'alignment',
      type: 'alignment',
      fieldset: 'alignment',
    },
    {
      name: 'chooseBackground',
      type: 'chooseBackground',
      fieldset: 'background',
    },
    {
      name: 'backgroundImage',
      title: 'Image',
      type: 'mainImage',
      fieldset: 'backgroundImage',
    },
    {
      name: 'title',
      title: 'Title',
      type: `titleBlockContent`,
      fieldset: 'blockTitle',
    },
    {
      name: 'embeddedTitle',
      title: 'Title',
      type: `titleBlockContent`,
      fieldset: 'embeddedTitle',
    },
    {
      name: 'text',
      title: 'Text',
      type: 'simpleBlockContent',
      fieldset: 'content',
    },
    {
      name: 'mainImage',
      title: 'Image',
      type: 'mainImage',
      fieldset: 'image',
    },
    {
      name: 'mainVideo',
      title: 'Main YouTube Video',
      type: 'youtube',
      fieldset: 'youtubeVideo',
    },
    {
      name: 'mainList',
      title: 'Main List',
      type: 'list',
      fieldset: 'mainList',
    },
    {
      name: 'ctas',
      type: 'array',
      of: [
        {
          name: 'cta',
          type: 'cta',
        },
      ],
      fieldset: 'cta',
    },
    {
      name: 'disclaimer',
      title: 'Disclaimer',
      type: 'simpleBlockContent',
      fieldset: 'disclaimer',
    },
  ],
  preview: {
    select: {
      alignment: 'alignment',
      imageUrl: 'asset.url',
      title: 'alignment',
      disabled: 'disabled',
    },
    prepare(selection: PrepareTitleDisabledArgs): PrepareReturn {
      return prepareTitle(selection, 'Hero');
    },
  },
};
