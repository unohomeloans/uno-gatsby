import { PrepareReturn } from '../../typings/Prepare';

export default {
  type: 'object',
  name: 'ratesLenders',
  title: 'Rates & Lenders',
  description: 'Section dedicated for rates and lenders.',
  fields: [
    {
      title: 'Title',
      name: 'title',
      type: 'titleBlockContent',
    },
  ],
  preview: {
    select: {
      title: 'title',
    },
    prepare(): PrepareReturn {
      return {
        title: `Rates & Lenders`,
      };
    },
  },
};
