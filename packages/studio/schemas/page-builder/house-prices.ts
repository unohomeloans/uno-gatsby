import { PrepareReturn } from '../../typings/Prepare';

export default {
  type: 'object',
  name: 'housePrices',
  title: 'House Prices',
  description: `A search tool for house prices based on Australia's different states`,
  fields: [
    {
      title: 'Title',
      name: 'title',
      type: 'string',
      readOnly: true,
      initialValue: 'House Prices',
    },
  ],
  preview: {
    select: {
      title: 'title',
    },
    prepare(): PrepareReturn {
      return {
        title: `House Prices`,
      };
    },
  },
};
