import { PrepareAlignmentArgs, PrepareReturn } from '../../typings/Prepare';

export default {
  type: 'object',
  name: 'article',
  title: 'Article',
  description: 'Article content, mainly text content',
  fields: [
    {
      name: 'alignment',
      type: 'cardAlignment',
    },
    {
      title: 'Text',
      name: 'text',
      type: 'simpleBlockContent',
    },
  ],
  preview: {
    select: {
      alignment: 'alignment',
    },
    prepare({ alignment }: PrepareAlignmentArgs): PrepareReturn {
      return {
        title: `Article content: ${alignment} aligned`,
      };
    },
  },
};
