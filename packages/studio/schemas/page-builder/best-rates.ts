import { PrepareReturn } from '../../typings/Prepare';

export default {
  type: 'object',
  name: 'bestRates',
  title: 'Best home loan rates',
  fieldsets: [
    {
      name: 'title',
      title: 'Title',
      description: 'This is the content above the rates',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
    {
      name: 'content',
      title: 'Content',
      description: 'This is the content below the rates',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
  ],
  fields: [
    {
      title: 'Title',
      name: 'title',
      type: 'simpleBlockContent',
      fieldset: 'title',
    },
    {
      title: 'Content',
      name: 'content',
      type: 'simpleBlockContent',
      fieldset: 'content',
    },
  ],
  preview: {
    select: {
      title: 'title',
    },
    prepare(): PrepareReturn {
      return {
        title: `Best home loan rates`,
      };
    },
  },
};
