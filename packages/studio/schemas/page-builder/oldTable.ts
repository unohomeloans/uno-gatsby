import { PrepareReturn } from '../../typings/Prepare';

export default {
  type: 'object',
  name: 'oldTable',
  title: 'Old Table',
  description: 'Section dedicated for the uno is different table.',
  fields: [
    {
      title: 'Title',
      name: 'title',
      type: 'titleBlockContent',
    },
    {
      name: 'ctas',
      type: 'array',
      of: [
        {
          name: 'cta',
          type: 'cta',
        },
      ],
    },
  ],
  preview: {
    select: {
      title: 'title',
    },
    prepare(): PrepareReturn {
      return {
        title: `Old Table`,
      };
    },
  },
};
