import { PrepareReturn } from '../../typings/Prepare';

export default {
  type: 'object',
  name: 'alert',
  title: 'Alert',
  description: 'Yeahhhhh Alert',
  fieldsets: [
    {
      name: 'title',
      title: 'Title',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
    {
      name: 'content',
      title: 'Content',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
  ],
  fields: [
    {
      title: 'Title',
      name: 'title',
      type: 'titleBlockContent',
      fieldset: 'title',
    },
    {
      title: 'Content',
      name: 'content',
      type: 'simpleBlockContent',
      fieldset: 'content',
    },
  ],
  preview: {
    prepare(): PrepareReturn {
      return {
        title: 'Alert',
      };
    },
  },
};
