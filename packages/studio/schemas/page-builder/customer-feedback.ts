import { PrepareReturn } from '../../typings/Prepare';

export default {
  type: 'object',
  name: 'customerFeedback',
  title: 'Customer Feedback',
  description:
    'Section dedicated for the customers feedback from Product Review.',
  fields: [
    {
      title: 'Title',
      name: 'title',
      type: 'titleBlockContent',
    },
  ],
  preview: {
    select: {
      title: 'title',
    },
    prepare(): PrepareReturn {
      return {
        title: `Customer Feedback`,
      };
    },
  },
};
