import { PrepareReturn } from '@typings/Prepare';

const CALCULATOR_TYPES = [
  { title: 'Cumulative Interest', value: 'cumulativeInterest' },
  { title: 'Stamp Duty', value: 'stampDuty' },
];

export default {
  type: 'object',
  name: 'calculator',
  title: 'Calculator',
  description: 'UNO calculators',
  fields: [
    {
      title: 'Calculator Type',
      name: 'type',
      type: 'string',
      options: {
        list: CALCULATOR_TYPES,
      },
    },
    {
      name: 'rbaRate',
      type: 'number',
      title: 'Current RBA Rate',
      hidden: ({ document }: any): boolean =>
        !(document?.type == 'cumulativeInterest'),
    },
  ],
  preview: {
    select: {
      calculator: 'type',
    },
    prepare({ calculator }: any): PrepareReturn {
      const calculatorName = CALCULATOR_TYPES.flatMap((option) =>
        option.value === calculator ? [option.title] : []
      );
      return {
        title: calculatorName.length
          ? `${calculatorName[0]} - Calculator`
          : 'No Calculator Selected',
      };
    },
  },
};
