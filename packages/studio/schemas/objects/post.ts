import PostIcon from '../../components/Icons/PostIcon';

interface PrepareSlugArgs {
  title?: any;
  publishedAt?: any;
  slug?: any;
  media?: any;
}

export default {
  name: 'post',
  title: 'Posts',
  type: 'document',
  icon: PostIcon,
  fieldsets: [
    {
      name: 'seo',
      title: 'SEO meta tags',
    },
    {
      name: 'imageLanding',
      title: 'Image Landing Page',
    },
    {
      name: 'heroImage',
      title: 'Image Hero',
    },
  ],
  fields: [
    {
      name: 'featured',
      title: 'Top Story',
      type: 'boolean',
      description:
        '☝ this feature will move the post to the Top Stories section in the categories template e.g. newsroom.',
    },
    {
      name: 'title',
      title: 'Title',
      type: 'string',
    },
    {
      name: 'slug',
      title: 'Slug',
      type: 'slug',
      options: {
        source: 'title',
        maxLength: 96,
      },
    },
    {
      name: 'author',
      title: 'Author',
      type: 'reference',
      to: { type: 'author' },
    },
    {
      name: 'mainImage',
      title: 'Landing page image',
      description:
        '🚨Yes, an image needs to ne updload here to be showes in the landing page. e.g. /newsroom',
      type: 'mainImage',
      fieldset: 'imageLanding',
    },
    {
      name: 'heroImage',
      title: 'Hero image',
      type: 'imageModule',
      fieldset: 'heroImage',
    },
    {
      name: 'categories',
      title: 'Categories',
      type: 'array',
      of: [{ type: 'reference', to: { type: 'category' } }],
    },
    {
      name: 'publishedAt',
      title: 'Published at',
      type: 'datetime',
    },
    {
      name: 'excerpt',
      title: 'Excerpt',
      type: 'string',
      description:
        'This ends up on summary pages, on Google, when people share your post in social media.',
    },
    {
      name: 'body',
      title: 'Body',
      type: 'blockContent',
    },
    {
      name: 'seoTitle',
      title: 'Title',
      description:
        'This title will show in search results and social sharing, please notice if empty the default title will apply ',
      type: 'string',
      fieldset: 'seo',
      validation: (Rule: any) => Rule.max(70),
    },
    {
      name: 'seoDescription',
      title: 'Description',
      description:
        'This description will show in search results and social sharing, please notice if empty the text will come from the Site Settings. 🍪',
      type: 'text',
      fieldset: 'seo',
      rows: 3,
      validation: (Rule: any) => Rule.min(50).max(300),
    },
  ],
  orderings: [
    {
      name: 'publishingDateAsc',
      title: 'Publishing date new–>old',
      by: [
        {
          field: 'publishedAt',
          direction: 'asc',
        },
        {
          field: 'title',
          direction: 'asc',
        },
      ],
    },
    {
      name: 'publishingDateDesc',
      title: 'Publishing date old->new',
      by: [
        {
          field: 'publishedAt',
          direction: 'desc',
        },
        {
          field: 'title',
          direction: 'asc',
        },
      ],
    },
  ],
  preview: {
    select: {
      title: 'title',
      author: 'author.name',
      publishedAt: 'publishedAt',
      slug: 'slug',
      media: 'mainImage',
    },
    prepare({
      title = 'No title',
      publishedAt,
      slug = {},
      media,
    }: PrepareSlugArgs) {
      const path = slug.current;
      return {
        title,
        media,
        subtitle: publishedAt ? path : 'Missing publishing date',
      };
    },
  },
};
