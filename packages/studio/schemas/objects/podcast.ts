export default {
  title: "Podcast",
  name: "podcast",
  type: "object",
  fields: [
    {
      title: "Url",
      name: "url",
      type: "string",
    },
  ],
  preview: {
    prepare: () => ({ title: "Podcast" }),
  },
};
