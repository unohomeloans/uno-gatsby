const POSITIONS = [
  { title: 'left', value: 'left' },
  { title: 'right', value: 'right' },
  { title: 'top', value: 'top' },
  { title: 'bottom', value: 'bottom' },
  { title: 'top-left', value: 'topLeft' },
  { title: 'bottom-left', value: 'bottomLeft' },
  { title: 'top-right', value: 'topRight' },
  { title: 'bottom-right', value: 'bottomRight' },
];

export default {
  title: 'Embedded Text',
  name: 'embeddedText',
  type: 'object',
  fields: [
    {
      title: 'Text to be Embedded on Image',
      name: 'text',
      type: 'simpleBlockContent',
      validation: (Rule): any => Rule.required(),
    },
    {
      name: 'position',
      title: 'Position of Text on Image',
      type: 'string',
      options: { list: POSITIONS },
    },
  ],
  preview: {
    select: {
      title: 'text',
    },
  },
};
