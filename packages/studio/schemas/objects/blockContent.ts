import { MdLink, MdArrowUpward } from 'react-icons/md';
import { FaSubscript, FaSuperscript } from 'react-icons/fa';
import {
  TextOne,
  TextTwo,
  TextThree,
  TextFour,
  TextFive,
  TextSix,
  TextSeven,
  TextEight,
  TextNine,
  NormalText,
} from '../../components/BlockContentText/Text';
import {
  HeadingFour,
  HeadingOne,
  HeadingThree,
  HeadingTwo,
} from '../../components/BlockContentText/Heading';

export default {
  title: 'Block Content',
  name: 'blockContent',
  type: 'array',
  of: [
    {
      title: 'Block',
      type: 'block',
      styles: [
        {
          title: 'Normal',
          value: 'normal',
          component: NormalText,
        },
        {
          title: 'H1',
          value: 'h1',
          component: HeadingOne,
        },
        { title: 'H2', value: 'h2', component: HeadingTwo },
        { title: 'H3', value: 'h3', component: HeadingThree },
        { title: 'H4', value: 'h4', component: HeadingFour },
        { title: 'T1', value: 'txt-1', component: TextOne },
        { title: 'T2', value: 'txt-2', component: TextTwo },
        { title: 'T3', value: 'txt-3', component: TextThree },
        { title: 'T4', value: 'txt-4', component: TextFour },
        { title: 'T5', value: 'txt-5', component: TextFive },
        { title: 'T6', value: 'txt-6', component: TextSix },
        { title: 'T7', value: 'txt-7', component: TextSeven },
        { title: 'T8', value: 'txt-8', component: TextEight },
        { title: 'T9', value: 'txt-9', component: TextNine },
        { title: 'Quote', value: 'blockquote' },
        { title: 'Small', value: 'small' },
      ],
      lists: [
        { title: 'Bullet', value: 'bullet' },
        { title: 'Number', value: 'number' },
      ],
      marks: {
        decorators: [
          { title: 'Strong', value: 'strong' },
          { title: 'Emphasis', value: 'em' },
          {
            title: 'Sup',
            value: 'sup',
            component: FaSuperscript,
          },
          {
            title: 'Sub',
            value: 'sub',
            component: FaSubscript,
          },
        ],
        annotations: [
          {
            name: 'internalLink',
            type: 'object',
            title: 'Internal link',
            icon: MdLink,
            fields: [
              {
                name: 'reference',
                type: 'reference',
                title: 'Reference',
                to: [{ type: 'post' }, { type: 'page' }],
              },
            ],
          },
          {
            title: 'External Link',
            name: 'link',
            type: 'object',
            icon: MdArrowUpward,
            fields: [
              {
                title: 'URL',
                name: 'href',
                type: 'url',
              },
              {
                title: 'Open in new tab',
                name: 'blank',
                description: 'Read https://css-tricks.com/use-target_blank/',
                type: 'boolean',
              },
            ],
          },
        ],
      },
    },
    { title: 'Add Table', type: 'table' },
    { type: 'contentImage' },
    { type: 'contentImageFull' },
    { type: 'ctaItem' },
    { type: 'podcast' },
    { type: 'youtube' },
    { type: 'brokerModal' },
    { type: 'calendlyEmbed' },
    { type: 'list' },
    { type: 'calendlyWidget' },
  ],
};
