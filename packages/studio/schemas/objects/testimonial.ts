export default {
  title: "Testimonial",
  name: "testimonial",
  type: "object",
  fields: [
    {
      title: "Reviewer's Full Name",
      name: "name",
      type: "string",
    },
    {
      title: "Reviewer's Stars",
      name: "stars",
      type: "number",
      validation: (Rule: any) => Rule.required().min(0).max(5),
    },
    {
      title: "Review",
      name: "review",
      type: "text",
    },
    {
      name: "broker",
      type: "reference",
      title: "Choose Broker",
      to: [{ type: "broker" }],
    },
    {
      title: "Broker Title",
      name: "brokerTitle",
      type: "string",
    },
    {
      title: "Broker Content",
      name: "brokerCard",
      type: "simpleBlockContent",
    },
    {
      title: "Broker Button",
      name: "brokerButton",
      type: "string",
    },
  ],
};
