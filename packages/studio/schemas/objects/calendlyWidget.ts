import { FaRuler } from 'react-icons/fa';
import { PrepareReturn } from '../../typings/Prepare';

export default {
  type: 'object',
  name: 'calendlyWidget',
  title: 'Calendly Widget',
  description: `All Calendly Widgets`,
  fields: [
    {
      title: 'Type',
      name: 'type',
      type: 'string',
      readOnly: true,
    },
  ],
  initialValue: { type: 'inline' },
  preview: {
    prepare(): PrepareReturn {
      return {
        title: `Calendly Widget`,
      };
    },
  },
};
