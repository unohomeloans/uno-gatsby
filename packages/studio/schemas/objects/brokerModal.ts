import { PrepareReturn } from '../../typings/Prepare';

export default {
  type: 'object',
  name: 'brokerModal',
  title: 'Broker Modal',
  description: `A modal that gives you access to online application or a broker`,
  fields: [
    {
      title: 'Title',
      name: 'title',
      type: 'string',
    },
    {
      title: 'Button Specs',
      name: 'content',
      type: 'ctaIcon',
    },
  ],
  preview: {
    select: {
      title: 'title',
    },
    prepare(): PrepareReturn {
      return {
        title: `Broker Modal`,
      };
    },
  },
};
