import { FaRuler } from 'react-icons/fa';
import { PrepareReturn } from '../../typings/Prepare';

export default {
  type: 'object',
  name: 'calendlyEmbed',
  title: 'Calendly Button Embed',
  description: `This is the calendly embed button`,
  fieldsets: [
    {
      name: 'ctaIcon',
      title: 'Icon Specs',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
  ],
  fields: [
    {
      title: 'Button Name',
      name: 'name',
      type: 'string',
      validation: (Rule: any) => Rule.required(),
    },
    {
      title: 'Calendly Link',
      name: 'link',
      type: 'string',
      validation: (Rule: any) =>
        Rule.regex(/https\:\/\/(www\.)?calendly\.com/, {
          name: 'link',
          invert: false,
        }),
    },
    {
      title: 'Add Icon',
      name: 'ctaIcon',
      type: 'ctaIcon',
      fieldset: 'ctaIcon',
    },
  ],
  initialValue: {
    link: 'https://calendly.com/uno-customer-care/uno-quick-call',
  },
  preview: {
    select: {
      title: 'title',
    },
    prepare(): PrepareReturn {
      return {
        title: `Calendly Embed`,
      };
    },
  },
};
