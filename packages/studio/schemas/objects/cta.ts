import { prepareCTAAndLink } from '../../utils/prepareObject';

export default {
  title: 'Call to action',
  name: 'cta',
  type: 'object',
  fieldsets: [
    {
      name: 'ctaIcon',
      title: 'Choose Icon',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
    {
      title: 'Internal Link',
      name: 'internalLink',
      description:
        'For internal links e.g. pages, posts and categories, notice: only the first value of these will be used',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
    {
      title: 'External Link',
      name: 'externalLink',
      description:
        'For external links, notice: only the first value of these will be used',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
    {
      title: 'Application Link',
      name: 'applicationLink',
      description: 'This will handle qa/prod links to our applications',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
  ],
  fields: [
    {
      name: 'ctaIcon',
      title: 'CTA Icon',
      type: 'ctaIcon',
      fieldset: 'ctaIcon',
    },
    {
      title: 'Title',
      name: 'title',
      type: 'string',
      fieldset: 'internalLink',
    },
    {
      name: 'linkedPage',
      title: 'Linked Page',
      type: 'reference',
      to: [{ type: 'page' }, { type: 'post' }, { type: 'category' }],
      fieldset: 'internalLink',
    },
    {
      title: 'External link',
      name: 'link',
      type: 'externalLink',
      description: 'Example: https://loanscore.unohomeloans.com.au/',
      fieldset: 'externalLink',
    },
    {
      title: 'Link',
      name: 'applink',
      type: 'applicationLink',
      fieldset: 'applicationLink',
    },
    {
      title: 'Kind',
      description: "This won't work for Navigation Items",
      name: 'kind',
      type: 'string',
      options: {
        layout: 'radio',
        list: ['button', 'link'],
      },
    },
  ],
  preview: {
    select: {
      title: 'title',
      linkedPage: 'linkedPage',
      link: 'link',
      applink: 'applink',
    },
    prepare: prepareCTAAndLink,
  },
};
