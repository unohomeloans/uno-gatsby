import CategoryIcon from '../../components/Icons/CategoryIcon';

export default {
  name: 'category',
  title: 'Category',
  type: 'document',
  icon: CategoryIcon,
  fields: [
    {
      name: 'featuredPosts',
      title: 'Enable Top Stories',
      type: 'boolean',
      description:
        '☝ Enable this to show Top Stories at top of the single category pages. Default is "False".',
    },
    {
      name: 'title',
      title: 'Title',
      type: 'string',
    },
    {
      name: 'slug',
      type: 'slug',
      title: 'Slug',
      validation: (Rule: any): any =>
        Rule.error('You have to generate out the slug.').required(),
      options: {
        // add a button to generate slug from the title field
        source: 'title',
      },
    },
    {
      name: 'categoryImage',
      title: 'Image',
      description: '📷 Add image here',
      type: 'image',
      options: {
        hotspot: true,
      },
    },
    {
      name: 'description',
      title: 'Description',
      type: 'text',
    },
  ],
};
