import {
  ArrowTopRight,
  Heart,
  Person,
  Trophy,
} from '../../components/IconsList/IconsList';

// const AVAILABLE_ICONS = {
//   arrowTopRight: { name: 'arrow-top-right', icon: ArrowTopRight },
//   trophy: { name: 'trophy', icon: Trophy },
//   person: { name: 'person', icon: Person },
//   heart: { name: 'heart', icon: Heart },
// };

const AVAILABLE_ICONS = [
  { title: 'arrow-top-right', value: 'arrowTopRight', icon: ArrowTopRight },
  { title: 'trophy', value: 'trophy', icon: Trophy },
  { title: 'person', value: 'person', icon: Person },
  { title: 'heart', value: 'heart', icon: Heart },
];

const ICON_TYPES = [
  { title: 'primary', value: 'primary' },
  { title: 'alternate', value: 'alternate' },
];

const ICON_POSITIONS = [
  { title: 'prefix', value: 'prefix' },
  { title: 'suffix', value: 'suffix' },
];

export default {
  title: 'CTA Icon',
  name: 'ctaIcon',
  type: 'object',
  fieldsets: [
    {
      name: 'type',
      title: 'Type',
    },
    {
      name: 'position',
      title: 'Position',
    },
    // {
    //   name: 'icon',
    //   title: 'Select Icon',
    //   options: {
    //     collapsible: true,
    //     collapsed: true,
    //   },
    // },
  ],
  fields: [
    {
      title: 'Type',
      name: 'type',
      type: 'string',
      fieldset: 'type',
      options: { list: ICON_TYPES },
    },
    {
      title: 'Position',
      name: 'position',
      type: 'string',
      fieldset: 'position',
      options: { list: ICON_POSITIONS },
    },
    {
      name: 'icon',
      title: 'Select Icon',
      type: 'string',
      // fieldset: 'icon',
      options: { list: AVAILABLE_ICONS },
      // type: 'visualOptions',
      // options: {
      //   showTooltip: true,
      //   optionSize: 'small',
      //   list: AVAILABLE_ICONS,
      // },
      // validation: (Rule): any => Rule.required(),
    },
  ],
  preview: {
    select: {
      type: 'type',
      icon: 'icon',
    },
  },
};
