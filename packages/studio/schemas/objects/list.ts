const LIST_TYPES = [
  { title: 'numbered', value: 'numbered' },
  { title: 'unordered', value: 'unordered' },
];

export default {
  title: 'List',
  name: 'list',
  type: 'object',
  fields: [
    {
      title: 'Type',
      name: 'type',
      type: 'string',
      options: { list: LIST_TYPES },
    },
    {
      title: 'Content',
      name: 'content',
      type: 'array',
      of: [{ type: 'listContent' }],
    },
  ],
  preview: {
    select: {
      title: 'type',
    },
  },
};
