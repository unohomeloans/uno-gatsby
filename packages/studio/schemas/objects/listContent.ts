import {
  ArrowTopRight,
  Heart,
  Person,
  Trophy,
} from '../../components/IconsList/IconsList';

// const AVAILABLE_ICONS = {
//   arrowTopRight: { name: 'arrow-top-right', icon: ArrowTopRight },
//   trophy: { name: 'trophy', icon: Trophy },
//   person: { name: 'person', icon: Person },
//   heart: { name: 'heart', icon: Heart },
// };

const AVAILABLE_ICONS = [
  { title: 'arrow-top-right', value: 'arrowTopRight', icon: ArrowTopRight },
  { title: 'trophy', value: 'trophy', icon: Trophy },
  { title: 'person', value: 'person', icon: Person },
  { title: 'heart', value: 'heart', icon: Heart },
];

export default {
  title: 'List Content',
  name: 'listContent',
  type: 'object',
  fieldsets: [
    {
      name: 'title',
      title: 'Title',
    },
    {
      name: 'content',
      title: 'Content',
    },
    // {
    //   name: 'icon',
    //   title: 'Icon',
    //   options: {
    //     collapsible: true,
    //     collapsed: true,
    //   },
    // },
  ],
  fields: [
    {
      title: 'Title',
      name: 'title',
      type: 'string',
      fieldset: 'title',
    },
    {
      title: 'Content',
      name: 'content',
      type: 'simpleBlockContent',
      fieldset: 'content',
    },
    {
      name: 'icon',
      title: 'Select Icon',
      type: 'string',
      // fieldset: 'icon',
      options: { list: AVAILABLE_ICONS },
      // type: 'visualOptions',
      // options: {
      //   showTooltip: true,
      //   optionSize: 'small',
      //   list: AVAILABLE_ICONS,
      // },
    },
  ],
  preview: {
    select: {
      title: 'title',
    },
  },
};
