export default {
  title: 'Menu Items',
  name: 'menuItems',
  type: 'array',
  of: [{ type: 'menuItemsDetails' }],
};
