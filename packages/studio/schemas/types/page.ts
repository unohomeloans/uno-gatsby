import { MdImportContacts } from 'react-icons/md';

export default {
  name: 'page',
  title: 'Page',
  type: 'document',
  liveEdit: false,
  icon: MdImportContacts,
  fields: [
    {
      name: 'content',
      type: 'pageContent',
    },
  ],
  preview: {
    select: {
      title: 'content.main.title',
      subtitle: 'herotext',
      media: 'mainImage',
    },
  },
};
