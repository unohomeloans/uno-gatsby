import LenderIcon from '../../components/Icons/LenderIcon';
import { PrepareReturn, PrepareTitleDisabledArgs } from '../../typings/Prepare';
import { prepareTitle } from '../../utils/prepareObject';

export default {
  name: 'lenders',
  title: 'Lenders',
  type: 'document',
  icon: LenderIcon,
  fieldsets: [{ name: 'seo', title: 'SEO meta tags' }],
  fields: [
    {
      name: 'title',
      title: 'Title',
      type: 'string',
    },
    {
      name: 'slug',
      title: 'Slug',
      type: 'slug',
      options: {
        source: 'title',
        maxLength: 96,
      },
    },
    {
      title: 'Lenders API Code',
      name: 'lendersCode',
      description:
        '☝ Yes it needs to be updated, will be a dropdown on the next release so please only select one.',
      type: 'array',
      of: [{ type: 'string' }],
      validation: (Rule: any): any => Rule.unique(),
      options: {
        // isHighlighted: true,
        list: [
          { title: '86400', value: '864' },
          { title: 'Adelaide Bank', value: 'ADB' },
          { title: 'AMP', value: 'AMP' },
          { title: 'ANZ', value: 'ANZ' },
          { title: 'Bluestone', value: 'BLU' },
          { title: 'Bank of Melbourne', value: 'BOM' },
          { title: 'BankSA', value: 'BSA' },
          { title: 'Bank of Sydney', value: 'BOS' },
          { title: 'Bank West', value: 'BWT' },
          { title: 'Citi Australia', value: 'CTI' },
          { title: 'Commonwealth Bank', value: 'CBA' },
          { title: 'Credit Union Australia (CUA)', value: 'CUA' },
          { title: 'Firefighters Mutual Bank', value: 'FMB' },
          { title: 'Resimac Group', value: 'HLL' },
          { title: 'Health Professionals Bank', value: 'HPB' },
          { title: 'ING Direct', value: 'ING' },
          { title: 'La Trobe Financial', value: 'LTB' },
          { title: 'Liberty', value: 'LIB' },
          { title: 'MAS', value: 'MAS' },
          { title: 'ME Bank', value: 'MEB' },
          { title: 'Mortgage House', value: 'MHO' },
          { title: 'National Australia Bank Group', value: 'NAB' },
          { title: 'Pepper', value: 'PPR' },
          { title: 'Qudos Bank', value: 'QML' },
          { title: 'St.George', value: 'STG' },
          { title: 'Suncorp Group', value: 'SCP' },
          { title: 'Teachers Mutual Bank', value: 'TMB' },
          { title: 'UniBank', value: 'UNI' },
          { title: 'Westpac', value: 'WBC' },
        ],
        layout: 'radio',
      },
    },
    {
      title: 'Deposit',
      name: 'deposit',
      description: 'E.g. 400000',
      type: 'number',
    },
    {
      title: 'Property Estimated Value',
      name: 'propertyValue',
      description: 'E.g. 1000000',
      type: 'number',
    },
    {
      title: 'Loan Amount',
      name: 'loanAmount',
      description: 'E.g. 1000000',
      type: 'number',
    },
    {
      title: 'Loan Type',
      name: 'loanType',
      type: 'array',
      of: [{ type: 'string' }],
      validation: (Rule: any): any => Rule.unique(),
      options: {
        // isHighlighted: true,
        list: [
          { title: 'Purchase', value: 'Purchase' },
          { title: 'Refinance', value: 'Refinance' },
        ],
        layout: 'radio',
      },
    },
    {
      title: 'Rate Type',
      name: 'rateType',
      type: 'array',
      of: [{ type: 'string' }],
      validation: (Rule: any): any => Rule.unique(),
      options: {
        // isHighlighted: true,
        list: [
          { title: 'Variable Rate', value: 'VariableRate' },
          { title: 'Fixed Rate', value: 'FixedRate' },
        ],
        layout: 'radio',
      },
    },
    {
      name: 'subtitle',
      title: 'Subtitle',
      type: 'simpleBlockContent',
    },
    {
      name: 'body',
      title: 'Body',
      type: 'simpleBlockContent',
    },
    {
      name: 'seoTitle',
      title: 'Title',
      description:
        'This title will show in search results and social sharing, please notice if empty the default title will apply ',
      type: 'string',
      fieldset: 'seo',
      validation: (Rule: any): any => Rule.max(150),
    },
    {
      name: 'seoDescription',
      title: 'Description',
      description:
        'This description will show in search results and social sharing, please notice if empty the text will come from the Site Settings. 🍪',
      type: 'text',
      fieldset: 'seo',
      rows: 3,
      validation: (Rule: any): any => Rule.min(50).max(300),
    },
  ],
  preview: {
    select: {
      title: 'title',
    },
    prepare(selection: PrepareTitleDisabledArgs): PrepareReturn {
      return prepareTitle(selection, 'Lender');
    },
  },
};
