import BriefcaseIcon from '../../components/Icons/BriefcaseIcon';

export default {
  name: 'broker',
  title: 'Broker',
  type: 'document',
  liveEdit: false,
  icon: BriefcaseIcon,
  fields: [
    {
      name: 'content',
      type: 'brokerContent',
    },
  ],
  preview: {
    select: {
      title: 'content.main.name',
      subtitle: 'content.main.location',
      media: 'content.main.mainImage',
    },
  },
};
