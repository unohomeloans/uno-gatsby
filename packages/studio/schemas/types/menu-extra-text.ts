export default {
  title: 'Menu Extra Text',
  name: 'menuExtraText',
  type: 'array',
  of: [{ type: 'menuExtraTextDetails' }],
};
