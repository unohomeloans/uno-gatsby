export default {
  title: 'Menu Items Details',
  name: 'menuItemsDetails',
  type: 'object',
  fieldsets: [
    {
      name: 'internalLink',
      title: 'Internal Link',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
    {
      name: 'externalLink',
      title: 'External Link',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
  ],
  fields: [
    {
      name: 'navItemLabel',
      title: 'Nav Item Label',
      type: 'string',
      validation: (Rule: any): any => Rule.required().min(1).max(32),
    },
    {
      name: 'navItemType',
      title: 'Nav Item Type',
      type: 'string',
      options: {
        layout: 'radio',
        list: ['internal', 'external'],
      },
    },
    {
      title: 'Nav Items',
      name: 'ctas',
      type: 'array',
      of: [{ name: 'menulinks', type: 'menulinks' }],
    },
  ],
};
