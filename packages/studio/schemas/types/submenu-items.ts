export default {
  title: 'Submenu Items',
  name: 'submenuItems',
  type: 'object',
  fields: [
    { name: 'submenuItemLabel', title: 'Submenu Item Label', type: 'string' },
    {
      name: 'submenuItemURL',
      title: 'Submenu Item URL',
      description: 'Select a Page, Post 🔥',
      type: 'reference',
      to: [{ type: 'page' }],
    },
  ],
};
