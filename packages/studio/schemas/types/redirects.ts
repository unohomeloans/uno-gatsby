import RedirectIcon from '../../components/Icons/RedirectIcon';
import {
  PrepareRedirectArgs,
  PrepareReturnCTACollection,
} from '../../typings/Prepare';

export default {
  name: 'redirects',
  title: 'Redirects',
  type: 'document',
  icon: RedirectIcon,
  fields: [
    {
      name: 'fromPath',
      title: 'From',
      description: 'Existing page path (without https://chucknorris.com)',
      type: 'string',
    },
    {
      name: 'toPath',
      title: 'To',
      description: 'New page path (without https://chucknorris.com)',
      type: 'string',
    },
    {
      name: 'statusCode',
      title: 'Type',
      type: 'string',
      validation: (Rule: any): any => Rule.required(),
      description:
        'Permanent or Temporary (https://moz.com/learn/seo/redirection) ',
      options: {
        list: [
          { title: 'Permanent', value: '301' },
          { title: 'Temporary', value: '302' },
        ],
      },
    },
  ],
  preview: {
    select: {
      fromPath: 'fromPath',
      to: 'destination',
      type: 'statusCode',
    },
    prepare({
      fromPath,
      type,
    }: PrepareRedirectArgs): PrepareReturnCTACollection {
      return {
        title: fromPath,
        subtitle: type || 'Unknown',
      };
    },
  },
};
