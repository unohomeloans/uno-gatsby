export default {
  title: 'Submenus',
  name: 'submenu',
  type: 'array',
  of: [{ name: 'cta', type: 'cta' }],
};
