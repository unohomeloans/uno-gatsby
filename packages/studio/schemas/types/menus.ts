import MenuIcon from '../../components/Icons/MenuIcon';

export default {
  name: 'menus',
  _id: 'menus',
  title: 'Menus',
  type: 'document',
  description:
    'This handles all the global settings throughout the site, promo bars, phone numbers etc, ',
  icon: MenuIcon,
  fieldsets: [
    {
      name: 'extraText',
      title: 'Extra Text',
      type: 'array',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
  ],
  fields: [
    { name: 'title', title: 'Title', type: 'string' },
    {
      name: 'slug',
      title: 'Slug',
      type: 'slug',
      options: { source: 'title', maxLength: 96 },
      validation: (Rule: any): any => Rule.required(),
    },
    { name: 'items', title: 'Nav Items', type: 'menuItems' },
    {
      name: 'extraText',
      title: 'Extra Menu Text',
      type: 'menuExtraText',
      fieldset: 'extraText',
    },
  ],
};
