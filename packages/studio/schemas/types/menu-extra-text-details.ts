export default {
  title: 'Menu Items Details',
  name: 'menuExtraTextDetails',
  type: 'object',
  fieldsets: [
    {
      name: 'internalLink',
      title: 'Internal Link',
      options: {
        collapsible: true,
        collapsed: true,
      },
    },
    ,
  ],
  fields: [
    {
      name: 'extraText',
      title: 'Extra Text',
      type: 'simpleBlockContent',
    },
  ],
};
