import { MdLink } from 'react-icons/md';
import { createClient, SanityClient } from '@sanity/client';
import {
  PrepareReturnCTACollection,
  PrepareTitleSubtitleArgs,
} from '../../typings/Prepare';
import moment from 'moment';

const datasetRegex = /^\/production/;

function myAsyncSlugifier(input: any) {
  const query = '*[_id == $id][0]';
  const params = { id: input._ref };

  const checkPathForDataset = () =>
    datasetRegex.test(window.location.pathname) ? 'staging' : 'dev';

  const client = createClient({
    projectId: '86mn51w1',
    dataset: checkPathForDataset(),
    apiVersion: moment().format('YYYY-MM-DD'),
  });

  return client
    .fetch(query, params)
    .then((doc: any) =>
      doc.content.main.title.toLowerCase().replace(/\s+/g, '-').slice(0, 200)
    );
}

export default {
  name: 'route',
  type: 'document',
  title: 'Page routes',
  icon: MdLink,
  initialValue: {
    useSiteTitle: false,
  },
  fieldsets: [
    {
      title: 'Visibility',
      name: 'visibility',
    },
  ],
  fields: [
    {
      name: 'page',
      type: 'reference',
      validation: (Rule: any) => Rule.required(),
      description:
        'The page you want to appear at this path. Remember it needs to be published.',
      to: [
        {
          type: 'page',
        },
      ],
    },
    {
      name: 'slug',
      type: 'slug',
      description: 'This is the website path the page will accessible on',
      title: 'Path',
      validation: (Rule: any) =>
        Rule.required().custom((slug: any) => {
          if (slug && slug.current && slug.current === '/') {
            return 'Cannot be /';
          }
          return true;
        }),
      options: {
        source: 'page',
        slugify: myAsyncSlugifier,
      },
    },
    {
      name: 'parent',
      type: 'reference',
      title: 'Parent Page Picker',
      description:
        'Is this a child page? If yes please select a page that will be appended in the URL.',
      to: [
        {
          type: 'page',
        },
      ],
    },
    {
      title: 'Use site title?',
      description:
        'Use the site settings title as page title instead of the title on the referenced page',
      name: 'useSiteTitle',
      type: 'boolean',
    },
    {
      title: 'Include in sitemap',
      description: 'For search engines. Will be generateed to /sitemap.xml',
      name: 'includeInSitemap',
      type: 'boolean',
      fieldset: 'visibility',
    },
    {
      title: 'Disallow in robots.txt',
      description: 'Hide this route for search engines like google',
      name: 'disallowRobots',
      type: 'boolean',
      fieldset: 'visibility',
    },
    {
      name: 'campaign',
      type: 'string',
      title: 'Campaign',
      description: 'UTM for campaings',
    },
  ],
  preview: {
    select: {
      title: 'slug.current',
      subtitle: 'page.title',
    },
    prepare({
      title,
      subtitle,
    }: PrepareTitleSubtitleArgs): PrepareReturnCTACollection {
      return {
        title: ['/', title].join(''),
        subtitle,
      };
    },
  },
};
