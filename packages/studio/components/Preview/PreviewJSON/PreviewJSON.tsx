import React from 'react';

interface PreviewJSONProps {
  options: any;
  document: any;
}

const PreviewJSON: React.FC<PreviewJSONProps> = ({ document, options }) => (
  <pre>{JSON.stringify(document.displayed, null, options.indentation)}</pre>
);

export default PreviewJSON;
