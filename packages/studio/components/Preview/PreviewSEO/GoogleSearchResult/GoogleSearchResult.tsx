import React from 'react';
import styles from './GoogleSearchResult.css?inline';
import { assemblePageUrl } from '../../../../utils/componentUtils';

interface GoogleSearchResultProps {
  document?: any;
  width?: number;
  options?: any;
}

const GoogleSearchResult: React.FC<GoogleSearchResultProps> = ({
  document,
  options,
  width,
}) => {
  const { title, excerpt: description, content } = document;

  const url = assemblePageUrl({ document, options });

  const remoteURL = process.env.GATSBY_SITE_URL;
  const localURL = 'http://localhost:8000';
  const baseUrl =
    window.location.hostname === 'localhost' ? localURL : remoteURL;

  const documentTitle = document._type === 'page' ? content.main.title : title;
  const documentDescription =
    document._type === 'page' ? content.meta.metaDescription : description;
  const documentUrl =
    document._type === 'page' ? `${baseUrl}/${content.main.slug.current}` : url;

  return (
    <div className={styles.seoItem}>
      <h3>Google search result preview</h3>
      <div className={styles.googleWrapper} style={{ width }}>
        {documentTitle}
        <div className={styles.url}>{documentUrl}</div>
        {documentDescription && <p>{documentDescription}</p>}
      </div>
    </div>
  );
};

export default GoogleSearchResult;
