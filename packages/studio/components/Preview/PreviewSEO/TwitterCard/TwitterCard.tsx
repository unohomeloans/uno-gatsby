import React, { useCallback } from 'react';
import imageUrlBuilder from '@sanity/image-url';
import { assemblePageUrl } from '../../../../utils/componentUtils';

import styles from './TwitterCard.css?inline';
import { useClient } from 'sanity';

const author = {
  name: 'uno home loans',
  handle: 'unohomeloans',
  image:
    'https://pbs.twimg.com/profile_images/1348393431916113923/ub74uMFb_400x400.png',
};

interface TwitterCardProps {
  document?: any;
  options?: any;
}

const TwitterCard: React.FC<TwitterCardProps> = ({ document, options }) => {
  const { title, excerpt: description, mainImage, content } = document;
  const url = assemblePageUrl({ document, options });
  const websiteUrlWithoutProtocol = url.split('://')[1];

  const sanityClient = useClient();
  const builder: any = useCallback(
    () => imageUrlBuilder(sanityClient),
    [sanityClient]
  );
  const urlFor = (source: any) => builder?.image(source);

  const remoteURL = process.env.GATSBY_SITE_URL;
  const localURL = 'http://localhost:8000';
  const baseUrl =
    window.location.hostname === 'localhost' ? localURL : remoteURL;

  const documentTitle = document._type === 'page' ? content.main.title : title;
  const documentDescription =
    document._type === 'page' ? content.meta.metaDescription : description;
  const documentImage =
    document._type === 'page' ? content.meta.openImage : mainImage;
  const documentUrl =
    document._type === 'page'
      ? `${baseUrl}/${content.main.slug.current}`
      : websiteUrlWithoutProtocol;

  return (
    <div className={styles.seoItem}>
      <h3>Twitter card preview</h3>
      <div className={styles.tweetWrapper} style={{ width: 500 }}>
        {author && (
          <div className={styles.tweetAuthor}>
            <img
              className={styles.tweetAuthorAvatar}
              src={
                author && typeof author.image === 'object'
                  ? urlFor(author.image).width(300).url() || ''
                  : author.image
              }
            />
            <span className={styles.tweetAuthorName}>{author.name}</span>
            <span className={styles.tweetAuthorHandle}>@{author.handle}</span>
          </div>
        )}
        <div className={styles.tweetText}>
          <p>
            The card for your website will look a little something like this!
          </p>
        </div>
        <a href={documentUrl} className={styles.tweetUrlWrapper}>
          <div className={styles.tweetCardPreview}>
            <div className={styles.tweetCardImage}>
              <img src={urlFor(documentImage).width(300).url() || ''} />
            </div>
            <div className={styles.tweetCardContent}>
              <h2 className={styles.tweetCardTitle}>{documentTitle}</h2>
              {documentDescription && (
                <div className={styles.tweetCardDescription}>
                  {documentDescription}
                </div>
              )}
              <div className={styles.tweetCardDestination}>{documentUrl}</div>
            </div>
          </div>
        </a>
      </div>
    </div>
  );
};

export default TwitterCard;
