import React from 'react';
import FacebookShare from './FacebookShare';
import GoogleSearchResult from './GoogleSearchResult';
import TwitterCard from './TwitterCard';

interface PreviewSEOProps {
  document: any;
  options: any;
}

const PreviewSEO: React.FC<PreviewSEOProps> = ({ document, options }) => {
  const { displayed } = document;
  return (
    <div
      style={{
        width: '100%',
        height: '100%',
      }}>
      <GoogleSearchResult document={displayed} options={options} />
      <TwitterCard document={displayed} options={options} />
      <FacebookShare document={displayed} options={options} />
    </div>
  );
};

export default PreviewSEO;
