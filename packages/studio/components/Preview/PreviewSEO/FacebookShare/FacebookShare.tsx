import React, { useCallback } from 'react';
import { useClient } from 'sanity';
import imageUrlBuilder from '@sanity/image-url';

import styles from './FacebookShare.css?inline';

interface FacebookShareProps {
  document?: any;
  width?: number;
  options?: any;
}

const FacebookShare: React.FC<FacebookShareProps> = ({ document, width }) => {
  const {
    title,
    excerpt: description = [],
    mainImage: openGraphImage,
    content,
  } = document;

  const sanityClient = useClient();
  const builder: any = useCallback(
    () => imageUrlBuilder(sanityClient),
    [sanityClient]
  );

  const urlFor = (source: any) => {
    return builder?.image(source);
  };

  const websiteUrl = 'http://localhost:3000';
  const websiteUrlWithoutProtocol = websiteUrl.split('://')[1];

  const remoteURL = process.env.GATSBY_SITE_URL;
  const localURL = 'http://localhost:8000';
  const baseUrl =
    window.location.hostname === 'localhost' ? localURL : remoteURL;

  const documentTitle = document._type === 'page' ? content.main.title : title;
  const documentDescription =
    document._type === 'page' ? content.meta.metaDescription : description;
  const documentImage =
    document._type === 'page' ? content.meta.openImage : openGraphImage;
  const documentUrl =
    document._type === 'page'
      ? `${baseUrl}/${content.main.slug.current}`
      : websiteUrlWithoutProtocol;

  return (
    <div className={styles.seoItem}>
      <h3>Facebook share</h3>
      <div className={styles.facebookWrapper} style={{ width: 500 }}>
        <div className={styles.facebookImageContainer}>
          <img
            className={styles.facebookCardImage}
            src={urlFor(documentImage).width(500).url() || ''}
          />
        </div>
        <div className={styles.facebookCardContent}>
          <div className={styles.facebookCardUrl}>{documentUrl}</div>
          <div className={styles.facebookCardTitle}>
            <a href={documentUrl}>{documentTitle}</a>
          </div>
          <div className={styles.facebookCardDescription}>
            {documentDescription}
          </div>
        </div>
      </div>
    </div>
  );
};

export default FacebookShare;
