// youtube.js
import React from 'react';
import getYouTubeId from 'get-youtube-id';
import YouTube from 'react-youtube';

interface PreviewYouTubeProps {
  value: any;
}

const PreviewYouTube: React.FC<PreviewYouTubeProps> = ({ value }) => {
  const { url } = value;
  const id = getYouTubeId(url) || '';
  return <YouTube videoId={id} />;
};

export default PreviewYouTube;
