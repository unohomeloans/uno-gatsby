import React from 'react';
import { EyeOpenIcon } from '@sanity/icons';
import resolveUrl from '../../../utils/resolvePreviewUrls';
import { StructureBuilder } from 'sanity/desk';

const env = process.env.NODE_ENV || 'development';

const PreviewIFrame: React.FC = (S: StructureBuilder) =>
  S.view
    .component(({ document }: any) => {
      const { displayed } = document;
      if (!displayed) {
        return <p>Nothing to display</p>;
      }
      return (
        <>
          {env !== 'development' && <div style={{ padding: '0 0.5em' }}></div>}
          <iframe
            style={{
              width: '100%',
              height: '100%',
            }}
            frameBorder={'0'}
            src={resolveUrl(displayed)}
          />
        </>
      );
    })
    .title('Web Preview')
    .icon(EyeOpenIcon);

export default PreviewIFrame;
