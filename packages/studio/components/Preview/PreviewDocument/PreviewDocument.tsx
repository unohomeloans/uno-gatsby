import React from 'react';
import styles from './PreviewDocument.css?inline';

interface PreviewDocumentProps {
  options: any;
  document: any;
}

const remoteURL = 'https://qa.uno.com.au/';
const localURL = 'http://local.unohomeloans.com.au:8000/';

const baseUrl = window.location.hostname === 'localhost' ? localURL : remoteURL;

const assemblePostUrl = ({ displayed, options }: any) => {
  if (displayed._type === 'page') {
    const slug = displayed.content.main.slug.current || '';
    return `${baseUrl}${slug}/`;
  } else {
    const { slug } = displayed;
    const { previewURL } = options;

    if (!slug || !baseUrl) {
      console.warn('Missing slug or previewURL', { slug, previewURL });
      return '';
    }

    switch (displayed._type) {
      case 'post':
        return `${baseUrl}${displayed.slug.current}/`;
      case 'route':
        return `${baseUrl}${displayed.slug.current}/`;
      case 'lenders':
        return `${baseUrl}lenders/${slug.current}/`;
      default:
        return null;
    }
  }
};

const PreviewDocument: React.FC<PreviewDocumentProps> = (props) => {
  const { options } = props;
  const { displayed } = props.document;

  if (!displayed) {
    return (
      <div className={styles.componentWrapper}>
        <p>There is no document to preview</p>
      </div>
    );
  }

  const url = assemblePostUrl({ displayed, options });

  if (!url) {
    return (
      <div className={styles.componentWrapper}>
        <p>Hmm. Having problems constructing the web front-end URL.</p>
      </div>
    );
  }

  return (
    <div className={styles.componentWrapper}>
      <div className={styles.iframeContainer}>
        <p>URL: {url}</p>
        <iframe src={url} frameBorder={'0'} />
      </div>
    </div>
  );
};

export default PreviewDocument;
