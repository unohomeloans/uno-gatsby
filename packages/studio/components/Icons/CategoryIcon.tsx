import React from 'react';
import Emoji from 'a11y-react-emoji';
import { FONT_SIZE_STYLE } from './styled';

const CategoryIcon: React.FC = () => (
  <Emoji style={FONT_SIZE_STYLE} symbol="👻" />
);

export default CategoryIcon;
