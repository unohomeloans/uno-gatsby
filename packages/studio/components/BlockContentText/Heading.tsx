import React from 'react';
import { getTextStyle } from './styled';
import styles from './styles.css?inline';

export const SectionHeading: React.FC = ({ children }) => (
  <span
    style={{
      ...getTextStyle(18, 21.6, 800),
      textTransform: 'uppercase',
      letterSpacing: '0.15em',
    }}>
    {children}
  </span>
);

export const HeadingOne: React.FC = ({ children }) => (
  <span style={getTextStyle(72, 86.4, 800)}>{children}</span>
);
export const HeadingTwo: React.FC = ({ children }) => (
  <span style={getTextStyle(48, 57.6, 800)}>{children}</span>
);
export const HeadingThree: React.FC = ({ children }) => (
  <span style={getTextStyle(24, 28.8, 800)}>{children}</span>
);
export const HeadingFour: React.FC = ({ children }) => (
  <span style={getTextStyle(20, 24, 800)}>{children}</span>
);
