interface TextStyle {
  fontSize: string;
  lineHeight: string;
  fontWeight?: number;
}

const pxToRem = (num: number): string => `${num / 16}rem`;

export const getTextStyle = (
  fontSize: number,
  lineHeight: number,
  fontWeight: number
): TextStyle => ({
  fontSize: pxToRem(fontSize),
  lineHeight: pxToRem(lineHeight),
  fontWeight: fontWeight,
});
