import React from 'react';
import { getTextStyle } from './styled';

export const NormalText: React.FC = ({ children }) => (
  <span style={getTextStyle(18, 28.8, 400)}>{children}</span>
);
export const TextOne: React.FC = ({ children }) => (
  <span style={getTextStyle(12, 19.2, 400)}>{children}</span>
);
export const TextTwo: React.FC = ({ children }) => (
  <span style={getTextStyle(14, 22.4, 400)}>{children}</span>
);
export const TextThree: React.FC = ({ children }) => (
  <span style={getTextStyle(16, 25.6, 400)}>{children}</span>
);
export const TextFour: React.FC = ({ children }) => (
  <span style={getTextStyle(20, 32, 400)}>{children}</span>
);
export const TextFive: React.FC = ({ children }) => (
  <span style={getTextStyle(24, 38.4, 400)}>{children}</span>
);
export const TextSix: React.FC = ({ children }) => (
  <span style={getTextStyle(28, 44.8, 400)}>{children}</span>
);
export const TextSeven: React.FC = ({ children }) => (
  <span style={getTextStyle(32, 51.2, 400)}>{children}</span>
);
export const TextEight: React.FC = ({ children }) => (
  <span style={getTextStyle(40, 64, 400)}>{children}</span>
);
export const TextNine: React.FC = ({ children }) => (
  <span style={getTextStyle(48, 76.8, 400)}>{children}</span>
);
