export interface PrepareTitleDisabledArgs {
  title: string;
  disabled?: boolean;
}

export interface PrepareCTAArgs {
  applink?: any;
  link?: any;
  linkedPage?: any;
  title?: string;
}

export interface PrepareRedirectArgs {
  fromPath: string;
  type?: string;
}

export interface PrepareTitleArgs {
  title: string;
}

export interface PrepareTitleSubtitleArgs {
  title: string;
  subtitle: string;
}

export interface PrepareAlignmentArgs {
  alignment: any;
}

export interface PrepareCTACollectionArgs {
  ctas: any;
}

export interface PrepareReturn {
  title: string;
}

export interface PrepareReturnCTACollection extends PrepareReturn {
  subtitle?: string;
}
