import React, { useEffect, useMemo } from 'react';
import { DashboardWidgetContainer } from '@sanity/dashboard';
import { Box, Flex, Heading } from '@sanity/ui';
import { useClient, useSchema } from 'sanity';
import { Link, withRouter } from 'sanity/router';
import moment from 'moment';
import { GoHome } from 'react-icons/go';
import './DeskMenuWidget.css';
import ConfigIcon from '../../components/Icons/ConfigIcon';
import PostIcon from '../../components/Icons/PostIcon';
import AuthorIcon from '../../components/Icons/AuthorIcon';
import CategoryIcon from '../../components/Icons/CategoryIcon';
import LenderIcon from '../../components/Icons/LenderIcon';
import BriefcaseIcon from '../../components/Icons/BriefcaseIcon';
import MenuIcon from '../../components/Icons/MenuIcon';
import RedirectIcon from '../../components/Icons/RedirectIcon';
import SettingsIcon from '../../components/Icons/SettingsIcon';
import { MdImportContacts, MdLink, MdOutlineLocalOffer } from 'react-icons/md';

const getIcon = (icon: string) => {
  switch (icon) {
    case 'frontpage':
      return <GoHome color="rgb(38, 47, 61)" className="icon" />;
    case 'configuration':
      return <ConfigIcon />;
    case 'page':
      return <MdImportContacts color="rgb(38, 47, 61)" className="icon" />;
    case 'post':
      return <PostIcon />;
    case 'author':
      return <AuthorIcon />;
    case 'category':
      return <CategoryIcon />;
    case 'lenders':
      return <LenderIcon />;
    case 'broker':
      return <BriefcaseIcon />;
    case 'menus':
      return <MenuIcon />;
    case 'redirects':
      return <RedirectIcon />;
    case 'siteSettings':
      return <SettingsIcon />;
    case 'route':
      return <MdLink color="rgb(38, 47, 61)" className="icon" />;
    case 'media.tag':
      return (
        <MdOutlineLocalOffer color="rgb(38, 47, 61)" className="icon r-95" />
      );
    default:
      return null;
  }
};

const getTitle = (title: string) => {
  switch (title) {
    case 'page':
      return 'pages';
    case 'post':
      return 'posts';
    case 'media.tag':
      return 'media tags';
    case 'configuration':
      return 'configurations';
    case 'siteSettings':
      return 'settings';
    case 'route':
      return 'page routes';
    default:
      return title;
  }
};

const DeskMenuWidget = (props: any) => {
  const currentDataset = useClient({
    apiVersion: moment().format('YYYY-MM-DD'),
  }).config().dataset;

  const schema = useSchema();
  const documentTypes = useMemo(
    () =>
      schema
        .getTypeNames()
        .filter((typeName) => {
          const schemaType = schema.get(typeName);
          return schemaType?.type?.name === 'document';
        })
        .filter(
          (docType) =>
            docType != 'sanity.imageAsset' && docType != 'sanity.fileAsset'
        ),
    [schema]
  );

  const extendedTypes = ['frontpage', ...documentTypes, 'configuration'].map(
    (val) => {
      return {
        name: val,
        path:
          (val == 'page' && 'pages') ||
          (val == 'siteSettings' && 'settings') ||
          (val == 'post' && 'posts') ||
          val,
        title: getTitle(val),
      };
    }
  );

  return (
    <DashboardWidgetContainer>
      <Box padding={3} as="header">
        <Heading size={1} as="h2">
          Edit Your Content
        </Heading>
      </Box>
      <Flex padding={3} wrap={'wrap'} align={'stretch'}>
        {extendedTypes.map((docType, index) => (
          <div
            className={`desk-item${
              index < extendedTypes.length - 1 ? ' mb' : ''
            }`}>
            <Link
              href={`/${
                currentDataset == 'staging' ? 'production' : 'development'
              }/desk/${docType.path}`}>
              {getIcon(docType.name)}
              <span>{docType.title}</span>
            </Link>
          </div>
        ))}
      </Flex>
    </DashboardWidgetContainer>
  );
};

export default withRouter(DeskMenuWidget);
