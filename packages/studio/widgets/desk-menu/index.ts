import { DashboardWidget, LayoutConfig } from '@sanity/dashboard';
import DeskMenuWidget from './DeskMenuWidget';

export function deskMenuWidget(config?: {
  layout?: LayoutConfig;
}): DashboardWidget {
  return {
    name: 'project-info',
    component: DeskMenuWidget,
    layout: config?.layout ?? { width: 'full' },
  };
}
