import { defineConfig, isDev } from 'sanity';
import { deskTool } from 'sanity/desk';
import { crossDatasetDuplicator } from '@sanity/cross-dataset-duplicator';
import { dashboardTool } from '@sanity/dashboard';
import dashboardConfig from './config/dashboardConfig';
import { media } from 'sanity-plugin-media';
import { colorInput } from '@sanity/color-input';
import { visionTool } from '@sanity/vision';
import { schemaTypes } from './schemas/schemaTypes';
import deskStructure, { getDefaultDocumentNode } from './deskStructure';
import { table } from '@sanity/table';

const devOnlyPlugins = [visionTool()];
const sharedPlugins = [
  // all other plugins...
  dashboardTool(dashboardConfig),
  deskTool({
    structure: deskStructure,
    defaultDocumentNode: getDefaultDocumentNode,
  }),
  table(),
  colorInput(),
  media(),
  crossDatasetDuplicator({
    // Required settings to show document action
    types: [
      'page',
      'broker',
      'category',
      'post',
      'author',
      'lenders',
      'redirects',
      'menus',
      'sanity.imageAsset',
    ],
    // Optional settings
    // tool: true,
    // filter: '_type != "product"',
    // follow: [],
  }),
  ...(isDev ? devOnlyPlugins : []),
];

export default defineConfig([
  {
    name: 'production',
    title: 'Production',
    projectId: '86mn51w1',
    dataset: 'staging',
    basePath: '/production',
    plugins: sharedPlugins,
    schema: { types: schemaTypes },
  },
  {
    name: 'development',
    title: 'Development',
    projectId: '86mn51w1',
    dataset: 'dev',
    basePath: '/development',
    plugins: sharedPlugins,
    schema: { types: schemaTypes },
  },
]);
