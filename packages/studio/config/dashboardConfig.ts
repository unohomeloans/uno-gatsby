import { netlifyWidget } from 'sanity-plugin-dashboard-widget-netlify';
import { documentListWidget } from 'sanity-plugin-dashboard-widget-document-list';
import { projectInfoWidget, projectUsersWidget } from '@sanity/dashboard';
import { deskMenuWidget } from '../widgets/desk-menu';

export default {
  widgets: [
    // { name: 'structure-menu' },
    deskMenuWidget(),
    projectInfoWidget({ layout: { width: 'small' } }),
    projectUsersWidget({ layout: { width: 'small' } }),
    documentListWidget({
      title: 'Last edited posts',
      order: '_updatedAt desc',
      types: ['post'],
      layout: {
        width: 'small',
      },
    }),
    netlifyWidget({
      title: 'My Netlify deploys',
      sites: [
        {
          title: 'QA',
          apiId: '9e6c6bd4-4d6c-44b4-a7c0-d2eb3509fe87',
          buildHookId: '60a1cacb7b087d18cdb8a2c6',
          name: 'mystifying-stonebraker-9e3541',
        },
        {
          title: 'Production',
          apiId: 'bf084737-d1c3-414c-a1e3-12bc6003a204',
          buildHookId: '5f51969500ba8a01122cdb17',
          name: 'determined-volhard-008adfb',
        },
      ],
    }),
  ],
};
