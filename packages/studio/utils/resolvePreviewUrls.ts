export default (document: any): string => {
  const remoteURL = 'https://qa.uno.com.au/';
  const localURL = 'http://localhost:8000';
  const baseUrl =
    window.location.hostname === 'localhost' ? localURL : remoteURL;

  switch (document._type) {
    case 'route':
      if (!document.slug || !document.slug.current) {
        return baseUrl;
      }
      return `${baseUrl}/${document.slug.current}`;
    case 'post':
      return `${baseUrl}/${document.slug.current}`;
    case 'lenders':
      return `${baseUrl}/lenders/${document.slug.current}`;
    case 'siteSettings':
      return baseUrl;
    case 'page':
      if (document._id === 'frontpage' || document._id === 'drafts.frontpage') {
        return `${baseUrl}/${document.content.main.slug.current}`;
      }
      return '';
    default:
      return '';
  }
};
