import {
  PrepareCTAArgs,
  PrepareReturn,
  PrepareTitleDisabledArgs,
} from '../typings/Prepare';

export const prepareCTAAndLink = ({
  applink,
  link,
  linkedPage,
  title,
}: PrepareCTAArgs): PrepareReturn => ({
  title: linkedPage
    ? `Page/Post/Category: ${title}`
    : link
    ? 'External Link'
    : applink
    ? `Application Link`
    : title || '',
});

export const prepareTitle = (
  { disabled, title }: PrepareTitleDisabledArgs,
  prefix: string
): PrepareReturn => {
  const titleText = title ?? '';
  const endText = disabled
    ? ': DISABLED'
    : `${!titleText ? '' : ': ' + titleText}`;
  return {
    title: `${prefix}${endText}`,
  };
};
