export const assemblePageUrl = ({ document, options }) => {
  const { slug } = document;
  const { previewURL } = options;

  if (!slug || !previewURL) {
    console.warn('Missing slug or previewURL', { slug, previewURL });
    return '';
  }
  return `${previewURL}/${slug.current}`;
};
