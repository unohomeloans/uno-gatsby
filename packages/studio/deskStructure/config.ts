import { StructureBuilder } from 'sanity/desk';
import ConfigIcon from '../components/Icons/ConfigIcon';
import MenuIcon from '../components/Icons/MenuIcon';

export default (S: StructureBuilder) =>
  S.listItem()
    .title('Configuration')
    .icon(ConfigIcon)
    .child(
      S.list()
        .title('Settings')
        .items([
          S.listItem()
            .title('Menus')
            .icon(MenuIcon)
            .child(
              S.documentTypeList('menus')
                .title('Menus')
                .filter('_type == $type')
                .params({ type: 'menus' })
            ),
        ])
    );
