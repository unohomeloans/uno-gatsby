import { EyeOpenIcon, EditIcon } from '@sanity/icons';
import { FormView, StructureBuilder } from 'sanity/desk';
import ConfigMenu from './config';
import PageMenuItem from './pages';
import siteSettings from './siteSettings';
import { GoHome } from 'react-icons/go';
import posts from './posts';
import PreviewSEO from '../components/Preview/PreviewSEO';
import PreviewDocument from '../components/Preview/PreviewDocument';

const remoteURL = process.env.SITE_URL;
const localURL = 'http://local.unohomeloans.com.au:8000';
const previewURL =
  window.location.hostname === 'localhost' ? localURL : remoteURL;

const hiddenTypes = [
  'companyInfo',
  'page',
  'person',
  'siteSettings',
  'post',
  'routes',
];

export default (S: StructureBuilder) => {
  return S.list()
    .title('Content')
    .items([
      S.documentListItem()
        .title('Frontpage')
        .schemaType('page')
        .icon(GoHome)
        .child(
          S.document()
            .schemaType('page')
            .documentId('frontpage')
            .views([
              S.view.form(),
              S.view
                .component(PreviewDocument)
                .options({ previewURL })
                .title('Web Preview')
                .icon(EyeOpenIcon),
            ])
        ),
      siteSettings(S),
      posts(S),
      ConfigMenu(S),
      PageMenuItem(S),
      ...S.documentTypeListItems().filter(
        (listItem: any) => !hiddenTypes.includes(listItem.getId())
      ),
    ]);
};
export const getDefaultDocumentNode = (S: StructureBuilder) => {
  return S.document().views([
    S.view.form().icon(EditIcon),
    S.view.component(PreviewSEO).title('SEO Preview').icon(EyeOpenIcon),
    S.view
      .component(PreviewDocument)
      .options({ previewURL })
      .title('Web Preview')
      .icon(EyeOpenIcon),
  ]);
};
