import { StructureBuilder } from 'sanity/desk';
import { EyeOpenIcon, EditIcon } from '@sanity/icons';
import { GoPulse } from 'react-icons/go';
import PreviewSEO from '../components/Preview/PreviewSEO';
import PreviewDocument from '../components/Preview/PreviewDocument';

const remoteURL = process.env.SITE_URL;
const localURL = 'http://local.unohomeloans.com.au:8000';
const previewURL =
  window.location.hostname === 'localhost' ? localURL : remoteURL;

export default (S: StructureBuilder) =>
  S.listItem()
    .title('Posts')
    .schemaType('post')
    .child(
      S.documentTypeList('post')
        .title('Posts')
        .child((documentId: any) =>
          S.document()
            .documentId(documentId)
            .schemaType('post')
            .views([
              S.view.form().icon(EditIcon),
              S.view
                .component(PreviewSEO)
                .options({ previewURL })
                .icon(GoPulse)
                .title('SEO Preview'),
              S.view
                .component(PreviewDocument)
                .options({ previewURL })
                .title('Web Preview')
                .icon(EyeOpenIcon),
            ])
        )
    );
