import { StructureBuilder } from 'sanity/desk';
import SettingsIcon from '../components/Icons/SettingsIcon';

export default (S: StructureBuilder) =>
  S.listItem()
    .title('Settings')
    .child(
      S.document()
        .id('siteSettings')
        .schemaType('siteSettings')
        .documentId('siteSettings')
    )
    .icon(SettingsIcon);
