import React from 'react';
import randomAlphaNumeric from './src/utils/randomAlphaNumeric';

const gtagScript = `
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag(\'js\', new Date());
  gtag(\'config\', \'UA-76366632-1\');
`;

const GTag = () => <script dangerouslySetInnerHTML={{ __html: gtagScript }} />;

export const onRenderBody = ({ setHtmlAttributes, setHeadComponents }) => {
  setHeadComponents([
    <link
      key={randomAlphaNumeric(5)}
      href="https://assets.calendly.com/assets/external/widget.css"
      rel="stylesheet"
    />,
    <script
      key={randomAlphaNumeric(5)}
      src="https://assets.calendly.com/assets/external/widget.js"
      type="text/javascript"
      async
    />,
    <script
      key={randomAlphaNumeric(5)}
      async
      src="https://www.googletagmanager.com/gtag/js?id=UA-76366632-1"
    />,
    <GTag key={randomAlphaNumeric(5)} />,
  ]);
  setHtmlAttributes({ lang: 'en' });
};
