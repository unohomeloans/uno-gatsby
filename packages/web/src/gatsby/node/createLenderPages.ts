import { resolve } from 'path';
import { CreatePageArgs, isProd } from '../../utils/createPagesUtil';

interface LenderPagesGraphQL {
  allSanityLenders: {
    edges: {
      node: {
        id: string;
        slug: {
          current: string;
        };
      };
    }[];
  };
}

export default async ({
  actions,
  graphql,
  reporter,
}: CreatePageArgs): Promise<void> => {
  const result = await graphql<LenderPagesGraphQL>(`
    {
      allSanityLenders(filter: { slug: { current: { ne: null } } }) {
        edges {
          node {
            id
            slug {
              current
            }
          }
        }
      }
    }
  `);

  if (result.errors || !result.data) {
    throw result.errors || new Error('No data found');
  }

  const lenderEdges = (result.data.allSanityLenders || {}).edges || [];

  if (!lenderEdges || lenderEdges.length === 0) {
    reporter.error('No lender pages found');
    return;
  }

  for (const edge of lenderEdges) {
    if (!edge.node) {
      reporter.error('No node found for the current Lender edge');
      break;
    } else if (!edge.node.slug) {
      reporter.error(
        `No slug found for the current Lender node:\n${JSON.stringify(
          edge.node,
          null,
          2
        )}`
      );
      break;
    }

    const slug = edge.node.slug.current;

    if (!slug) {
      reporter.error(
        `No current slug found for the Lender node:\n${JSON.stringify(
          edge.node,
          null,
          2
        )}`
      );
      break;
    }

    const path = `/lenders/${slug}/`;

    if (!isProd()) {
      reporter.info(`Creating Lender page: ${path}`);
    }

    actions.createPage({
      path,
      component: resolve('./src/templates/Lender.tsx'),
      context: {
        id: edge.node.id ?? `lender-${slug}`,
      },
    });
  }
};
