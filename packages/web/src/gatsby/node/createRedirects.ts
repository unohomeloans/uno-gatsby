import { CreatePageArgs, isProd } from '../../utils/createPagesUtil';

export interface RedirectNode {
  fromPath: string;
  toPath: string;
  statusCode: string;
}

interface RedirectsGraphQL {
  allSanityRedirects: {
    nodes: RedirectNode[];
  };
}

export default async (
  { actions, graphql, reporter }: CreatePageArgs,
  internalRedirections: RedirectNode[]
): Promise<void> => {
  const result = await graphql<RedirectsGraphQL>(`
    {
      allSanityRedirects {
        nodes {
          fromPath
          toPath
          statusCode
        }
      }
    }
  `);

  if (result.errors || !result.data) {
    throw result.errors || new Error('No data found ');
  }

  const redirectNodes = (result.data.allSanityRedirects || {}).nodes || [];

  if (!redirectNodes || redirectNodes.length === 0) {
    reporter.error('No redirects found');
    return;
  }

  for (const node of redirectNodes) {
    if (!node.fromPath || node.fromPath.trim().length === 0) {
      reporter.error(
        `No fromPath found in current Redirect:\n${JSON.stringify(
          node,
          null,
          2
        )}`
      );
      continue;
    } else if (!node.toPath || node.toPath.trim().length === 0) {
      reporter.error(
        `No toPath found in current Redirect:\n${JSON.stringify(node, null, 2)}`
      );
      continue;
    }

    if (!isProd()) {
      reporter.info(
        `Creating redirect page from ${node.fromPath} to ${
          node.toPath
        } - redirect type ${node.statusCode || ''}`
      );
    }

    if (node.fromPath.includes('/#')) {
      reporter.info(
        `Appending to internal redirection ${node.fromPath} to ${node.toPath}`
      );
      internalRedirections.push(node);
    }

    actions.createRedirect({
      fromPath: node.fromPath,
      toPath: node.toPath,
      statusCode: node.statusCode ? parseInt(node.statusCode, 10) : 300,
      force: node.statusCode == '301' && true,
    });
  }
};
