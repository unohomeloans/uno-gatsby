import { resolve } from 'path';
import { CreatePageArgs, isProd } from '../../utils/createPagesUtil';

interface CategoryPagesGraphQL {
  allSanityCategory: {
    nodes: {
      id: string;
      slug: {
        current: string;
      };
      title: string;
    }[];
  };
}

export default async ({
  actions,
  graphql,
  reporter,
}: CreatePageArgs): Promise<void> => {
  const result = await graphql<CategoryPagesGraphQL>(`
    {
      allSanityCategory {
        nodes {
          title
          slug {
            current
          }
          id
        }
      }
    }
  `);

  if (result.errors || !result.data) {
    throw result.errors || new Error('No data found');
  }

  const categoryNodes = (result.data.allSanityCategory || {}).nodes || [];

  if (!categoryNodes || categoryNodes.length === 0) {
    reporter.warn('No category pages found');
    return;
  }

  for (const node of categoryNodes) {
    if (!node.slug) {
      reporter.info(
        `No slug found for the current Category node:\n${JSON.stringify(
          node,
          null,
          2
        )}`
      );
      continue;
    }

    const slug = node.slug.current;

    if (!slug) {
      reporter.info(
        `No current slug found for current Category node:\n${JSON.stringify(
          node,
          null,
          2
        )}`
      );
      continue;
    }

    const path = `/categories/${slug}/`;

    if (!isProd()) {
      reporter.info(`Creating Category page: ${path}`);
    }

    actions.createPage({
      path,
      component: resolve('./src/templates/Category.tsx'),
      context: {
        id: node.id ?? `categories-${slug}`,
      },
    });
  }
};
