import { resolve } from 'path';
import { CreatePageArgs, isProd } from '../../utils/createPagesUtil';

interface BrokerPagesGraphQL {
  allSanityBroker: {
    edges: {
      node: {
        id: string;
        content: {
          main: {
            slug: {
              current: string;
            };
          };
        };
      };
    }[];
  };
}

export default async ({
  pathPrefix,
  actions,
  graphql,
  reporter,
}: CreatePageArgs): Promise<void> => {
  const { createPage } = actions;

  const result = await graphql<BrokerPagesGraphQL>(
    `
      {
        allSanityBroker {
          edges {
            node {
              id
              content {
                main {
                  slug {
                    current
                  }
                }
              }
            }
          }
        }
      }
    `
  );

  if (result.errors || !result.data) {
    throw result.errors || new Error('No data found');
  }

  const brokerEdges = (result.data.allSanityBroker || {}).edges || [];

  if (!brokerEdges || brokerEdges.length === 0) {
    reporter.info('No brokers found');
    return;
  }

  const brokerTemplate = resolve(`./src/templates/Broker.tsx`);

  for (const edge of brokerEdges) {
    if (!edge.node) {
      reporter.info('No node found for this Broker page node');
      break;
    } else if (!edge.node.content.main.slug) {
      reporter.info(
        `No slug found for this Broker page node: ${JSON.stringify(
          edge.node,
          null,
          2
        )}`
      );
      break;
    }

    const path = `${pathPrefix}${edge.node.content.main.slug.current}/`;

    if (!isProd()) {
      reporter.info(`Creating Broker page: ${path}`);
    }

    createPage({
      path,
      component: brokerTemplate,
      context: {
        id: edge.node.id,
      },
    });
  }
};
