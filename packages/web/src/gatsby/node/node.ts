import fs from 'fs';
import { config } from 'dotenv';
import { CreateResolversArgs, GatsbyNode } from 'gatsby';
import createCategoryPages from './createCategoryPages';
import createLandingPages from './createLandingPages';
import createNewsRoomPostPages from './createNewsRoomPostPages';
import createSubLandingPages from './createSubLandingPages';
import createRedirects, { RedirectNode } from './createRedirects';
import createLenderPages from './createLenderPages';
import createBrokerPages from './createBrokerPages';

config({ path: `.env.${process.env.NODE_ENV || 'development'}` });

const internalRedirections: RedirectNode[] = [];

export const createPages: GatsbyNode['createPages'] = async (args) => {
  await createLandingPages({ ...args, pathPrefix: '/' });
  await createSubLandingPages({ ...args });
  await createNewsRoomPostPages({ ...args });
  await createBrokerPages({ ...args, pathPrefix: '/' });
  await createLenderPages({ ...args });
  await createCategoryPages({ ...args });
  await createRedirects({ ...args }, internalRedirections);
};

export const createResolvers: GatsbyNode['createResolvers'] = async ({
  createResolvers,
}: CreateResolversArgs) => {
  const resolvers = {
    SanityCategory: {
      posts: {
        type: ['SanityPost'],
        resolve(source: any, _: any, context: any) {
          return context.nodeModel.runQuery({
            type: 'SanityPost',
            query: {
              filter: {
                categories: {
                  elemMatch: {
                    _id: {
                      eq: source._id,
                    },
                  },
                },
              },
            },
          });
        },
      },
    },
  };

  createResolvers(resolvers);
};

export const onPostBootstrap: GatsbyNode['onPostBootstrap'] = async ({
  reporter,
}) => {
  const internalRedirectionsJSON = JSON.stringify(internalRedirections);
  reporter.info('Adding internal redirections ' + internalRedirectionsJSON);
  fs.writeFile('./internalRedirections.json', internalRedirectionsJSON, () => {
    reporter.info('Added internal redirections!');
  });
};
