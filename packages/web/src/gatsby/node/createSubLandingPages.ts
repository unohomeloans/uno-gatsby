import { resolve } from 'path';
import { CreatePageArgs, isProd } from '../../utils/createPagesUtil';

interface SubLandingPagesGraphQL {
  allSanityRoute: {
    edges: {
      node: {
        id: string;
        slug: {
          current: string;
        };
        _rawParent: any;
      };
    }[];
  };
}

export default async ({
  pathPrefix,
  actions,
  graphql,
  reporter,
}: CreatePageArgs): Promise<void> => {
  const result = await graphql<SubLandingPagesGraphQL>(`
    {
      allSanityRoute(
        filter: { slug: { current: { ne: null } }, page: { id: { ne: null } } }
      ) {
        edges {
          node {
            id
            slug {
              current
            }
            _rawParent(resolveReferences: { maxDepth: 10 })
          }
        }
      }
    }
  `);

  if (result.errors || !result.data) {
    throw result.errors || new Error('No data found ');
  }

  const subRouteEdges = (result.data.allSanityRoute || {}).edges || [];

  if (!subRouteEdges || subRouteEdges.length === 0) {
    reporter.info('No sub landing pages found');
    return;
  }

  for (const edge of subRouteEdges) {
    if (!edge.node) {
      reporter.info('No node found');
      break;
    } else if (!edge.node.slug) {
      reporter.info(`No slug found for this node: ${edge.node}`);
      break;
    } else if (!edge.node.slug.current) {
      reporter.info('No current slug found');
      break;
    }

    const parentSlug = edge.node._rawParent
      ? '/' + edge.node._rawParent.content.main.slug.current
      : '';

    const path = `${parentSlug}/${edge.node.slug.current}/`;

    if (!isProd()) {
      reporter.info(`Creating sublanding page: ${path}`);
    }

    actions.createPage({
      path,
      component: resolve('./src/templates/SubPage.tsx'),
      context: {
        id: edge.node.id,
      },
    });
  }
};
