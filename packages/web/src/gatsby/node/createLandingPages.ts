import { resolve } from 'path';
import { CreatePageArgs, isProd } from '../../utils/createPagesUtil';

interface LandingPagesGraphQL {
  allSanityRoute: {
    edges: {
      node: {
        id: string;
        slug: {
          current: string;
        };
      };
    }[];
  };
}

export default async ({
  pathPrefix,
  actions,
  graphql,
  reporter,
}: CreatePageArgs): Promise<void> => {
  const result = await graphql<LandingPagesGraphQL>(`
    {
      allSanityRoute(
        filter: { slug: { current: { ne: null } }, page: { id: { ne: null } } }
      ) {
        edges {
          node {
            id
            slug {
              current
            }
          }
        }
      }
    }
  `);

  if (result.errors || !result.data) {
    throw result.errors || new Error('No data found');
  }

  const routeEdges = (result.data.allSanityRoute || {}).edges || [];

  if (!routeEdges || routeEdges.length === 0) {
    reporter.info('No landing pages found');
    return;
  }

  for (const edge of routeEdges) {
    if (!edge.node) {
      reporter.info('No node found for this Landing page node');
      break;
    } else if (!edge.node.slug) {
      reporter.info(
        `No slug found for this Landing page node: ${JSON.stringify(
          edge.node,
          null,
          2
        )}`
      );
      break;
    }

    const slug = edge.node.slug.current;

    if (!slug) {
      reporter.info(
        `No current slug found for this Landing page node: ${JSON.stringify(
          edge.node,
          null,
          2
        )}`
      );
      break;
    }

    const path = pathPrefix + slug + '/';

    if (!isProd()) {
      reporter.info(`Creating landing page: ${path}`);
    }

    actions.createPage({
      path,
      component: resolve('./src/templates/Page.tsx'),
      context: {
        id: edge.node.id ?? `landing-${slug}`,
      },
    });
  }
};
