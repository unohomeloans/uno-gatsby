import { resolve } from 'path';
import { CreatePageArgs, isProd } from '../../utils/createPagesUtil';

interface NewsRoomPostPagesGraphQL {
  allSanityPost: {
    edges: {
      node: {
        id: string;
        slug: {
          current: string;
        };
      };
    }[];
  };
}

export default async ({
  actions,
  graphql,
  reporter,
}: CreatePageArgs): Promise<void> => {
  const result = await graphql<NewsRoomPostPagesGraphQL>(`
    {
      allSanityPost(filter: { slug: { current: { ne: null } } }) {
        edges {
          node {
            id
            slug {
              current
            }
          }
        }
      }
    }
  `);

  if (result.errors || !result.data) {
    throw result.errors || new Error('No data found ');
  }

  const postEdges = (result.data.allSanityPost || {}).edges || [];

  if (!postEdges || postEdges.length === 0) {
    reporter.error('No news room posts found');
    return;
  }

  for (const edge of postEdges) {
    if (!edge.node) {
      reporter.error('No node found for the current News Room Post edge');
      break;
    } else if (!edge.node.slug) {
      reporter.error(
        `No slug found for the current News Room Post edge:\n${JSON.stringify(
          edge.node,
          null,
          2
        )}`
      );
      break;
    }

    const slug = edge.node.slug.current;

    if (!slug) {
      reporter.error(
        `No current slug found for the current News Room Post edge:\n${JSON.stringify(
          edge.node,
          null,
          2
        )}`
      );
      break;
    }

    const path = `/${slug}/`;

    if (!isProd()) {
      reporter.info(`Creating News Room post: ${path}`);
    }

    actions.createPage({
      path,
      component: resolve('./src/templates/Post.tsx'),
      context: {
        id: edge.node.id ?? `newsroom-post-${slug}`,
      },
    });
  }
};
