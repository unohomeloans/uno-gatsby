export default (): void => {
  let scrollPos = document.documentElement.scrollTop + window.innerHeight;
  document.addEventListener('scroll', () => {
    if (document.documentElement.scrollTop > scrollPos) {
      scrollPos = document.documentElement.scrollTop;
    }
  });
  window.addEventListener('beforeunload', () => {
    const maxScrollPos = document.body.offsetHeight;
    const depthRatio = scrollPos / maxScrollPos;
    (window as any).analytics.track('scroll', {
      depth: `${Math.round(depthRatio * 100)}%`,
      eventSource: 'clientSide',
      codeSource: 'gatsby',
      origin: window.location.href,
    });
  });
};
