export default (properties?: any): void => {
  (window as any).analytics.track(properties.trackName || 'click', {
    properties: {
      ...properties,
      path: window.location.pathname,
      referrer: document.referrer,
      origin: window.location.href,
      eventSource: 'clientSide',
      codeSource: 'gatsby',
    },
  });
};
