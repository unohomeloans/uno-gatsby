/* eslint-disable @typescript-eslint/no-explicit-any */
import { Actions, Reporter } from 'gatsby';

export interface CreatePageArgs {
  pathPrefix?: string;
  graphql: <TData, TVariables = any>(
    query: string,
    variables?: TVariables | undefined
  ) => Promise<{
    errors?: any;
    data?: TData | undefined;
  }>;
  actions: Actions;
  reporter: Reporter;
}

export const isProd = (): boolean => process.env.NODE_ENV === 'production';
