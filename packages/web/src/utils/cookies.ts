import Cookies, { CookieAttributes } from 'js-cookie';

export const UNO_REFERRER = 'uno-referrer';
export const UNO_DOMAIN = process.env.GATSBY_SITE_URL;
export const COOKIE_EXPIRES = 30;

export const setCookie = (
  name: string,
  value: string,
  options: CookieAttributes
): string | undefined => Cookies.set(name, value, options || {});

export const getCookie = (name: string, isJson = false): any => {
  const cookieContent = Cookies.get(name);
  return isJson && cookieContent ? JSON.parse(cookieContent) : cookieContent;
};

export const removeCookie = (name: string): void =>
  Cookies.remove(name, { path: '/', domain: '.unohomeloans.com.au' });
