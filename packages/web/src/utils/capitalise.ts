export default (term: string): string =>
  term.replace(/(^\w{1})|(\s{1}\w{1})/g, (match) => match.toUpperCase());
