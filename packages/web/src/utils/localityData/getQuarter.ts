export default (date: Date): number => Math.floor((date.getMonth() + 3) / 3);
