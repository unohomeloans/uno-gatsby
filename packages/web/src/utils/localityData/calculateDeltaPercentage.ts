export default (afterValue: number, beforeValue: number): number => {
  if (afterValue === 0 && beforeValue === 0) return 0;
  if (afterValue === 1 && beforeValue === 0) return 1;
  if (afterValue === 0 && beforeValue === 1) return -1;

  return ((afterValue - beforeValue) / beforeValue) * 100;
};
