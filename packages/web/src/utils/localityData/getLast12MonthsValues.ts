export default <T>(arr: T[]): T[] => {
  const values = arr.slice(Math.max(arr.length - 13, 1));
  values.pop();
  return values;
};
