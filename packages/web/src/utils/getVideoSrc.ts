interface Videos {
  [key: string]: string;
}

import homeVideo from '../video/home.mp4';

const VIDEOS: Videos = {
  home: homeVideo,
};

export default (text: any): string => {
  let videoText = '';

  text &&
    text.forEach(
      (textBlock: any) =>
        textBlock.children &&
        textBlock.children.forEach((textString: any) => {
          if (textString.text.includes('--video')) {
            videoText = textString.text;
          }
        })
    );

  return videoText ? VIDEOS[videoText.split('|')[1].replace(/--/g, '')] : '';
};
