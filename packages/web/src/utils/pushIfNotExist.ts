// eslint-disable-next-line @typescript-eslint/ban-types
const pushIfNotExist: <T extends {}>(inputArray: T[], element: T) => void = (
  inputArray,
  element
) => {
  if (!inputArray.includes(element)) inputArray.push(element);
};

export default pushIfNotExist;
