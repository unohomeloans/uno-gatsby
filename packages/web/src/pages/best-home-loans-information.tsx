import React from 'react';
import isNil from 'lodash/isNil';
import { Link, graphql } from 'gatsby';
import styled from 'styled-components';
import GraphQLErrorList from '../components/GraphQLErrorList';
import Layout from '../components/Layout';
import SEO from '../components/SEO';
import pxToRem from '../utils/pxToRem';
import defatulSocialImage from '../images/uno-shared-default.jpg';

const SuperHubHeading = styled.div`
  text-align: center;
  background: ${(props) => props.theme.colors.colorAccent};
  padding: ${pxToRem(150)} 0 ${pxToRem(83)} 0;

  h1 {
    color: ${(props) => props.theme.colors.colorNavy};
    max-width: 700px;
    margin: 0 auto;
  }
`;

const SuperHubWrapper = styled.div`
  margin: 0 auto ${pxToRem(40)};
  padding: ${pxToRem(24)} ${pxToRem(12)};
  border-radius: ${pxToRem(4)};
  background: ${(props) => props.theme.colors.colorAccent};

  @media (min-width: 768px) {
    max-width: ${pxToRem(920)};
    padding: ${pxToRem(32)} ${pxToRem(24)};
  }
`;

const ContentWrapper = styled.div`
  padding-top: ${pxToRem(150)};
  padding-bottom: ${pxToRem(130)};
`;

const SuperHubWrapperItem = styled.div`
  position: relative;
  padding: 0 0 ${pxToRem(20)} ${pxToRem(20)};
  display: block;

  @media (min-width: 768px) {
    padding: 0 0 ${pxToRem(20)} ${pxToRem(25)};
  }

  &:before {
    content: '';
    position: absolute;
    left: 0;
    top: ${pxToRem(7)};
    height: ${pxToRem(8)};
    width: ${pxToRem(8)};
    background: ${(props) => props.theme.colors.colorNavy};
    border-radius: 50%;
  }
`;

export const query = graphql`
  query SuperHubQuery {
    posts: allSanityPost(sort: { fields: [publishedAt], order: DESC }) {
      nodes {
        title
        id
        publishedAt
        slug {
          current
        }
      }
    }
  }
`;

const SuperHub = (props: any) => {
  const { data, errors, idSelector } = props;

  const idPrefix = !isNil(idSelector)
    ? idSelector
    : `best-home-loans-information`;

  if (errors) {
    return (
      <Layout>
        <GraphQLErrorList errors={errors} />
      </Layout>
    );
  }

  const postNodes = data.posts.nodes.map((post: any) => ({
    ...post,
  }));

  return (
    <Layout headerBackground="#EBF0F4">
      <SuperHubHeading id={`${idPrefix}-wrapper-header`}>
        <h1>Every article we’ve ever written</h1>
      </SuperHubHeading>
      <div className="container">
        <ContentWrapper className="disclaimerWrapperContainer">
          <p>Ah, home loans. They’re wonderful things. Right?</p>
          <p>
            Well, many people would describe them as a huge pile of debt that
            takes a lifetime to repay. But in the way we see the world, a
            mortgage can be your friend. With the right rate and the right
            structure, a home loan can be the vehicle that allows you to grow
            your wealth, build a stable home for your family, and change the
            paint colour of that nasty ‘70s bathroom you’ve been enduring as a
            renter for the past 4 years.
          </p>
          <p>
            But as much as we’re home loan cheerleaders here at uno, once you
            get yourself into the right loan, you want to get rid of it as soon
            as possible. And by that we mean, you want to pay it down as quickly
            as you can so you can live out your days blissfully and smugly
            mortgage-free. Refinancing to a lower interest rate or a better loan
            structure can – along with a bit of old-fashioned savings,
            discipline and cleverness – help you do just that.
          </p>
          <p>
            As a catch-all, every article we’ve written on the topic is linked
            from this page. Think of it as the ultimate all-you-can-read buffet
            of property and mortgage content.
          </p>
          <p>
            If you’re looking for our most popular pieces, you can jump straight
            to our <Link to={'/calculators/'}>Mortgage Calculators</Link> or our
            guides for <Link to={'/first-home-buyer/'}>First Home Buyers</Link>,{' '}
            <Link to={'/refinance-home-loan/'}>Refinancers</Link>,{' '}
            <Link to={'/buying/'}>Property Buyers</Link>, and{' '}
            <Link to={'/investing/'}>Investors</Link>. Our blog home page has
            all of our latest and greatest pieces, too.
          </p>
          <SuperHubWrapper id={`${idPrefix}-wrapper-content`}>
            {postNodes.map((post: any) => (
              <SuperHubWrapperItem key={post.id}>
                {post.slug && post.slug && (
                  <Link to={post.slug.current}>{post.title}</Link>
                )}
              </SuperHubWrapperItem>
            ))}
          </SuperHubWrapper>
        </ContentWrapper>
      </div>
    </Layout>
  );
};

export default SuperHub;

export const Head = () => (
  <SEO
    title="Every article we've ever written"
    description="Find our Mortgage Calculators or guides for First Home Buyers, Refinancers, Property Buyers, and Investors. Our blog home page has all of our latest and greatest pieces, too."
    meta={[
      {
        property: 'og:image',
        content: defatulSocialImage,
      },
    ]}
  />
);
