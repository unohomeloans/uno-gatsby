import React from 'react';
import { graphql } from 'gatsby';
import Errors from '../components/Errors';
import Page from '../templates/Page';
import defatulSocialImage from '../images/uno-shared-default.jpg';
import SEO from '../components/SEO';

export const query = graphql`
  query IndexPageQuery {
    page: sanityPage(_id: { regex: "/(drafts.|)frontpage/" }) {
      ...PageInfo
    }
    site: sanitySiteSettings(_id: { regex: "/(drafts.|)siteSettings/" }) {
      title
      description
      _rawDisclaimer(resolveReferences: { maxDepth: 10 })
    }
  }
`;

const Index = (props: any) => {
  const { data, errors } = props;

  if (errors) {
    return <Errors errors={errors} />;
  }

  return <Page data={data} />;
};

export default Index;

export const Head = (props: any) => {
  const { data } = props;
  const page = data.page || data.route.page;

  const pageTitle =
    page.content && page.content.meta && page.content.meta.metaTitle
      ? page.content.meta.metaTitle
      : page.title;
  const pageDescription = data.route && page.content.meta.metaDescription;
  const getSocialImage = page.content.meta?.openImage?.asset?.gatsbyImageData
    ? page.content.meta.openImage.asset.gatsbyImageData.images.fallback.src
    : defatulSocialImage;

  const checkForFeedbackComponent = (page._rawContent.main.modules || [])
    .filter((c: any) => !c.disabled)
    .map((c: any) => c._type)
    .filter((val: any) => val == 'customerFeedback');

  return (
    <SEO
      title={pageTitle}
      description={pageDescription}
      meta={[
        {
          property: 'og:image',
          content: getSocialImage,
        },
        {
          name: 'referrer',
          content: 'unsafe-url',
        },
      ]}
      {...(checkForFeedbackComponent.length
        ? {
            script: [
              {
                src:
                  'https://cdn.productreview.com.au/assets/widgets/loader.js',
              },
            ],
          }
        : Object.create(null))}
    />
  );
};
