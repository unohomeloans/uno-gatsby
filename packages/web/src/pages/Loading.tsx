import React, { useEffect } from 'react';

import '../styles/scss/main.scss';
import Layout from '../components/Layout';

const Loading = ({ redirectLink }: any) => {
  useEffect(() => {
    window.location.href = redirectLink;
    (window as any).redirectLink = '';
  }, [redirectLink]);

  return (
    <Layout>
      <div className="loadingWrapper">
        <div className="centeredWrapper">
          <h1>Welcome to uno</h1>
          <p>Please wait...</p>
        </div>
      </div>
    </Layout>
  );
};

export default Loading;
