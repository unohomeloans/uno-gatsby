import React from 'react';
import { graphql, useStaticQuery, Link } from 'gatsby';
import styled from 'styled-components';

const HeaderImg = styled.img`
  width: 100%;
  height: 32.06px;

  @media (max-width: 768px) {
    width: 100%;
    height: 20px;
  }
`;

const HeaderLogo: React.FC = () => {
  const data = useStaticQuery(graphql`
    {
      sanitySiteSettings {
        logo {
          asset {
            url
          }
          alt
        }
      }
    }
  `);

  return (
    <div className="header-brand">
      <div className="header-brand--logo">
        <Link to="/" data-url="/">
          <HeaderImg
            src={data.sanitySiteSettings?.logo?.asset.url}
            alt={data.sanitySettings?.logo?.alt}
          />
        </Link>
      </div>
    </div>
  );
};

export default HeaderLogo;
