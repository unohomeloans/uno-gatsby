import React, { useRef, useState } from 'react';
import {
  ThumbsWrapper,
  ThumbsButton,
  ThumbsItems,
  ThumbsTitle,
} from './Thumbs.styled';
import { USERID, THUMBSUPURL, THUMBSDOWNURL } from './Thumbs.constants';
import segment from '../../utils/segment';

const Thumbs: React.FC = () => {
  const ThumbsDownRef = useRef();
  const ThumbsUpRef = useRef();

  const [like, setLike] = useState(false);
  const [dislike, setDisLike] = useState(false);

  const thumbsUpLink = (e: any) => {
    e.preventDefault();
    const encodedUrl = encodeURIComponent(window.location.href);
    const url = `${THUMBSUPURL}?url=${encodedUrl}&user_id=${USERID}`;
    setLike(!like);
    segment('track', {
      trackName: 'click',
      text: 'Satisfied',
      elementType: 'button',
      elementID: 'thumbs-up',
      colour: 'primary',
      destination: url,
    });
    window.setTimeout(() => {
      window.open(url, '_blank');
    }, 1000);
  };

  const thumbsDownLink = (e: any) => {
    e.preventDefault();
    const encodedUrl = encodeURIComponent(window.location.href);
    const url = `${THUMBSDOWNURL}?url=${encodedUrl}&user_id=${USERID}`;

    setDisLike(!dislike);
    segment('track', {
      text: 'Unsatisfied',
      elementType: 'button',
      elementID: 'thumbs-down',
      colour: 'primary',
      destination: url,
    });
    window.setTimeout(() => {
      window.open(url, '_blank');
    }, 1000);
  };

  return (
    <>
      <ThumbsWrapper>
        <ThumbsTitle id="thumbs-title">Was this helpful?</ThumbsTitle>
        <ThumbsItems>
          <ThumbsButton
            id="thumbs-up"
            ref={ThumbsUpRef.current}
            className={like ? 'liked' : ''}
            onClick={thumbsUpLink}
          >
            <div className="hand">
              <div className="thumb"></div>
            </div>
          </ThumbsButton>
          <ThumbsButton
            id="thumbs-down"
            ref={ThumbsDownRef.current}
            className={dislike ? 'down disliked' : 'down'}
            onClick={thumbsDownLink}
          >
            <div className="hand down">
              <div className="thumb"></div>
            </div>
          </ThumbsButton>
        </ThumbsItems>
      </ThumbsWrapper>
    </>
  );
};

export default Thumbs;
