import styled from 'styled-components';
import pxToRem from '../../utils/pxToRem';

export const ThumbsWrapper = styled.div`
  padding: ${(props) => props.theme.spacing.spacingFour} 0;

  @media (min-width: 768px) {
    padding: ${(props) => props.theme.spacing.spacingEight} 0;
  }
`;

export const ThumbsTitle = styled.h2`
  font-family: 'Nunito Sans', sans-serif;
  font-size: ${(props) => props.theme.text.textFive};
  line-height: 3.125rem;
  text-align: center;
  @media (min-width: 768px) {
    font-size: 2rem;
  }
`;

export const ThumbsItems = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const ThumbsButton = styled.button`
  --color: #1e2235;
  --color-hover: #1e2235;
  --color-active: #fff;
  --icon: #14b2a1;
  --icon-hover: #fff;
  --icon-active: #fff;
  --background: #e7f7f5;
  --background-hover: #14b2a1;
  --background-active: #66d228;
  --background-dislike-active: #939fa9;
  --border: #d1efec;
  --border-active: transparent;
  --shadow: #{rgba(#001177, 0.025)};
  display: block;
  outline: none;
  cursor: pointer;
  position: relative;
  border: none;
  background: none;
  border-radius: ${pxToRem(8)};
  line-height: 27px;
  font-family: inherit;
  font-weight: 600;
  font-size: 14px;
  color: var(--color);
  -webkit-appearance: none;
  -webkit-tap-highlight-color: transparent;
  transition: color 0.2s linear;
  margin: 0 10px;
  padding: ${pxToRem(18)} ${pxToRem(30)} ${pxToRem(28)} ${pxToRem(34)};
  @media (min-width: 768px) {
    padding: ${pxToRem(28)} ${pxToRem(40)} ${pxToRem(38)} ${pxToRem(44)};
    margin: 0 20px;
  }

  &:hover {
    --icon: var(--icon-hover);
    --color: var(--color-hover);
    --background: var(--background-hover);
    --border-width: 2px;
  }
  &:active {
    --scale: 0.95;
  }
  &:not(.liked) {
    &:hover {
      --hand-rotate: 8;
      --hand-thumb-1: -12deg;
      --hand-thumb-2: 36deg;
    }
  }
  &:not(.disliked) {
    &:hover {
      --hand-rotate: 8;
      --hand-thumb-1: -12deg;
      --hand-thumb-2: 36deg;
    }
  }
  &.liked {
    --span-x: 2px;
    --span-d-o: 1;
    --span-d-x: 0;
    --icon: var(--icon-active);
    --color: var(--color-active);
    --border: var(--border-active);
    --background: var(--background-active);
  }
  &.disliked {
    --span-x: 2px;
    --span-d-o: 1;
    --span-d-x: 0;
    --icon: var(--icon-active);
    --color: var(--color-active);
    --border: var(--border-active);
    --background: var(--background-dislike-active);
  }
  &:before {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    right: 0;
    bottom: 0;
    border-radius: inherit;
    transition: background 0.2s linear, transform 0.2s, box-shadow 0.2s linear;
    transform: scale(var(--scale, 1)) translateZ(0);
    background: var(--background);
    box-shadow: inset 0 0 0 var(--border-width, 1px) var(--border),
      0 4px 8px var(--shadow), 0 8px 20px var(--shadow);
  }
  .hand {
    width: 11px;
    height: 11px;
    border-radius: 2px 0 0 0;
    background: var(--icon);
    position: relative;
    margin: 10px 8px 0 0;
    transform-origin: 0 -1px;
    transition: transform 0.25s, background 0.2s linear;
    transform: rotate(calc(var(--hand-rotate, 0) * 1deg)) translateZ(0)
      scale(1.5);
    @media (min-width: 768px) {
      transform: rotate(calc(var(--hand-rotate, 0) * 1deg)) translateZ(0)
        scale(2);
    }
    &:before,
    &:after {
      content: '';
      background: var(--icon);
      position: absolute;
      transition: background 0.2s linear, box-shadow 0.2s linear;
    }
    &:before {
      left: -5px;
      bottom: 0;
      height: 12px;
      width: 4px;
      border-radius: 1px 1px 0 1px;
    }
    &:after {
      right: -3px;
      top: 0;
      width: 4px;
      height: 4px;
      border-radius: 0 2px 2px 0;
      background: var(--icon);
      box-shadow: -0.5px 4px 0 var(--icon), -1px 8px 0 var(--icon),
        -1.5px 12px 0 var(--icon);
      transform: scaleY(0.6825);
      transform-origin: 0 0;
    }
    .thumb {
      background: var(--icon);
      width: 10px;
      height: 4px;
      border-radius: 2px;
      transform-origin: 2px 2px;
      position: absolute;
      left: 0;
      top: 0;
      transition: transform 0.25s, background 0.2s linear;
      transform: scale(0.85) translateY(-0.5px)
        rotate(var(--hand-thumb-1, -45deg)) translateZ(0);
      &:before {
        content: '';
        height: 4px;
        width: 7px;
        border-radius: 2px;
        transform-origin: 2px 2px;
        background: var(--icon);
        position: absolute;
        left: 7px;
        top: 0;
        transition: transform 0.25s, background 0.2s linear;
        transform: rotate(var(--hand-thumb-2, -45deg)) translateZ(0);
      }
    }
    &.down {
      transform: rotate(calc(var(--hand-rotate, 0) * 1deg)) translateZ(0)
        scale(-1.5, -1.5);
      transform-origin: 5px;

      @media (min-width: 768px) {
        transform: rotate(calc(var(--hand-rotate, 0) * 1deg)) translateZ(0)
          scale(-2, -2);
      }
    }
    &.hand {
      display: inline-block;
      vertical-align: top;
    }
  }
  .hand {
    display: inline-block;
    vertical-align: top;
  }
`;
