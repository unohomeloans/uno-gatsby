import React, { useEffect, useState } from 'react';
import isNil from 'lodash/isNil';
import useRouter from '../../hooks/useRouter';
import removeAccents from '../../utils/removeAccents';
import segment from '../../utils/segment';

const CTAButton = (props: any) => {
  const LS = process.env.GATSBY_MY_SANITY_LS;
  const CV2 = process.env.GATSBY_MY_SANITY_CV2;

  const router = useRouter();

  const internalLink = props.title;
  const link = props.linkedPage || props.link || props.applink || '#';

  const idPrefix = !isNil(props.idSelector) ? props.idSelector : `ph-hero-cta`;

  const withAppend = !isNil(link.append) ? link.append : '';
  const withRouter = !isNil(router.search) ? router.search : '';

  const [clicked, setClicked] = useState(false);
  const [clickedAppllicationCV2, setClickedAppllicationCV2] = useState(false);
  const [clickedAppllicationLS, setClickedAppllicationLS] = useState(false);

  useEffect(() => {
    if (clicked) {
      // do something meaningful, Promises, if/else, whatever, and then
      window.location.assign(`${link.link}${withRouter}`);
    }
    if (clickedAppllicationCV2) {
      // do something meaningful, Promises, if/else, whatever, and then
      window.location.assign(`${CV2}${withAppend}${withRouter}`);
    }
    if (clickedAppllicationLS) {
      // do something meaningful, Promises, if/else, whatever, and then
      window.location.assign(`${LS}${withAppend}${withRouter}`);
    }
  });

  const hancleClickWithRouting = () => {
    setClicked(true);
    segment('track', {
      trackName: 'click',
      text: link.title,
      elementType: 'button',
      elementID: `button-${removeAccents(link.title.toLowerCase())}`,
      colour: 'primary',
      destination: link.link,
    });
  };

  const hancleClickWithApplicationCV2 = () => {
    setClickedAppllicationCV2(true);
    segment('track', {
      trackName: 'click',
      text: link.title,
      elementType: 'button',
      elementID: `button-${removeAccents(link.title.toLowerCase())}`,
      colour: 'primary',
      destination: `${CV2}${withAppend}`,
    });
  };

  const hancleClickWithApplicationLS = () => {
    setClickedAppllicationLS(true);
    segment('track', {
      trackName: 'click',
      text: link.title,
      elementType: 'button',
      elementID: `button-${removeAccents(link.title.toLowerCase())}`,
      colour: 'primary',
      destination: `${LS}${withAppend}`,
    });
  };

  const hancleClickNoRouting = () => {
    segment('track', {
      trackName: 'click',
      text: link.title,
      elementType: 'button',
      elementID: `button-${removeAccents(internalLink.toLowerCase())}`,
      colour: 'primary',
      destination: `${window.location.origin}/${link.content.main.slug.current}`,
    });
  };

  if (props.kind === 'button' && link._type === 'externalLink') {
    return (
      <button
        data-button={link.title}
        id={`${idPrefix}-item-${link._type}-${removeAccents(link.title)}`}
        onClick={hancleClickWithRouting}
        className="button button--md button--height--md button--primary button--rounded"
        aria-label={link.title}>
        {link.title}
      </button>
    );
  }

  if (props.kind === 'button' && link._type === 'page') {
    return (
      <a
        id={`${idPrefix}-item-${link._type}-${removeAccents(internalLink)}`}
        data-link={internalLink}
        className="button button--md button--height--md button--primary button--rounded"
        href={`/${link.content.main.slug.current}/`}
        aria-label={internalLink}
        onClick={hancleClickNoRouting}>
        {props.title}
      </a>
    );
  }

  if (props.kind === 'button' && link._type === 'post') {
    return (
      <a
        id={`${idPrefix}-item-${link._type}-${removeAccents(internalLink)}`}
        data-link={internalLink}
        className="button button--md button--height--md button--primary button--rounded"
        href={`${link.slug.current}/`}
        aria-label={internalLink}
        onClick={hancleClickNoRouting}>
        {internalLink}
      </a>
    );
  }

  if (
    props.applink &&
    props.kind === 'button' &&
    props.applink.select === 'LS'
  ) {
    return (
      <button
        data-button={link.title}
        id={`${idPrefix}-item-${link._type}-${removeAccents(link.title)}`}
        onClick={hancleClickWithApplicationLS}
        className="button button--md button--height--md button--primary button--rounded"
        aria-label={link.title}
        {...{ referrerpolicy: 'unsafe-url' }}>
        {link.title}
      </button>
    );
  }

  if (
    props.applink &&
    props.kind === 'button' &&
    props.applink.select === 'CV2'
  ) {
    return (
      <button
        data-button={link.title}
        id={`${idPrefix}-item-${link._type}-${removeAccents(link.title)}`}
        onClick={hancleClickWithApplicationCV2}
        className="button button--md button--height--md button--primary button--rounded"
        aria-label={link.title}
        {...{ referrerpolicy: 'unsafe-url' }}>
        {link.title}
      </button>
    );
  }

  return null;
};

export default CTAButton;
