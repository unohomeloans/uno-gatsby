import styled from 'styled-components';
import pxToRem from '../../utils/pxToRem';

export const StyledHeaderWrapper = styled.div.attrs(
  (props: { $headerBackground?: string }) => props
)`
  display: flex;
  justify-content: center;
  z-index: 1000;
  background: ${(props) => props.$headerBackground || '#fff'};
  width: 100%;

  @media (max-width: 992px) {
    justify-content: start;
  }
`;
export const StyledHeaderContainer = styled.div.attrs(
  (props: { $headerBackground?: string }) => props
)`
  header,
  ul,
  label {
    background: ${(props) => props.$headerBackground || '#fff'};
  }

  .menu-item--sub-item {
    border: 1px solid
      ${(props) => (props.$headerBackground ? '#b7c9d7' : '#f3f3f3')};
    &:before {
      background: ${(props) => props.$headerBackground || '#fff'};
      border-right: 1px solid
        ${(props) => (props.$headerBackground ? '#b7c9d7' : '#f3f3f3')};
      border-bottom: 1px solid
        ${(props) => (props.$headerBackground ? '#b7c9d7' : '#f3f3f3')};
    }
  }

  .menu-item a:hover,
  .menu-item button:hover,
  .header-ctas--item a:hover {
    color: ${(props) => props.theme.colors.colorNavy};
    background: ${(props) =>
      props.$headerBackground ? '#f6f8fa' : props.$headerBackground};
  }

  label {
    &:hover {
      background: #fff;
    }
  }

  @media (min-width: 1400px) {
    max-width: ${pxToRem(1360)};
    padding-left: 0;
    padding-right: 0;
  }

  @media (min-width: 992px) {
    margin: 0 auto;
    padding-left: 20px;
    padding-right: 20px;
  }
`;
