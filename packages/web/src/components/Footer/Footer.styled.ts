import styled from 'styled-components';
import pxToRem from '../../utils/pxToRem';

export const FooterContentWrappper = styled.div`
  display: flex;
  flex-direction: column;
  gap: 20px;

  margin: 0 0 ${pxToRem(10)};
  @media (min-width: 768px) {
    flex-direction: row;
  }
`;

export const FooterContentItem = styled.div`
  &:first-child {
    max-width: ${pxToRem(265)};

    @media (max-width: 768px) {
      margin: ${pxToRem(25)} 0 0;
      max-width: 100%;
    }
  }
`;

export const FooterGridContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(1, 2fr);
  gap: ${pxToRem(15)};
  grid-template-areas: '.';
  @media (min-width: 768px) {
    grid-template-columns: 1fr 1fr 1fr;
    grid-template-areas: '. . .';
  }
`;

export const FooterGridContainerItem = styled.div``;

export const FooterGridContainerLabel = styled.h4`
  color: ${(props) => props.theme.colors.colorBlack};
  font-size: ${pxToRem(18)};
  margin: ${pxToRem(20)} 0 ${pxToRem(8)};
  letter-spacing: -0.3px;
  @media (min-width: 768px) {
    font-size: ${pxToRem(18)};
    margin: 0 0 ${pxToRem(9)};
  }
`;

export const FooterGridContainerList = styled.div``;

export const FooterGridContainerListItem = styled.div`
  display: inline-block;
  padding: 0 ${pxToRem(10)} ${pxToRem(8)};
  margin: 0;

  @media (min-width: 768px) {
    display: block;
    padding: 0 0 ${pxToRem(13)};
  }
`;

export const FooterGridContainerLink = styled.a`
  color: ${(props) => props.theme.colors.colorBlack};
  font-size: ${(props) => props.theme.text.textThree};
`;
