import React from 'react';
import BlockText from '../BlockText';
import {
  DisclaimerWrapper,
  DisclaimerWrapperContainer,
} from './Disclaimer.styled';

interface DisclaimerProps {
  _rawDisclaimer?: any;
}

const Disclaimer: React.FC<DisclaimerProps> = ({ _rawDisclaimer }) => (
  <>
    {_rawDisclaimer && (
      <DisclaimerWrapper>
        <div className="container">
          <DisclaimerWrapperContainer>
            <BlockText blocks={_rawDisclaimer} />
          </DisclaimerWrapperContainer>
        </div>
      </DisclaimerWrapper>
    )}
  </>
);

export default Disclaimer;
