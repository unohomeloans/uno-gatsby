import styled from 'styled-components';
import pxToRem from '../../utils/pxToRem';

export const DisclaimerWrapper = styled.div`
  padding: ${pxToRem(32)} 0 ${pxToRem(32)} 0;

  @media (min-width: 768px) {
    padding: ${pxToRem(32)} 0 ${pxToRem(48)} 0;
  }
`;

export const DisclaimerWrapperContainer = styled.div`
  max-width: ${pxToRem(770)};
  margin: 0 auto;

  @media (max-width: 768px) {
    padding: 0;
  }
`;
