/* eslint-disable react/display-name */
/* eslint-disable @typescript-eslint/no-var-requires */
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import isNil from 'lodash/isNil';
import BaseBlockContent from '@sanity/block-content-to-react';
import urlBuilder from '@sanity/image-url';
import styled from 'styled-components';
import getYouTubeId from 'get-youtube-id';
import YouTube from 'react-youtube';
import useRouter from '../../hooks/useRouter';
import pxToRem from '../../utils/pxToRem';
import removeAccents from '../../utils/removeAccents';
import RatesInterestRate from '../Rates/RatesInterestRate';
import RatesComparisonRate from '../Rates/RatesComparisonRate';
import segment from '../../utils/segment';
import randomAlphaNumeric from '../../utils/randomAlphaNumeric';
import {
  ArrowTopRight,
  Heart,
  Person,
  Trophy,
} from '../Icons/GeneralIcons/GeneralIcons';
import StarSVG from '../../images/svg/star.svg';
import ModalButton from '../ModalButton';
import CalendlyInlineWidget from '../CalendlyInlineWidget';
import { useExternalScript } from '../../hooks/useExternalScript';

const clientConfig = require('../../../config/site-config.js');

const ImageEmbeddedText = styled.div.attrs((props: { $pos?: string }) => props)`
  position: absolute;
  color: ${(props) => props.theme.colors.colorBlack};
  background: #fff;
  padding: ${pxToRem(14)} ${pxToRem(24)};
  border-radius: ${pxToRem(12)};
  ${(props) => {
    switch (props.$pos) {
      case 'top':
        return 'top: 10%;';
      case 'bottom':
        return 'bottom: 10%;';
      case 'left':
        return 'left: 10%;';
      case 'right':
        return 'right: 10%;';
      case 'topRight':
        return 'top: 10%; right: 10%;';
      case 'topLeft':
        return 'top: 10%; left: 10%;';
      case 'bottomLeft':
        return 'bottom: 10%; left: 10%;';
      default:
        return 'bottom: 10%; right: 10%;';
    }
  }}

  p {
    margin: 0;

    & strong {
      font-family: 'Nunito Sans', sans-serif;
    }
  }
`;

const Figure = styled.figure`
  position: relative;
`;

interface RenderListProps {
  list: any;
}

const ListItem = styled.div`
  display: flex;
  flex-direction: row;
  margin-bottom: 50px;

  &:last-of-type {
    margin-bottom: 0;
  }

  p {
    margin: 0;
  }
`;

const ListItemNumOrIcon = styled.div.attrs(
  (props: { $sz?: string; $bg?: string; $br?: string }) => props
)`
  display: flex;
  flex-shrink: 0;
  justify-content: center;
  align-items: center;
  font-size: ${pxToRem(48)};
  color: ${(props) => props.theme.colors.colorNavy};
  background: ${(props) => (props.$bg ? props.$bg : '#fff')};
  border-radius: ${(props) => (props.$br ? props.$br : 'none')};
  height: ${(props) => (props.$sz ? props.$sz : pxToRem(62))};
  width: ${(props) => (props.$sz ? props.$sz : pxToRem(62))};
  margin-right: ${pxToRem(32)};

  svg {
    height: 29.09px;
    width; auto;
  }
`;

const ListItemTitle = styled.div`
  font-size: ${pxToRem(24)};
  font-family: ${(props) => props.theme.text.textMedium};
  color: ${(props) => props.theme.colors.colorNavy};
  margin-bottom: ${pxToRem(8)};
  line-height: ${pxToRem(38.4)};
`;
const getIcon = (icon: string) => {
  switch (icon) {
    case 'arrowTopRight':
      return <ArrowTopRight svgClasses="svg-icn" color="white" />;
    case 'trophy':
      return <Trophy svgClasses="svg-icn" color="white" />;
    case 'person':
      return <Person svgClasses="svg-icn" color="white" />;
    case 'heart':
      return <Heart svgClasses="svg-icn" color="white" />;
    default:
      return null;
  }
};

const RenderList: React.FC<RenderListProps> = ({ list }) => {
  const mapList = useCallback(
    () =>
      list.content.map((val: any, index: number) => {
        return (
          <ListItem key={randomAlphaNumeric(6)}>
            <ListItemNumOrIcon $sz="91.90px" $bg="#284053" $br="50%">
              {!list?.type || list?.type == 'numbered'
                ? index > 9
                  ? index + 1
                  : `0${index + 1}`
                : null}
              {list?.type == 'unordered' && val?.icon
                ? getIcon(val.icon)
                : null}
            </ListItemNumOrIcon>
            <div>
              {val?.title ? <ListItemTitle>{val.title}</ListItemTitle> : null}
              <div>
                <BlockText blocks={val.content} />
              </div>
            </div>
          </ListItem>
        );
      }),
    [list]
  );
  return <>{mapList()}</>;
};

interface TitleIconProps {
  type: string | null;
  position: string | null;
  icon: string;
  title: string;
}

const TitleIcon: React.FC<TitleIconProps> = ({
  type,
  position,
  icon,
  title,
}) => {
  const getIcon = useCallback(() => {
    const classes =
      position == 'suffix' || !position ? 'icn-1  suffix' : 'icn-1 prefix';
    const color = !type || type == 'primary' ? 'white' : '#284053';
    switch (icon) {
      case 'arrowTopRight':
        return <ArrowTopRight svgClasses={classes} color={color} />;
      case 'trophy':
        return <Trophy svgClasses={classes} color={color} />;
      case 'person':
        return <Person svgClasses={classes} color={color} />;
      case 'heart':
        return <Heart svgClasses={classes} color={color} />;
      default:
        return null;
    }
  }, [icon, position]);
  return (
    <>
      {(position == 'suffix' || !position) && title}
      {getIcon()}
      {position == 'prefix' && title}
    </>
  );
};

const LinkLS = ({ node }: any) => {
  const router = useRouter();

  const LS = process.env.GATSBY_MY_SANITY_LS;
  const withAppend = !isNil(node.applink.append) ? node.applink.append : '';
  const withRouter = !isNil(router.search) ? router.search : '';

  const [clickedAppllicationLS, setClickedAppllicationLS] = useState(false);

  useEffect(() => {
    if (clickedAppllicationLS) {
      // do something meaningful, Promises, if/else, whatever, and then
      window.location.assign(`${LS}${withAppend}${withRouter}`);
    }
  });

  return (
    <button
      id={`${node.applink.title}-${removeAccents(
        node.applink.title.toLowerCase()
      )}`}
      {...{ referrerPolicy: 'unsafe-url' }}
      className={`button button--height--md button--md button--rounded ${
        node?.ctaIcon?.type == 'alternate'
          ? 'button--alternate'
          : 'button--primary'
      }`}
      onClick={() => {
        setClickedAppllicationLS(true);
        segment('track', {
          trackName: 'click',
          text: node.applink.title,
          elementType: 'button',
          elementID: `button-${removeAccents(node.applink.title)}`,
          colour: 'primary',
          destination: `${window.location.origin}/${LS}${withAppend}`,
        });
      }}
      data-link={node.applink.title}>
      {node?.ctaIcon?.icon ? (
        <TitleIcon
          title={node.applink.title}
          position={node.ctaIcon?.position || null}
          type={node.ctaIcon?.type || null}
          icon={node.ctaIcon.icon}
        />
      ) : (
        node.applink.title
      )}
    </button>
  );
};

const LinkCV2 = ({ node }: any) => {
  const router = useRouter();
  const CV2 = process.env.GATSBY_MY_SANITY_CV2;
  const withAppend = !isNil(node.applink.append) ? node.applink.append : '';
  const withRouter = !isNil(router.search) ? router.search : '';

  const [clickedAppllicationCV2, setClickedAppllicationCV2] = useState(false);

  useEffect(() => {
    if (clickedAppllicationCV2) {
      window.location.assign(`${CV2}${withAppend}${withRouter}`);
    }
  });

  return (
    <button
      id={`${node.applink.title}-${removeAccents(
        node.applink.title.toLowerCase()
      )}`}
      className={`button button--height--md button--md button--rounded ${
        node?.ctaIcon?.type == 'alternate'
          ? 'button--alternate'
          : 'button--primary'
      }`}
      {...{ referrerpolicy: 'unsafe-url' }}
      onClick={() => {
        setClickedAppllicationCV2(true);
        segment('track', {
          trackName: 'click',
          text: node.applink.title,
          elementType: 'button',
          elementID: `button-${removeAccents(node.applink.title)}`,
          colour: 'primary',
          destination: `${window.location.origin}/${CV2}${withAppend}`,
        });
      }}
      data-link={node.applink.title}>
      {node?.ctaIcon?.icon ? (
        <TitleIcon
          title={node.applink.title}
          type={node.ctaIcon?.type || null}
          position={node.ctaIcon?.position || null}
          icon={node.ctaIcon.icon}
        />
      ) : (
        node.applink.title
      )}
    </button>
  );
};

const Button = ({ node }: any) => {
  const router = useRouter();
  const [clicked, setClicked] = useState(false);

  useEffect(() => {
    if (clicked) {
      window.location.assign(`${node.externalLink.link}${router.search}`);
      segment('track', {
        trackName: 'click',
        text: node.externalLink.title,
        elementType: 'button',
        elementID: `button-${removeAccents(
          node.externalLink.title.toLowerCase()
        )}`,
        colour: 'primary',
        destination: node.externalLink.link,
      });
    }
  });

  return (
    <a
      id={`button-${node.externalLink._type}`}
      href={`${node.externalLink.link}/`}
      data-button={node.externalLink.title}
      className={`button button--height--md button--rounded ${
        node?.ctaIcon?.type == 'alternate'
          ? 'button--alternate'
          : 'button--primary'
      }`}
      onClick={() => setClicked(true)}>
      {node?.ctaIcon?.icon ? (
        <TitleIcon
          title={node.externalLink.title}
          type={node.ctaIcon?.type || null}
          position={node.ctaIcon?.position || null}
          icon={node.ctaIcon.icon}
        />
      ) : (
        node.externalLink.title
      )}
    </a>
  );
};

const ArticleFullImage = styled.div`
  left: 50%;
  margin-left: -50vw;
  margin-right: -50vw;
  max-width: 100vw;
  position: relative;
  right: 50%;
  width: 100vw;
  text-align: center;
`;

const ContentImageWrapper = styled.div.attrs(
  (props: { $roundedImageAccent: boolean | null }) => props
)`
  &:first-child {
    order: 0;
  }
  background: #ebf0f4;
  border-radius: 50%;
  overflow: hidden;
  height: 400px;
  width: 400px;

  @media (max-width: 992px) {
    height: 360px;
    width: 360px;
  }

  @media (max-width: 768px) {
    height: 325px;
    width: 325px !important;
  }

  img {
    margin-top: 10%;
  }
`;

const CustomTable = styled.table.attrs(
  (props: { $roundedImageAccent: boolean | null }) => props
)`
  tr {
    border-top: none;
    border-left: none;
    border-right: none;
    border-bottom: 1px solid ${(props) => props.theme.colors.colorAccent};
    th {
      border: none;
      color: ${(props) => props.theme.colors.colorNavy};
      font-size: ${(props) => props.theme.text.textFour};
    }
    td {
      border: none;
    }
  }
`;

const CustomBanner = styled.div`
  display: flex;
  border: 2px solid ${(props) => props.theme.colors.colorAccent};

  h2 {
    font-size: 32px;
    font-weight: 800;
  }

  .review-card {
    display: flex;
    gap: 24px;
    flex-direction: column;
    font-size: 1rem;

    .review-header {
      display: flex;
      align-items: center;
      gap: 24px;

      .card-avatar {
        height: 64px;
        width: 64px;
        border-radius: 50%;
        overflow: hidden;
      }

      .review-title {
        span {
          font-size: 20px;
          font-weight: 800;
          margin-bottom: 8px;
        }
      }
    }
  }

  .bg-circle {
    pointer-events-none;

    div {
      position: absolute;
      border-radius: 50%;
      z-index: -1;
      right: -60px;
      bottom: -140px;
      opacity: 0.6;
      height: 378px;
      width: 378px;
      background: ${(props) => props.theme.colors.colorAccent};
    }
    img {
      display: none;
      position: absolute;
      z-index: -1;
      height: 342px;
      bottom: -35px;
      right: -30px;

      @media screen and (min-width: 768px) {
        display: block;
      }
    }
  }

  & > div {
    padding: 65px 48px;
    text-align: left;
    overflow: hidden;
    position: relative;
    z-index: 5;

    &:first-child {
      display: flex;
      align-items: center;
      background: ${(props) => props.theme.colors.colorAccent};
      width: 34.19%;
      div.un-title {
        z-index: -1;
        top: 4vh;
        right: -10vh;
        pointer-events: none;
        position: absolute;
        opacity: 0.6;
        font-weight: 800;
        font-size: 40vh;
        color: #fff;
      }
      div.o-title {
        z-index: -1;
        position: absolute;
        pointer-events: none;
        left: 0;
        bottom: 7vh;
        color: #fff;
        opacity: 0.6;
        font-weight: 800;
        font-size: 40vh;
      }
    }
    &:last-child {
      width: 65.81%;
      p {
        font-size: 1.125rem.;
      }
    }

    @media screen and (max-width: 768px) {
      padding: 20px 10px;
      width: 100% !important;
    }
  }

  @media screen and (max-width: 768px) {
    flex-direction: column;
  }
`;

const RatingsContainer = styled.div`
  display: flex;
  gap: 10px;

  svg {
    height: 18px;
    fill: ${(props) => props.theme.colors.colorNavy};
  }
`;

interface BrokerButtonProps {
  title: string;
  link: string;
}

const BrokerButton: React.FC<BrokerButtonProps> = ({ title, link }) => {
  const buttonHandler = (e: any) => {
    e.stopPropagation();
    // e.stopImmediatePropagation();
    e.preventDefault();
    (window as any).Calendly.initPopupWidget({
      url: link,
    });
    return false;
  };
  return (
    <button
      className="button button--height--md button--md button--rounded button--primary button button--height--md button--md button--rounded button--primary"
      onClick={buttonHandler}>
      {title}
      <ArrowTopRight svgClasses="icn-1 suffix" color="#fff" />
    </button>
  );
};

const Podcast = ({ url }: any) => {
  const spotifyUrl = 'https://open.spotify.com/embed/iframe-api/v1';
  const attr = [{ name: 'async', value: true }];
  const embedElement = useRef(null);
  const state = useExternalScript(spotifyUrl, attr);
  useEffect(() => {
    if (state == 'ready') {
      const newUrl = `spotify:${url
        .replace(/^https:\/\/.+(?=(show|episode))/, '')
        .replace(/\?.+$/, '')
        .replace('/', ':')}`;
      (window as any).onSpotifyIframeApiReady = (IFrameAPI: any) => {
        const element = embedElement.current;
        const options = {
          uri: newUrl,
        };
        const callback = (EmbedController: any) => null;
        IFrameAPI.createController(element, options, callback);
      };
    }
  }, [state, url]);
  return <div id="spotify-embed-frame" ref={embedElement}></div>;
};
// Text Styles

const Txt1 = styled.p`
  font-family: ${(props) => props.theme.text.textBook};
  font-size: ${(props) => props.theme.text.textOne};
  line-height: 160%;
`;

const Txt2 = styled.p`
  font-family: ${(props) => props.theme.text.textBook};
  font-size: ${(props) => props.theme.text.textTwo};
  line-height: 160%;
  margin-bottom: 0.6rem;
`;

const Txt3 = styled.p`
  font-family: ${(props) => props.theme.text.textBook};
  font-size: ${(props) => props.theme.text.textThree};
  line-height: 160%;
`;

const Txt4 = styled.p`
  font-family: ${(props) => props.theme.text.textBook};
  font-size: ${(props) => props.theme.text.textFour};
  line-height: 160%;
`;

const Txt5 = styled.p`
  font-family: ${(props) => props.theme.text.textBook};
  font-size: ${(props) => props.theme.text.textFive};
  line-height: 160%;
`;

const Txt6 = styled.p`
  font-family: ${(props) => props.theme.text.textBook};
  font-size: ${(props) => props.theme.text.textSix};
  line-height: 160%;
`;

const Txt7 = styled.p`
  font-family: ${(props) => props.theme.text.textBook};
  font-size: ${(props) => props.theme.text.textSeven};
  line-height: 160%;
`;

const Txt8 = styled.p`
  font-family: ${(props) => props.theme.text.textBook};
  font-size: ${(props) => props.theme.text.textEight};
  line-height: 160%;
`;

const Txt9 = styled.p`
  font-family: ${(props) => props.theme.text.textBook};
  font-size: ${(props) => props.theme.text.textNine};
  line-height: 160%;
`;

const H2 = styled.h2`
  font-family: ${(props) => props.theme.text.textBlack};
  font-size: ${(props) => props.theme.text.HeadingTwo};
  line-height: ${pxToRem(57.6)};
`;

const H3 = styled.h3`
  font-family: ${(props) => props.theme.text.textBlack};
  font-size: ${(props) => props.theme.text.HeadingThree};
  line-height: ${pxToRem(48)};
`;

const H4 = styled.h4`
  font-family: ${(props) => props.theme.text.textBlack};
  font-size: ${(props) => props.theme.text.HeadingFour};
  line-height: ${pxToRem(38.4)};
`;

const SectionHead = styled.span`
  display: block;
  font-family: ${(props) => props.theme.text.textBook};
  font-size: ${pxToRem(18)};
  line-height: ${pxToRem(21.6)};
  letter-spacing: 0.15em;
  text-transform: uppercase;
  color: ${(props) => props.theme.text.colorBlack};
  margin-bottom: ${pxToRem(16)};
`;

const YouTubeWrapper = styled.div`
  @media (max-width: 768px) {
    iframe {
      width: 100%;
      height: auto;
    }
  }
`;

const urlFor = (source: any) =>
  urlBuilder({ ...clientConfig.sanity }).image(source);

const serializers = {
  types: {
    block(props: any) {
      switch (props.node.style) {
        case 'blockquote':
          return <blockquote>{props.children}</blockquote>;
        case 'section-head':
          return <SectionHead>{props.children}</SectionHead>;
        case 'h1':
          return <h1>{props.children}</h1>;
        case 'h2':
          return <h2>{props.children}</h2>;
        case 'h3':
          return <h3>{props.children}</h3>;
        case 'h4':
          return <h4>{props.children}</h4>;
        case 'txt-1':
          return <Txt1>{props.children}</Txt1>;
        case 'txt-2':
          return <Txt2>{props.children}</Txt2>;
        case 'txt-3':
          return <Txt3>{props.children}</Txt3>;
        case 'txt-4':
          return <Txt4>{props.children}</Txt4>;
        case 'txt-5':
          return <Txt5>{props.children}</Txt5>;
        case 'txt-6':
          return <Txt6>{props.children}</Txt6>;
        case 'txt-7':
          return <Txt7>{props.children}</Txt7>;
        case 'txt-8':
          return <Txt8>{props.children}</Txt8>;
        case 'txt-9':
          return <Txt9>{props.children}</Txt9>;
        case 'title':
          return <Txt9>{props.children}</Txt9>;
        case 'small':
          return <small>{props.children}</small>;
        default:
          return <p>{props.children}</p>;
      }
    },
    contentIcon: (props: any) => (
      <object
        type="image/svg+xml"
        data={props.node.asset.url}
        width={200}></object>
    ),
    contentImage: (props: any) => (
      <Figure>
        {props.node?.roundedImageAccent ? (
          <ContentImageWrapper>
            <img
              // src={urlFor(props.node.asset).width(780).url() || ''}
              src={props.node.asset.url}
              width={450}
              alt={props.node.alt}
            />
          </ContentImageWrapper>
        ) : (
          <>
            <img
              // src={urlFor(props.node.asset).width(780).url() || ''}
              src={props.node.asset.url}
              width={780}
              alt={props.node.alt}
            />
          </>
        )}
        {props.node?.embeddedText ? (
          <ImageEmbeddedText $pos={props.node.embeddedText.position}>
            <BlockText blocks={props.node.embeddedText.text} />
          </ImageEmbeddedText>
        ) : null}
      </Figure>
    ),
    contentImageFull: (props: any) => (
      <ArticleFullImage>
        <Figure>
          <img
            // src={urlFor(props.node.asset).width(960).url() || ''}
            src={props.node.asset.url}
            width={960}
            alt={props.node.alt}
          />
          <figcaption>{props.node.caption}</figcaption>
          {props.node?.embeddedText ? (
            <ImageEmbeddedText $pos={props.node.embeddedText.position}>
              <BlockText blocks={props.node.embeddedText.text} />
            </ImageEmbeddedText>
          ) : null}
        </Figure>
      </ArticleFullImage>
    ),
    ctaItem: ({ node }: any) => {
      if (node.externalLink) {
        return (
          <>
            <Button node={node} />
          </>
        );
      }
      if (node.internalLink) {
        return (
          <>
            {node?.internalLink?.link?._type === 'page' && (
              <a
                id={`${node.internalLink.link._type}-${removeAccents(
                  node.internalLink.title.toLowerCase()
                )}`}
                className="button button--height--md button--md button--primary"
                href={`/${node.internalLink.link.content.main.slug.current}/`}
                onClick={() => {
                  segment('track', {
                    trackName: 'click',
                    text: node.internalLink.title,
                    elementType: 'button',
                    elementID: `button-${removeAccents(
                      node.internalLink.title
                    )}`,
                    colour: 'primary',
                    destination: `${window.location.origin}/${node.internalLink.link.content.main.slug.current}`,
                  });
                }}
                data-link={node.internalLink.title}>
                {node.internalLink.title}
              </a>
            )}
            {node.internalLink.link._type === 'post' && (
              <a
                id={`${node.internalLink.link._type}-${removeAccents(
                  node.internalLink.title.toLowerCase()
                )}`}
                className={`button button--height--md button--md ${
                  node?.ctaIcon?.type == 'alternate'
                    ? 'button--alternate'
                    : 'button--primary'
                }`}
                href={`/${node.internalLink.link.slug.current}/`}
                onClick={() => {
                  segment('track', {
                    trackName: 'click',
                    text: node.internalLink.title,
                    elementType: 'button',
                    elementID: `button-${removeAccents(
                      node.internalLink.title
                    )}`,
                    colour: 'primary',
                    destination: `${window.location.origin}/${node.internalLink.link.slug.current}`,
                  });
                }}
                data-link={node.internalLink.title}>
                {node?.ctaIcon?.icon ? (
                  <TitleIcon
                    title={node.internalLink.title}
                    type={node.ctaIcon?.type || null}
                    position={node.ctaIcon?.position || null}
                    icon={node.ctaIcon.icon}
                  />
                ) : (
                  node.internalLink.title
                )}
              </a>
            )}
          </>
        );
      }

      if (node.applink && node.applink.select === 'CV2') {
        return <LinkCV2 node={node} />;
      }

      if (node.applink && node.applink.select === 'LS') {
        return <LinkLS node={node} />;
      }

      return null;
    },
    interestRate: () => <RatesInterestRate />,
    comparisonRate: () => <RatesComparisonRate />,
    youtube: ({ node }: any) => {
      const { url } = node;
      const id = getYouTubeId(url) || '';
      return useMemo(
        () => (
          <YouTubeWrapper>
            <YouTube videoId={id} />
          </YouTubeWrapper>
        ),
        [url]
      );
    },
    list: ({ node }: any) => {
      return <RenderList list={node} />;
    },
    brokerModal: ({ node }: any) => {
      return <ModalButton title={node.title} content={node?.content} />;
    },
    calendlyEmbed: ({ node }: any) => {
      const { link, name } = node;
      const getIcon = useCallback(() => {
        const { position, type, icon } = node.ctaIcon;
        const classes =
          position == 'suffix' || !position ? 'icn-1  suffix' : 'icn-1 prefix';
        const color = !type || type == 'primary' ? 'white' : '#284053';
        switch (icon) {
          case 'arrowTopRight':
            return <ArrowTopRight svgClasses={classes} color={color} />;
          case 'trophy':
            return <Trophy svgClasses={classes} color={color} />;
          case 'person':
            return <Person svgClasses={classes} color={color} />;
          case 'heart':
            return <Heart svgClasses={classes} color={color} />;
          default:
            return null;
        }
      }, [node.ctaIcon]);
      const bookBrokerHandle = (e: any) => {
        e.stopPropagation();
        // e.stopImmediatePropagation();
        e.preventDefault();
        (window as any).Calendly.initPopupWidget({
          url: link,
        });
        return false;
      };

      return (
        <button
          className={`button button--md button--height--md button--rounded ${
            node?.ctaIcon?.type == 'primary'
              ? 'button--primary'
              : 'button--alternate'
          }`}
          style={{ whiteSpace: 'normal' }}
          onClick={bookBrokerHandle}>
          {node?.ctaIcon?.icon && node?.ctaIcon?.position == 'prefix'
            ? getIcon()
            : null}
          {name}
          {node?.ctaIcon?.icon &&
          ((node?.ctaIcon?.icon && node?.ctaIcon?.position == 'suffix') ||
            !node?.ctaIcon?.position)
            ? getIcon()
            : null}
        </button>
      );
    },
    calendlyWidget: ({ node }: any) => <CalendlyInlineWidget />,
    table: ({ node }: any) => {
      if (node?.rows?.length > 0) {
        return (
          <CustomTable>
            {node.rows.map((row: any, index: number) => {
              if (row?.cells) {
                if (index == 0)
                  return (
                    <tr key={randomAlphaNumeric(8)}>
                      {row.cells.map((cell: string) => (
                        <th key={randomAlphaNumeric(8)}>{cell}</th>
                      ))}
                    </tr>
                  );
                return (
                  <tr key={randomAlphaNumeric(8)}>
                    {row.cells.map((cell: string) => (
                      <td key={randomAlphaNumeric(8)}>{cell}</td>
                    ))}
                  </tr>
                );
              }
            })}
          </CustomTable>
        );
      }
      return null;
    },
    podcast: ({ node }: any) => <Podcast url={node.url} />,
    customBanner: ({ node }: any) => {
      const getStars = () => {
        const stars = node?.testimonial?.stars;
        if (typeof stars != 'number') return null;
        if (stars == 0) return null;
        const starsArr = [];
        for (let i = 0; i < stars; i++) {
          starsArr.push(<StarSVG key={randomAlphaNumeric(5 + i)} />);
        }
        return starsArr;
      };
      const getImageAlt =
        node?.testimonial?.broker?.content?.main?.image?.asset?.alt;
      const getImageSrc = () => {
        const imageName =
          node?.testimonial?.broker?.content?.main?.image?.asset?._ref
            ?.replace(/^image-/, '')
            .replace(/(-png|-jpg|-jpeg)$/, (val: any) => {
              return val.replace('-', '.');
            });
        const defaultBrokerImage =
          '47f877ab8006ccf2b0cc66c30fcee138bfcd490d-1000x1000.png';
        return `https://cdn.sanity.io/images/86mn51w1/${
          process.env.NODE_ENV == 'development' ? 'dev' : 'staging'
        }/${imageName || defaultBrokerImage}`;
      };
      const getCalendlyLink = () =>
        node?.testimonial?.broker?.content?.main?.links?.calendly ||
        'https://calendly.com/uno-customer-care/uno-quick-call/';

      return (
        <CustomBanner className="custom-banner">
          <div>
            <div className="review-card">
              <div className="review-header">
                {/* <div className="card-avatar">
                <img
                  src="https://cdn.sanity.io/images/86mn51w1/staging/79920390c00a14bad0d4d4e61b3679e7d90aac34-720x720.jpg"
                  alt="tiffany headshot"
                />
              </div> */}
                <div className="review-title">
                  <span>{node?.testimonial?.name}</span>
                  <RatingsContainer>{getStars()}</RatingsContainer>
                </div>
              </div>
              <div className="review-body">“{node?.testimonial?.review}”</div>
            </div>
            <div className="un-title">un</div>
            <div className="o-title">o.</div>
          </div>
          <div>
            <div className="custom-banner-cta">
              {node?.testimonial?.brokerTitle ? (
                <h2>{node?.testimonial?.brokerTitle}</h2>
              ) : null}
              <BlockText blocks={node?.testimonial?.brokerCard} />
            </div>
            <BrokerButton
              title={node?.testimonial?.brokerButton}
              link={getCalendlyLink()}
            />
            <div className="bg-circle">
              <div className="circle"></div>
              <img
                // src="https://cdn.sanity.io/images/86mn51w1/dev/47f877ab8006ccf2b0cc66c30fcee138bfcd490d-1000x1000.png"
                src={getImageSrc()}
                alt={getImageAlt}
              />
            </div>
          </div>
        </CustomBanner>
      );
    },
  },
  marks: {
    internalLink: ({ mark, children }: any) => {
      if (mark.reference) {
        const { slug = {} } = mark.reference ? mark.reference : '';

        const postHref = `/${slug.current}/`;

        return (
          <>
            {mark.reference._type === 'page' && (
              <a
                href={`/${mark.reference.content.main.slug.current}/`}
                onClick={() => {
                  segment('track', {
                    trackName: 'click',
                    elementType: 'link',
                    elementID: `link-${removeAccents(
                      mark.reference.content.main.slug.current
                    )}`,
                    colour: 'primary',
                    destination: `${window.location.origin}/${mark.reference.content.main.slug.current}`,
                  });
                }}
                data-link={`link-${mark.reference.content.main.slug.current}`}>
                {children}
              </a>
            )}
            {mark.reference._type === 'post' && (
              <a
                href={postHref}
                onClick={() => {
                  segment('track', {
                    trackName: 'click',
                    text: children,
                    elementType: 'link',
                    elementID: `link-${removeAccents(slug.current)}`,
                    colour: 'primary',
                    destination: `${window.location.origin}/${slug.current}`,
                  });
                }}
                data-link={`link-${removeAccents(slug.current)}`}>
                {children}
              </a>
            )}
          </>
        );
      }
      return null;
    },
    link: ({ mark, children }: any) => {
      const { blank, href } = mark;

      return blank ? (
        <a
          data-link={`link-${mark._key}`}
          href={`${href}/`}
          target="_blank"
          rel="noopener noreferrer">
          {children}
        </a>
      ) : (
        <a
          href={href}
          onClick={() => {
            segment('track', {
              trackName: 'click',
              text: children,
              elementType: 'link',
              elementID: `link-${mark._key}`,
              colour: 'primary',
              destination: href,
            });
          }}
          data-link={`link-${mark._key}`}>
          {children}
        </a>
      );
    },
    sup: (props: any) => <sup>{props.children}</sup>,
    sub: (props: any) => <sub>{props.children}</sub>,
    i: (props: any) => <i>{props.children}</i>,
  },
};

const BlockText = (props: any) => (
  <BaseBlockContent blocks={props.blocks} serializers={serializers} />
);

export default BlockText;
