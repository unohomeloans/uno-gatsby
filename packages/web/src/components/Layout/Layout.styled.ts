import styled from 'styled-components';
import pxToRem from '../../utils/pxToRem';

export const StyledMain = styled.main`
  padding: ${pxToRem(90)} 0 0;

  @media (max-width: 375px) {
    padding: ${pxToRem(233)} 0 0;
  }
  @media (max-width: 415px) {
    padding: ${pxToRem(205)} 0 0;
  }
`;
