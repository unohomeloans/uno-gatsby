import React, { ReactNode, useEffect, useRef } from 'react';
import { ThemeProvider } from 'styled-components';
import isNil from 'lodash/isNil';
import isEmpty from 'lodash/isEmpty';
import { COOKIE_EXPIRES, UNO_REFERRER, setCookie } from '../../utils/cookies';
import useRouter from '../../hooks/useRouter';
import theme from '../../styles/theme';
import GlobalStyle from '../../styles/GlobalStyle';
import Header from '../Header';
import Footer from '../Footer';
import { StyledMain } from './Layout.styled';

import '../../styles/scss/main.scss';
import Modal from '../Modal';
import CalendlyPopupWidget from '../CalendlyPopupWidget';
import {
  StyledHeaderContainer,
  StyledHeaderWrapper,
} from '../Header/Header.styled';
import Banner from '../Banner';

interface RedirectLinks {
  [key: string]: string;
}

const REDIRECT_LINKS: RedirectLinks = {
  '/home-loans/#chat-with-expert': 'https://app.unohomeloans.com.au/contact',
  '/home-loans/#advice': 'https://unohomeloans.com.au/find-me-a-home-loan/',
  '/home-loans/#refinance-calculator': 'https://app.unohomeloans.com.au',
  '/home-loans/#borrowing-calculator': 'https://borrow.integration.uno',
  '/home-loans/#stamp-duty-calculator': 'https://stampduty.integration.uno',
  '/home-loans/#apply-intro':
    'https://unohomeloans.com.au/find-me-a-home-loan/',
  '/onboarding-alt': 'https://unohomeloans.com.au/find-me-a-home-loan/',
};

interface LayoutProps {
  children: ReactNode;
  headerBackground?: string;
  showCalendlyWidget?: boolean;
  showBrokerModalButton?: boolean;
}

const Layout: React.FC<LayoutProps> = ({
  children,
  headerBackground,
  showCalendlyWidget = true,
  showBrokerModalButton = true,
}) => {
  const router = useRouter();
  useEffect(() => {
    let utmSource = '';
    let utmMedium = '';
    let utmTerm = '';
    let utmContent = '';
    let utmCampaign = '';
    let unoReferrer = '';

    const parsedSearch = new URLSearchParams(router.search);

    if (isEmpty(parsedSearch.keys())) {
      return;
    }

    if (!isNil(parsedSearch.get('utm_source'))) {
      utmSource = parsedSearch.get('utm_source') as string;
      setCookie('utm_source', utmSource, { expires: COOKIE_EXPIRES });
    }

    if (!isNil(parsedSearch.get('utm_medium'))) {
      utmMedium = parsedSearch.get('utm_medium') as string;
      setCookie('utm_medium', utmMedium, { expires: COOKIE_EXPIRES });
    }
    if (!isNil(parsedSearch.get('utm_term'))) {
      utmTerm = parsedSearch.get('utm_term') as string;
      setCookie('utm_term', utmTerm, { expires: COOKIE_EXPIRES });
    }
    if (!isNil(parsedSearch.get('utm_content'))) {
      utmContent = parsedSearch.get('utm_content') as string;
      setCookie('utm_content', utmContent, { expires: COOKIE_EXPIRES });
    }

    if (!isNil(parsedSearch.get('utm_campaign'))) {
      utmCampaign = parsedSearch.get('utm_campaign') as string;
      setCookie('utm_campaign', utmCampaign, { expires: COOKIE_EXPIRES });
    }
    if (!isNil(parsedSearch.get('uno-referrer'))) {
      unoReferrer = parsedSearch.get('uno-referrer') as string;
      setCookie(UNO_REFERRER, unoReferrer, { expires: COOKIE_EXPIRES });
    }
  }, [router.search]);

  const mainRef = useRef<HTMLElement>(null);

  useEffect(() => {
    if (mainRef.current) {
      const aLinks = mainRef.current.querySelectorAll('a');
      aLinks.forEach((a) => {
        Object.keys(REDIRECT_LINKS).forEach((redirectLink) => {
          if (
            a.href.length > 0 &&
            (a.href.endsWith(redirectLink) || a.href.includes(redirectLink))
          ) {
            a.href = REDIRECT_LINKS[redirectLink];
          }
        });
        if (a.textContent?.toLowerCase().trim() === 'talk to a uno broker') {
          a.href = '';
          a.addEventListener('click', (e) => {
            e.stopPropagation();
            e.stopImmediatePropagation();
            e.preventDefault();
            (window as any).Calendly.initPopupWidget({
              url: 'https://calendly.com/vt-uno/uno-call-15-minutes',
            });
            return false;
          });
        }
      });
    }
  }, []);

  return (
    <>
      <GlobalStyle />
      <ThemeProvider theme={theme}>
        <Header headerBackground={headerBackground} />
        <StyledMain ref={mainRef}>{children}</StyledMain>
        <Footer />
        <Modal showBrokerButton={showBrokerModalButton} />
        {showCalendlyWidget && <CalendlyPopupWidget />}
      </ThemeProvider>
    </>
  );
};

export default Layout;
