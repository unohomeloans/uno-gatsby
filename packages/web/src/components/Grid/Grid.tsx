import styled, { css } from 'styled-components';

interface GridProps {
  width?: number;
  fraction?: number;
  gap?: number;
  align?: string;
  span?: number;
}

const spanP = (props: any) =>
  props.span
    ? {
        gridColumn: `span ${props.span}`,
      }
    : null;

const Grid = styled.div<GridProps>`
  display: grid;
  grid-template-columns: ${({ width, fraction }) =>
    `repeat(${width || 320}, ${fraction || 1}fr)`};
  gap: ${({ gap }) => gap + 'px' || 32 + 'px'};

  @media (max-width: 992px) {
    display: block;
  }

  ${({ align }) =>
    align &&
    css`
      align-items: ${align};
    `}

  ${({ span }) =>
    span &&
    css`
      grid-column: span ${span};
    `}
`;

(Grid as any).Item = styled.div([] as any, spanP as any);

export default Grid;
