import React from 'react';
import { useStaticQuery, graphql } from 'gatsby';

interface RatesComparisonRateProps {
  idSelector?: string;
}

const RatesComparisonRate: React.FC<RatesComparisonRateProps> = ({
  idSelector,
}) => {
  const dataRates = useStaticQuery(graphql`
    query RatesComparisonSettingsQuery {
      sanitySiteSettings {
        comparisonRate
      }
    }
  `);

  const idPrefix = idSelector || `post-content-ratescomparisonrate`;

  return (
    <>
      {dataRates.sanitySiteSettings.comparisonRate && (
        <p id={`${idPrefix}`} className="RatesComparisonRate">
          {dataRates.sanitySiteSettings.comparisonRate}
        </p>
      )}
    </>
  );
};

export default RatesComparisonRate;
