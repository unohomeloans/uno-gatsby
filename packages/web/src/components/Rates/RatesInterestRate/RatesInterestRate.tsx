import React from 'react';
import { useStaticQuery, graphql } from 'gatsby';

interface RatesInterestRateProps {
  idSelector?: string;
}

const RatesInterestRate: React.FC<RatesInterestRateProps> = ({
  idSelector,
}) => {
  const dataRates = useStaticQuery(graphql`
    query RatesInterestSettingsQuery {
      sanitySiteSettings {
        interestRate
      }
    }
  `);

  const idPrefix = idSelector || 'post-content-ratesinterestrate';

  return (
    <>
      {dataRates.sanitySiteSettings.interestRate && (
        <p id={`${idPrefix}`} className="ratesinterestrate">
          {dataRates.sanitySiteSettings.interestRate}
        </p>
      )}
    </>
  );
};

export default RatesInterestRate;
