import React from 'react';
import { format } from 'date-fns';
import { Link } from 'gatsby';
import { GatsbyImage } from 'gatsby-plugin-image';
import styled from 'styled-components';
import pxToRem from '../../../utils/pxToRem';

const PostFeaturedWrapper = styled.div`
  display: block;
  height: 100%;
  background: ${(props) => props.theme.colors.colorWhite};
  box-shadow: 0 0 ${pxToRem(20)} ${pxToRem(4)} rgba(0, 0, 0, 0.03),
    0 ${pxToRem(40)} ${pxToRem(40)} 0 rgba(40, 64, 83, 0.03),
    0 ${pxToRem(16)} ${pxToRem(16)} 0 rgba(40, 64, 83, 0.03),
    0 ${pxToRem(8)} ${pxToRem(8)} 0 rgba(40, 64, 83, 0.03),
    0 ${pxToRem(4)} ${pxToRem(4)} 0 rgba(0, 0, 0, 0.05);
  border-radius: ${pxToRem(12)};

  @media (min-width: 992px) {
    display: flex;
    flex-direction: column;
    flex: 1;
  }
`;

const PostFeaturedImage = styled.div`
  .card--img {
    border-top-left-radius: ${pxToRem(12)};
    border-top-right-radius: ${pxToRem(12)};
    max-height: ${pxToRem(180)};
  }
`;
const PostFeaturedContent = styled.div`
  padding: ${(props) => props.theme.spacing.spacingTwo};
  height: 100%;
  display: flex;
  flex-flow: column nowrap;
`;

const PostFeaturedContentCategories = styled.div`
  ul {
    list-style-type: none;
    margin: ${(props) => props.theme.spacing.spacingTwo} 0 0;
    padding: 0;
    display: flex;

    @media (min-width: 768px) {
      margin: 0;
    }

    li {
      color: ${(props) => props.theme.colors.colorPrimary};
      font-size: ${(props) => props.theme.text.textOne};
      font-family: ${(props) => props.theme.text.textHeavy};
      padding: 0 ${(props) => props.theme.spacing.spacingOne} 0
        ${(props) => props.theme.spacing.spacingOne};
      margin: 0 0 ${(props) => props.theme.spacing.spacingOne};

      &:first-child {
        padding: 0 ${(props) => props.theme.spacing.spacingOne} 0 0;
      }

      &:not(:last-child) {
        border-right: ${pxToRem(1)} solid
          ${(props) => props.theme.colors.colorPrimary};
      }
    }
  }
`;

const PostFeaturedContentTitle = styled.div`
  font-family: ${(props) => props.theme.text.textBlack};
  font-size: ${(props) => props.theme.text.textFive};
  a {
    color: ${(props) => props.theme.colors.colorBlack};
  }
`;

const PostFeaturedContentExcerpt = styled.div`
  padding: ${(props) => props.theme.spacing.spacingOne} 0;
  line-height: ${pxToRem(24)};
`;

const PostFeaturedContentMeta = styled.div`
  margin: auto 0 0;
  font-size: ${(props) => props.theme.text.textOne};
  padding: ${pxToRem(8)} 0;
  color: ${(props) => props.theme.colors.colorGrey03};
`;

const PostFeaturedContentMetaDate = styled.div`
  font-size: ${(props) => props.theme.text.textOne};
  padding: 0;
  color: ${(props) => props.theme.colors.colorGrey03};
`;

const BlogPostFeatured = (props: any) => {
  const {
    title,
    excerpt,
    publishedAt,
    mainImage,
    categories,
    author,
    slug,
    featured,
  } = props;

  return (
    <>
      {featured && (
        <PostFeaturedWrapper>
          <PostFeaturedImage>
            {mainImage && (
              <Link to={`/${slug.current}/`}>
                <GatsbyImage
                  className="card--img"
                  imgClassName="card--img"
                  alt={mainImage.asset}
                  image={mainImage.asset.gatsbyImageData}
                />
              </Link>
            )}
          </PostFeaturedImage>
          <PostFeaturedContent>
            {categories && (
              <PostFeaturedContentCategories>
                <ul>
                  {categories.map((category: any, key: any) => (
                    <li key={category._id}>{category.title}</li>
                  ))}
                </ul>
              </PostFeaturedContentCategories>
            )}
            <PostFeaturedContentTitle>
              <Link to={`/${slug.current}/`}>{title}</Link>
            </PostFeaturedContentTitle>
            {excerpt && (
              <PostFeaturedContentExcerpt>{excerpt}</PostFeaturedContentExcerpt>
            )}
            {author && author.name && publishedAt && (
              <>
                <PostFeaturedContentMeta>{author.name}</PostFeaturedContentMeta>
                <PostFeaturedContentMetaDate>
                  {format(new Date(publishedAt), 'MMMM Do, yyyy')}
                </PostFeaturedContentMetaDate>
              </>
            )}
          </PostFeaturedContent>
        </PostFeaturedWrapper>
      )}
    </>
  );
};

export default BlogPostFeatured;
