/* eslint-disable prettier/prettier */
import React from 'react';
import { Link } from 'gatsby';
import { GatsbyImage } from 'gatsby-plugin-image';
import dayjs from 'dayjs';
import advancedFormat from 'dayjs/plugin/advancedFormat';
import styled from 'styled-components';
import pxToRem from '../../../utils/pxToRem';

dayjs.extend(advancedFormat);

const PostItemWrapper = styled.div`
  display: flex;
  background: ${(props) => props.theme.colors.colorAccent};
  border-radius: 8px;
  border-bottom: solid ${pxToRem(1)}
    ${(props) => props.theme.colors.colorGrey02};
  flex-direction: row;
  padding: ${(props) => props.theme.spacing.spacingFive} ${pxToRem(50)};
  gap: ${40};

  @media (max-width: 768px) {
    flex-direction: column;
    padding: 1.5rem;
  }

  .card--img {
    border-radius: 50%;
    height: ${pxToRem(270)};
    width: ${pxToRem(270)};
  }
`;

const PostItemWrapperImage = styled.div`
  display: flex;
  justify-content: center;

  @media (min-width: 768px) {
    // width: ${pxToRem(360)};
    padding: 0 ${(props) => props.theme.spacing.spacingThree} 0 0;
  }
`;

const PostItemWrapperContent = styled.div`
  flex: 1;
  padding: 0;
`;

const PostItemWrapperContentTitle = styled.div`
  font-family: ${(props) => props.theme.text.textMedium};

  a {
    font-size: ${pxToRem(32)};
    color: ${(props) => props.theme.colors.colorNavy};
  }
`;

const PostItemWrapperContentCategories = styled.div`
  ul {
    list-style-type: none;
    margin: ${(props) => props.theme.spacing.spacingTwo} 0 0;
    padding: 0;
    display: flex;

    @media (min-width: 768px) {
      margin: 0;
    }

    li {
      color: ${(props) => props.theme.colors.colorNavy};
      font-size: ${(props) => props.theme.text.textTwo};
      font-family: ${(props) => props.theme.text.textBook};
      padding: 0 ${(props) => props.theme.spacing.spacingOne} 0
        ${(props) => props.theme.spacing.spacingOne};
      margin: 0 0 ${(props) => props.theme.spacing.spacingOne};
      text-transform: uppercase;
      letter-spacing: 0.15em;
      line-height: 120%;

      &:first-child {
        padding: 0 ${(props) => props.theme.spacing.spacingOne} 0 0;
      }

      &:not(:last-child) {
        border-right: ${pxToRem(1)} solid
          ${(props) => props.theme.colors.colorGrey02};
      }
    }
  }
`;

const PostItemWrapperContentExcerpt = styled.div`
  padding: ${(props) => props.theme.spacing.spacingOne} 0;
  font-size: ${pxToRem(18)};
  // line-height: ${pxToRem(24)};
`;

const PostItemWrapperContentMeta = styled.div`
  font-size: ${(props) => props.theme.text.textOne};
  padding: ${pxToRem(8)} 0;
  color: ${(props) => props.theme.colors.colorGrey03};
`;

const BlogPost = (props: any) => {
  const {
    title,
    excerpt,
    publishedAt,
    mainImage,
    categories,
    author,
    slug,
  } = props;

  return (
    <>
      <PostItemWrapper>
        {mainImage && (
          <PostItemWrapperImage>
            {mainImage?.asset?.gatsbyImageData && (
              <Link
                data-post-link={`/${slug.current}`}
                to={`/${slug.current}/`}>
                <GatsbyImage
                  className="card--img"
                  imgClassName="card--img"
                  alt={title}
                  image={mainImage.asset.gatsbyImageData}
                  data-post-image={mainImage.asset.gatsbyImageData}
                />
              </Link>
            )}
          </PostItemWrapperImage>
        )}
        <PostItemWrapperContent>
          {categories && (
            <PostItemWrapperContentCategories>
              <ul>
                {categories.map((category: any) => (
                  <li data-category-name={category.title} key={category._id}>
                    {category.title}
                  </li>
                ))}
              </ul>
            </PostItemWrapperContentCategories>
          )}
          <PostItemWrapperContentTitle>
            <Link data-post-title={title} to={`/${slug.current}/`}>
              {title}
            </Link>
          </PostItemWrapperContentTitle>
          {excerpt && (
            <PostItemWrapperContentExcerpt>
              {excerpt}
            </PostItemWrapperContentExcerpt>
          )}
          {author && author.name && publishedAt && (
            <PostItemWrapperContentMeta>
              By {author.name}, {dayjs(publishedAt).format('MMMM Do, YYYY')}
            </PostItemWrapperContentMeta>
          )}
        </PostItemWrapperContent>
      </PostItemWrapper>
    </>
  );
};

export default BlogPost;
