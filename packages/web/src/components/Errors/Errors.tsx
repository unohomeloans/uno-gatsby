import React from 'react';
import GraphQLErrorList from '../GraphQLErrorList';
import Layout from '../Layout';

const Errors = ({ errors }: any) => {
  return (
    <Layout>
      <GraphQLErrorList errors={errors} />
    </Layout>
  );
};

export default Errors;
