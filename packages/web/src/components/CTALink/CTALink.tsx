import React, { useEffect, useState } from 'react';
import { Link } from 'gatsby';
import isNil from 'lodash/isNil';
import removeAccents from '../../utils/removeAccents';
import useRouter from '../../hooks/useRouter';
import segment from '../../utils/segment';

const CTALink = (props: any) => {
  const LS = process.env.GATSBY_MY_SANITY_LS;
  const CV2 = process.env.GATSBY_MY_SANITY_CV2;

  const router = useRouter();
  const internalLink = props.title;
  const link = props.linkedPage || props.link || props.applink || '#';

  const idPrefix = !isNil(props.idSelector) ? props.idSelector : `ph-hero-cta`;
  const withAppend = !isNil(link.append) ? link.append : '';

  const [clicked, setClicked] = useState(false);
  const [clickedAppllicationCV2, setClickedAppllicationCV2] = useState(false);
  const [clickedAppllicationLS, setClickedAppllicationLS] = useState(false);

  useEffect(() => {
    if (clicked) {
      // do something meaningful, Promises, if/else, whatever, and then
      window.location.assign(`${link.link}${router.search}`);
    }
    if (clickedAppllicationCV2) {
      // do something meaningful, Promises, if/else, whatever, and then
      window.location.assign(`${CV2}${withAppend}${router.search}`);
    }
    if (clickedAppllicationLS) {
      // do something meaningful, Promises, if/else, whatever, and then
      window.location.assign(`${LS}${withAppend}${router.search}`);
    }
  });

  const hancleClickWithRouting = () => {
    setClicked(true);
    segment('track', {
      trackName: 'click',
      text: link.title,
      elementType: 'link',
      elementID: `link-${removeAccents(link.title.toLowerCase())}`,
      colour: 'primary',
      destination: link.link,
    });
  };

  const hancleClickNoRouting = () => {
    segment('track', {
      trackName: 'click',
      text: link.title,
      elementType: 'link',
      elementID: `link-${removeAccents(internalLink.toLowerCase())}`,
      colour: 'primary',
      destination: `${window.location.origin}/${link.content.main.slug.current}`,
    });
  };

  if (props.kind === 'link' && link._type === 'externalLink') {
    return (
      <a
        id={`${idPrefix}-item-${link._type}-${removeAccents(link.title)}`}
        href={link.link}
        onClick={hancleClickWithRouting}
        target={`${link.blank ? '_blank' : '_self'}`}
      >
        {link.title}
      </a>
    );
  }

  if (props.kind === 'link' && link._type === 'page') {
    return (
      <Link
        id={`${idPrefix}-item-${link._type}-${removeAccents(internalLink)}`}
        to={`/${link.content.main.slug.current}`}
        onClick={hancleClickNoRouting}
      >
        {internalLink}
      </Link>
    );
  }

  if (props.kind === 'link' && link._type === 'post') {
    return (
      <a
        id={`${idPrefix}-item-${link._type}-${removeAccents(internalLink)}`}
        href={link.slug.current}
        onClick={() => {
          segment('track', {
            trackName: 'click',
            text: link.title,
            elementType: 'link',
            elementID: `link-${removeAccents(internalLink.toLowerCase())}`,
            colour: 'primary',
            destination: `${link.slug.current}`,
          });
        }}
      >
        {internalLink}
      </a>
    );
  }

  if (props.applink && props.kind === 'link' && props.applink.select === 'LS') {
    return (
      <a
        id={`${idPrefix}-item-${link._type}-${removeAccents(
          props.applink.select
        )}`}
        href={`${LS}${withAppend}${router.search}`}
        onClick={() => {
          setClickedAppllicationLS(true);
          segment('track', {
            trackName: 'click',
            text: link.title,
            elementType: 'link',
            elementID: `link-${removeAccents(link.title.toLowerCase())}`,
            colour: 'primary',
            destination: `${LS}${withAppend}`,
          });
        }}
      >
        {link.title}
      </a>
    );
  }

  if (
    props.applink &&
    props.kind === 'link' &&
    props.applink.select === 'CV2'
  ) {
    return (
      <a
        id={`${idPrefix}-item-${link._type}-${removeAccents(
          props.applink.select
        )}`}
        href={`${CV2}${withAppend}${router.search}`}
        onClick={() => {
          setClickedAppllicationCV2(true);
          segment('track', {
            trackName: 'click',
            text: link.title,
            elementType: 'link',
            elementID: `link-${removeAccents(link.title.toLowerCase())}`,
            colour: 'primary',
            destination: `${CV2}${withAppend}`,
          });
        }}
      >
        {link.title}
      </a>
    );
  }

  return null;
};

export default CTALink;
