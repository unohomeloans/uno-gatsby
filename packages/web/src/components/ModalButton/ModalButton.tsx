/* eslint-disable @typescript-eslint/no-var-requires */
import React, { useCallback } from 'react';
import styled from 'styled-components';
import {
  ArrowTopRight,
  Heart,
  Person,
  Trophy,
} from '../Icons/GeneralIcons/GeneralIcons';

interface TitleIconProps {
  type: string | null;
  position: string | null;
  icon: string;
  title: string;
}

const TitleIcon: React.FC<TitleIconProps> = ({
  type,
  position,
  icon,
  title,
}) => {
  const getIcon = useCallback(() => {
    const classes =
      position == 'suffix' || !position ? 'icn-1  suffix' : 'icn-1 prefix';
    const color = !type || type == 'primary' ? 'white' : '#284053';
    switch (icon) {
      case 'arrowTopRight':
        return <ArrowTopRight svgClasses={classes} color={color} />;
      case 'trophy':
        return <Trophy svgClasses={classes} color={color} />;
      case 'person':
        return <Person svgClasses={classes} color={color} />;
      case 'heart':
        return <Heart svgClasses={classes} color={color} />;
      default:
        return null;
    }
  }, [icon, position]);
  return (
    <>
      {(position == 'suffix' || !position) && title}
      {getIcon()}
      {position == 'prefix' && title}
    </>
  );
};

interface ModalButtonProps {
  title: string;
  inHeader?: boolean;
  content?: any;
  isRed?: boolean;
}

const Label = styled.label.attrs((props: { $isRed?: boolean }) => props)`
  ${({ $isRed }) =>
    $isRed &&
    'color: #fff !important; background: #e15856 !important; border-color: #e15856 !important;'}
`;

const ModalButton: React.FC<ModalButtonProps> = ({
  title,
  content = false,
  inHeader = false,
  isRed = false,
}) => {
  return (
    <>
      <Label
        htmlFor="show"
        style={{ cursor: 'pointer' }}
        $isRed={isRed}
        className={
          inHeader
            ? 'button button--alternate button--fullwidth button--rounded'
            : `button button--height--md button--md button--rounded ${
                content?.type == 'alternate'
                  ? 'button--alternate'
                  : 'button--primary'
              }`
        }>
        {content?.icon ? <TitleIcon title={title} {...content} /> : title}
      </Label>
    </>
  );
};

export default ModalButton;
