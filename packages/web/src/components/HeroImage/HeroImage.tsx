import React from 'react';
import PropTypes from 'prop-types';
import { GatsbyImage } from 'gatsby-plugin-image';
import styled from 'styled-components';

const Parent = styled.div<{ bc: any }>`
  position: relative;
  background: rgba(${({ bc }: any) => bc}, 0.4);
`;

const StyledHeroImage = styled(GatsbyImage)<{
  height: any;
  mobileHeight: any;
}>`
  position: relative;
  top: 0;
  left: 0;
  width: 100%;
  height: ${({ height }: any) => height || '500px'};
  z-index: -1;

  & > img {
    object-fit: cover !important;
    object-position: 0% 0% !important;
    font-family: 'object-fit: cover !important; object-position: 0% 0% !important;';
  }

  @media screen and (max-width: 600px) {
    height: ${({ mobileHeight }: any) => mobileHeight};
  }
`;

const Content = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 100%;
`;

const HeroImage = ({
  fluid,
  title,
  height,
  mobileHeight,
  overlayColor,
  children,
  className,
}: any) => {
  return (
    <Parent bc={overlayColor}>
      <StyledHeroImage
        image={fluid}
        alt={title}
        title={title}
        height={height}
        mobileHeight={mobileHeight}
      />
      <Content className={className}>{children}</Content>
    </Parent>
  );
};

HeroImage.propTypes = {
  fluid: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  height: PropTypes.string,
  mobileHeight: PropTypes.string,
  overlayColor: PropTypes.string,
  children: PropTypes.node,
  className: PropTypes.string,
};
HeroImage.defaultProps = {
  height: null,
  mobileHeight: null,
  overlayColor: 'transparent',
  children: null,
  className: null,
};

export default HeroImage;
