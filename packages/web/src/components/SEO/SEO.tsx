import React from 'react';
import { StaticQuery, graphql, useStaticQuery } from 'gatsby';
import randomAlphaNumeric from '../../utils/randomAlphaNumeric';

const DETAILSQUERY = graphql`
  query DefaultSEOQuery {
    site: sanitySiteSettings(_id: { regex: "/(drafts.|)siteSettings/" }) {
      title
      description
    }
    post: sanityPost {
      seoDescription
      seoTitle
    }
    page: sanityPage {
      content {
        main {
          title
        }
      }
    }
  }
`;

interface HeadProps {
  title?: string;
  description?: string;
  meta?: any[];
  script?: any[];
}

const SEO: React.FC<HeadProps> = ({ description, title, script, meta }) => {
  const data = useStaticQuery(DETAILSQUERY);

  const metaDescription =
    description || (data.site && data.site.description) || '';
  const siteTitle = (data.title && data.site.title) || 'UNO';
  const siteAuthor =
    (data.site && data.site.author && data.site.author.name) || '';
  const pageTitle = title || siteTitle;

  return (
    <>
      {/* <meta property="google-site-verification" content="" /> */}
      <meta property="description" content={metaDescription} />
      <meta property="og:title" content={pageTitle} />
      <meta property="og:description" content={metaDescription} />
      <meta property="og:type" content="website" />
      <meta property="twitter:creator" content={siteAuthor || 'UNO'} />
      <meta property="twitter:card" content="summary" />
      <meta property="twitter:title" content={pageTitle} />
      <meta property="twitter:description" content={metaDescription} />
      {meta?.map((metaItem: any) => (
        <meta
          key={randomAlphaNumeric(6)}
          property={metaItem?.property}
          name={metaItem?.name}
          content={metaItem.content}
        />
      ))}
      {script?.map((scriptItem: any) => (
        <script key={randomAlphaNumeric(6)} src={scriptItem.src} async />
      ))}
      <title>{pageTitle}</title>
    </>
  );
};
export default SEO;
