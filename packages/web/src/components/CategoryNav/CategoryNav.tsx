import React from 'react';

interface CategoryNavProps {
  children: React.ReactNode;
}

const CategoryNav: React.FC<CategoryNavProps> = ({ children }) => {
  return <>{children}</>;
};

export default CategoryNav;
