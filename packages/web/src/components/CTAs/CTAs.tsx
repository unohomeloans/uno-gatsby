import React from 'react';
import removeAccents from '../../utils/removeAccents';
import segment from '../../utils/segment';
import ModalButton from '../ModalButton';

interface CTAsProps {
  idSelector?: string;
}

const CTAs: React.FC<CTAsProps> = ({ idSelector }) => {
  const idPrefix = idSelector || `header-ctas`;

  const CV2 = process.env.GATSBY_MY_SANITY_CV2;

  return (
    <div id={idPrefix} className="header-ctas">
      <div id={`${idPrefix}-item-login`} className="header-ctas--item">
        <a
          href={`${CV2}login`}
          onClick={() => {
            segment('track', {
              trackName: 'click',
              text: 'Login',
              elementType: 'button',
              elementID: `button-${removeAccents('Login')}`,
              destination: `${window.location.href}home-loans/#sign-in`,
            });
          }}>
          Login
        </a>
      </div>
      <div id={`${idPrefix}-item-getstarted`} className="header-ctas--item">
        <ModalButton title="Get Started" inHeader={true} isRed={true} />
      </div>
    </div>
  );
};

export default CTAs;
