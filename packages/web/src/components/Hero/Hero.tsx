import React from 'react';
import isNil from 'lodash/isNil';
import { GatsbyImage } from 'gatsby-plugin-image';
import styled from 'styled-components';
import pxToRem from '../../utils/pxToRem';
import useSiteMetadata from '../../hooks/useSiteMetadata';
import HeroImage from '../HeroImage';

const ArticleTitle = styled.div`
  text-align: left;

  h1 {
    font-family: ${(props) => props.theme.text.textBlack};
    font-size: ${(props) => props.theme.text.textSeven};
    letter-spacing: ${pxToRem(-1)};
    margin: 0 0 ${pxToRem(13)};

    @media (min-width: 768px) {
      font-size: ${(props) => props.theme.text.textEight};
    }
  }
`;

const ArticleExcerpt = styled.div`
  font-size: ${(props) => props.theme.text.textFour};
  letter-spacing: ${pxToRem(-0.52)};
  color: ${(props) => props.theme.colors.colorWhite};
  padding: ${(props) => props.theme.spacing.spacingOne} 0
    ${(props) => props.theme.spacing.spacingThree};
  border-bottom: ${pxToRem(1)} solid
    ${(props) => props.theme.colors.colorGrey02};
  margin: 0 0 ${(props) => props.theme.spacing.spacingTwo};
  line-height: 1.4rem;

  @media (min-width: 768px) {
    font-size: ${pxToRem(24)};
    line-height: 2rem;
    margin: 0 0 ${(props) => props.theme.spacing.spacingThree};
  }

  &.ArticleExcerptDarker {
    color: ${(props) => props.theme.colors.colorBlack};
  }
`;

const ArticleHero = styled.div``;

const ArticleHeroDefault = styled.div`
  max-width: 790px;
  margin: 0 auto;
  padding: ${pxToRem(45)} 0 0;

  @media (min-width: 992px) {
    padding: ${pxToRem(60)} 0 0;
  }

  .article__header--meta__icons {
    svg {
      fill: ${(props) => props.theme.colors.colorGrey02};
    }
  }

  .article__header--meta {
    padding: ${(props) => props.theme.spacing.spacingThree} 0;
  }
`;

const ArticleHeroFull = styled.div``;

const ArticleHeroFullContent = styled.div`
  padding: ${(props) => props.theme.spacing.spacingThree} 0;
  text-align: center;

  @media (min-width: 992px) {
    padding: ${(props) => props.theme.spacing.spacingFive} ${pxToRem(170)};
  }

  ${ArticleTitle} {
    h1 {
      color: ${(props) => props.theme.colors.colorWhite};
      text-align: center;
    }
  }

  .article__header--meta__wrapper__content,
  .article__header--meta__wrapper__content--name {
    text-align: left;
    color: ${(props) => props.theme.colors.colorWhite};
  }

  .article__header--meta__icons {
    svg {
      fill: ${(props) => props.theme.colors.colorWhite};
    }
  }
`;
const ArticleHeader = styled.div`
  padding: 30px 0;

  .article__header--meta__icons {
    svg {
      fill: ${(props) => props.theme.colors.colorGrey02};
    }
  }
`;

const Hero = (props: any) => {
  const {
    title,
    mainList,
    heroImage,
    author,
    publishedAt,
    slug,
    excerpt,
    idSelector,
  } = props;
  const { siteUrl } = useSiteMetadata();

  const idPrefix = !isNil(idSelector) ? idSelector : `post-hero`;

  // Icons Here 👇
  const icons = [
    {
      name: 'fb',
      svg: (
        <svg width="30" height="30" xmlns="http://www.w3.org/2000/svg">
          <path
            fillRule="evenodd"
            d="M14.992 0C6.708.005-.003 6.722 0 15.006.003 23.289 6.72 30.002 15.003 30 23.286 29.998 30 23.283 30 15A15 15 0 0014.992 0zm3.556 14.664h-2.522V24.5h-3.422v-9.846H10.24v-2.884h2.018V9.358c0-.504-.1-3.918 4.254-3.918h2.405v2.918h-1.917a.774.774 0 00-.958.958v2.455h2.892l-.387 2.893z"
          />
        </svg>
      ),
      link: `https://www.facebook.com/sharer/sharer.php?u=${
        siteUrl + '/' + slug.current
      }`,
    },
    {
      name: 'twitter',
      svg: (
        <svg width="30" height="30" xmlns="http://www.w3.org/2000/svg">
          <path
            fillRule="evenodd"
            d="M15 0c8.285 0 15 6.716 15 15 0 8.285-6.715 15-15 15S0 23.285 0 15C0 6.716 6.715 0 15 0zm3.895 8.443c-2.017 0-3.654 1.587-3.654 3.547 0 .278.032.55.095.809a10.485 10.485 0 01-7.534-3.707 3.446 3.446 0 00-.495 1.783c0 1.23.645 2.318 1.625 2.952a3.716 3.716 0 01-1.656-.444v.045c0 1.72 1.26 3.153 2.932 3.479a3.78 3.78 0 01-1.65.06c.465 1.41 1.815 2.436 3.416 2.465a7.473 7.473 0 01-4.54 1.518 7.67 7.67 0 01-.873-.05 10.578 10.578 0 005.604 1.595c6.724 0 10.4-5.407 10.4-10.095l-.01-.46a7.293 7.293 0 001.823-1.835 7.467 7.467 0 01-2.1.56 3.577 3.577 0 001.608-1.964 7.412 7.412 0 01-2.323.861 3.703 3.703 0 00-2.668-1.119z"
          />
        </svg>
      ),
      link: `https://twitter.com/intent/tweet/?text=${title}&url=${
        siteUrl + '/' + slug.current
      }%2F&via=unohomeloans`,
    },
    {
      name: 'linkedIn',
      svg: (
        <svg width="30" height="30" xmlns="http://www.w3.org/2000/svg">
          <path
            fillRule="evenodd"
            d="M15 0c8.285 0 15 6.716 15 15 0 8.284-6.715 15-15 15S0 23.284 0 15C0 6.716 6.715 0 15 0zm-3.739 11.248H8.453v9.362h2.808v-9.362zm8.134-.324c-1.363 0-2.583.498-3.448 1.596v-1.303h-2.82v9.393h2.82v-5.08c0-1.073.983-2.12 2.215-2.12 1.232 0 1.536 1.047 1.536 2.094v5.105h2.809v-5.314c0-3.69-1.748-4.371-3.112-4.371zM9.844 7.5a1.407 1.407 0 100 2.814 1.407 1.407 0 000-2.814z"
          />
        </svg>
      ),
      link: `https://www.linkedin.com/shareArticle?mini=true&url=${
        siteUrl + '/' + slug.current
      }&title=${title}&source=${title}`,
    },
  ];

  const metaContent = (
    <div className="article__header--meta">
      <div className="article__header--meta__wrapper">
        {author && author.image && (
          <div className="article__header--meta__wrapper__image">
            <GatsbyImage
              // data-author-image={author.image.asset.gatsbyImageData.src}
              image={author.image.asset.gatsbyImageData}
              alt={author.name}
            />
          </div>
        )}
        <div className="article__header--meta__wrapper__content">
          {author && author.name && (
            <div
              className="article__header--meta__wrapper__content--name"
              data-author-name={author.name}>
              {author.name}
            </div>
          )}
          {publishedAt && (
            <div
              className="article__header--meta__wrapper__content--date"
              data-publishedat={publishedAt}>
              {publishedAt}
            </div>
          )}
        </div>
      </div>
      {/* <div className="article__header--meta__icons">
        {icons.map((icon) => (
          <a
            data-meta-icon={icon.name}
            key={icon.link}
            href={icon.link}
            target="_blank"
            rel="noopener noreferrer">
            {icon.svg}
          </a>
        ))}
      </div> */}
    </div>
  );

  return (
    <>
      {heroImage && (
        <>
          {heroImage.layout === 'default' && (
            <ArticleHero id={`${idPrefix}-${heroImage.layout}`}>
              <ArticleHeroDefault>
                <div className="container">
                  {heroImage.asset && (
                    <GatsbyImage
                      className="card--img"
                      imgClassName="card--img"
                      image={heroImage.asset.gatsbyImageData}
                      alt={title}
                    />
                  )}
                  <ArticleHeader id={`${idPrefix}-${heroImage.layout}-header`}>
                    <ArticleTitle
                      id={`${idPrefix}-${heroImage.layout}-header-title`}>
                      <h1 data-article-title={title}>{title}</h1>
                    </ArticleTitle>
                    {excerpt && (
                      <ArticleExcerpt
                        id={`${idPrefix}-${heroImage.layout}-header-excerpt`}
                        className="ArticleExcerptDarker">
                        {excerpt}
                      </ArticleExcerpt>
                    )}
                    {metaContent}
                  </ArticleHeader>
                </div>
              </ArticleHeroDefault>
            </ArticleHero>
          )}
          {heroImage.layout === 'full' && (
            <ArticleHeroFull id={`${idPrefix}-${heroImage.layout}`}>
              <HeroImage
                title="astronaut"
                fluid={heroImage.asset.gatsbyImageData}
                overlayColor="40, 64, 83"
                height="500px"
                mobileHeight="447px"
                data-bg-image={heroImage.asset.gatsbyImageData}>
                <div className="container">
                  <ArticleHeroFullContent>
                    <ArticleTitle>
                      <h1 data-article-title={title}>{title}</h1>
                    </ArticleTitle>
                    {excerpt && <ArticleExcerpt>{excerpt}</ArticleExcerpt>}
                    {metaContent && <div>{metaContent}</div>}
                  </ArticleHeroFullContent>
                </div>
              </HeroImage>
            </ArticleHeroFull>
          )}
        </>
      )}
      {heroImage &&
        heroImage.layout !== 'default' &&
        heroImage.layout !== 'full' && (
          <ArticleHeroDefault>
            <div className="container">
              <div>
                <ArticleTitle>
                  <h1 data-article-title={title}>{title}</h1>
                </ArticleTitle>
                {excerpt && (
                  <ArticleExcerpt className="ArticleExcerptDarker">
                    {excerpt}
                  </ArticleExcerpt>
                )}
                {metaContent}
              </div>
            </div>
          </ArticleHeroDefault>
        )}
    </>
  );
};

export default Hero;
