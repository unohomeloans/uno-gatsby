import React, { useEffect, useRef } from 'react';
import styled from 'styled-components';

const CalendlyInlineWidgetWrapper = styled.div`
  min-width: 320px;
  height: 1180px;

  @media (max-width: 768px) {
    height: 1080px;
  }
`;

const CalendlyInlineWidget: React.FC = () => {
  const widgetEl = useRef(null);

  useEffect(() => {
    (window as any).Calendly.initInlineWidget({
      url:
        'https://calendly.com/uno-customer-care/uno-quick-call-re-home-loan-clearscore',
      parentElement: widgetEl.current,
    });
  }, []);

  return <CalendlyInlineWidgetWrapper ref={widgetEl} />;
};

export default CalendlyInlineWidget;
