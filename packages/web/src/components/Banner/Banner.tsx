/* eslint-disable react/display-name */
/* eslint-disable @typescript-eslint/no-var-requires */
import React from 'react';
import styled from 'styled-components';
import pxToRem from '../../utils/pxToRem';
import theme from '../../styles/theme';

const BannerDiv = styled.a`
  display: block;
  padding: 1rem 1rem;
  text-align: center;
  // color: #fff;
  background: ${(props) => theme.colors.colorWhite};
`;
const RedText = styled.span`
  color: #d6083b;
`;

const Banner = () => {
  return (
    <BannerDiv href="https://velocity.unohomeloans.com.au/index.html">
      <span>
        <RedText>
          Win 3 million Velocity points, lodge an application by Dec 22, 2023 to
          be eligible.
        </RedText>{' '}
        Learn more here
      </span>
    </BannerDiv>
  );
};

export default Banner;
