import React, { useEffect, useRef } from 'react';
import styled from 'styled-components';
import pxToRem from '../../utils/pxToRem';

const CalendlyWrapper = styled.div`
  position: fixed;
  z-index: 1000;
  bottom: 0;
  width: 100%;
  pointer-events: none;
`;

const CalendlyContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  padding: 24px 15px;

  @media (min-width: 1400px) {
    width: ${pxToRem(1360)};
    padding: 24px 0;
  }

  @media (min-width: 992px) {
    margin: 0 auto;
    padding: 24px 20px;
  }
  @media (max-width: 768px) {
    padding: 20px 15px;
  }
`;

const CalendlyButton = styled.button`
  color: #fff;
  background: #e15856;
  pointer-events: auto;
`;

const CalendlyPopupWidget: React.FC = () => {
  const widgetEl = useRef(null);

  return (
    <CalendlyWrapper>
      <CalendlyContainer>
        <CalendlyButton
          className="calendly-button button button--rounded"
          ref={widgetEl}
          onClick={(e) => {
            (window as any).Calendly.initPopupWidget({
              url: 'https://calendly.com/uno-customer-care/uno-quick-call',
            });
            return false;
          }}>
          Book in a Call
        </CalendlyButton>
      </CalendlyContainer>
    </CalendlyWrapper>
  );
};

export default CalendlyPopupWidget;
