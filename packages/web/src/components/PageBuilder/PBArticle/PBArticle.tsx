import React from 'react';
import isNil from 'lodash/isNil';
import styled from 'styled-components';
import BlockText from '../../BlockText';
import pxToRem from '../../../utils/pxToRem';

const PBArticleWrapper = styled.div.attrs(
  (props: { $alignment?: string }) => props
)`
  padding: ${(props) => props.theme.spacing.spacingTen} 0 ${pxToRem(130)} 0;

  text-align: ${({ $alignment }) => {
    switch ($alignment) {
      case 'center':
        return 'center';
      case 'left':
        return 'left';
      default:
        return 'left';
    }
  }};
`;

const PBArticle = (props: any) => {
  const { alignment, text, idSelector } = props;

  const idPrefix = !isNil(idSelector) ? idSelector : `ph-article`;

  return (
    <>
      {text && (
        <PBArticleWrapper
          $alignment={alignment}
          id={`${idPrefix}-wrapper`}
          data-id={`${idPrefix}-wrapper`}>
          <div className="container" data-container="container">
            <div
              id={`${idPrefix}-wrapper-content`}
              data-content={`${idPrefix}-wrapper-content`}
              className="disclaimerWrapperContainer">
              <BlockText blocks={text} />
            </div>
          </div>
        </PBArticleWrapper>
      )}
    </>
  );
};

export default PBArticle;
