import React from 'react';
import isNil from 'lodash/isNil';
import styled from 'styled-components';
import BlockText from '../../BlockText';
import pxToRem from '../../../utils/pxToRem';

const PBYellowBarWrapper = styled.div`
  background: ${(props) => props.theme.colors.colorYellow};
  padding: ${pxToRem(20)} 0;
  text-align: center;
`;

const PBYellowBarWrapperContent = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;

  h3 {
    a {
      color: ${(props) => props.theme.colors.colorBlack};
      text-decoration: underline;
    }
  }

  h4 {
    margin: 0;
    a {
      color: ${(props) => props.theme.colors.colorBlack};
      text-decoration: underline;
    }
  }

  p {
    margin: 0;

    a {
      color: ${(props) => props.theme.colors.colorBlack};
      text-decoration: underline;
    }
  }
`;

const PBYellowBarWrapperContentItem = styled.div``;

const PBYellowBar = (props: any) => {
  const { content, title, idSelector } = props;

  const idPrefix = !isNil(idSelector) ? idSelector : `ph-alert`;

  return (
    <PBYellowBarWrapper id={idPrefix} data-id={idPrefix}>
      <div className="container" data-container="container">
        <PBYellowBarWrapperContent
          id={`${idPrefix}-wrapper`}
          data-wrapper={`${idPrefix}-wrapper`}
        >
          {title && (
            <PBYellowBarWrapperContentItem
              id={`${idPrefix}-wrapper-title`}
              data-title={`${idPrefix}-wrapper-title`}
            >
              <BlockText blocks={title} />
            </PBYellowBarWrapperContentItem>
          )}
          {content && (
            <PBYellowBarWrapperContentItem
              id={`${idPrefix}-wrapper-content`}
              data-content={`${idPrefix}-wrapper-content`}
            >
              <BlockText blocks={content} />
            </PBYellowBarWrapperContentItem>
          )}
        </PBYellowBarWrapperContent>
      </div>
    </PBYellowBarWrapper>
  );
};

export default PBYellowBar;
