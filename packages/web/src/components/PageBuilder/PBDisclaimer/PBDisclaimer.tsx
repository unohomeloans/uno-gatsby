import React from 'react';
import isNil from 'lodash/isNil';
import styled from 'styled-components';
import BlockText from '../../BlockText';
import pxToRem from '../../../utils/pxToRem';

const PBDisclaimerWrapper = styled.div.attrs(
  (props: { $alignment?: string }) => props
)`
  padding: ${(props) => props.theme.spacing.spacingTen} 0 ${pxToRem(130)} 0;

  text-align4: ${({ $alignment }) => {
    switch ($alignment) {
      case 'center':
        return 'center';
      case 'left':
        return 'left';
      default:
        return 'left';
    }
  }};
`;

const PBDisclaimer = (props: any) => {
  const { alignment, text, idSelector } = props;

  const idPrefix = !isNil(idSelector) ? idSelector : `ph-disclaimer`;

  return (
    <>
      {text && (
        <PBDisclaimerWrapper
          $alignment={alignment}
          id={`${idPrefix}-wrapper`}
          data-id={`${idPrefix}-wrapper`}>
          <div className="container" data-container="container">
            <div
              id={`${idPrefix}-wrapper-content`}
              data-content={`${idPrefix}-wrapper-content`}
              className="disclaimerWrapperContainer">
              <BlockText blocks={text} />
            </div>
          </div>
        </PBDisclaimerWrapper>
      )}
    </>
  );
};

export default PBDisclaimer;
