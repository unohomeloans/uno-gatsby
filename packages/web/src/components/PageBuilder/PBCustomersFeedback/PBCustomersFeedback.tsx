/* eslint-disable react/no-unescaped-entities */
import React, { useEffect } from 'react';
import isNil from 'lodash/isNil';
// import { Flex, Box } from 'reflexbox';
import styled from 'styled-components';
import pxToRem from '../../../utils/pxToRem';
import BlockText from '../../BlockText';
import { useExternalScript } from '../../../hooks/useExternalScript';
// import Card from '../../Card';

const PBCustomersFeedbackWrapper = styled.div`
  background: #ebf0f4;
  padding: ${pxToRem(100)} 0;
  margin: ${pxToRem(75)} 0 0;

  #pr-reviews-carousel-widget .a-uk4rpk {
    margin-left: 1rem !important;
    margin-right: 1rem !important;
  }
`;

const SectionHeading = styled.div`
  font-family: ${(props) => props.theme.text.textBook};
  font-size: ${pxToRem(18)};
  line-height: ${pxToRem(21.6)};
  letter-spacing: 0.15em;
  text-transform: uppercase;
  margin-bottom: ${(props) => props.theme.spacing.spacingTwo};
`;

const PBCustomersFeedbackTitle = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 0 0 ${(props) => props.theme.spacing.spacingFive};
  color: ${(props) => props.theme.colors.colorNavy};

  @media (max-width: 768px) {
    flex-flow: column nowrap;
    gap: ${pxToRem(24)};
  }
`;

const CustomerIcon = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 50%;
  text-transform: uppercase;
  height: ${pxToRem(80)};
  width: ${pxToRem(80)};
  color: ${(props) => props.theme.colors.colorWhite};
  font-size: ${(props) => props.theme.text.textEight};
  background: ${(props) => props.theme.colors.colorNavy};
  overflow: hidden;
`;

const RatingsNameWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin-left: ${pxToRem(24)};
`;

const CustomerName = styled.div`
  font-size: ${(props) => props.theme.text.textFive};
  line-height: ${pxToRem(28.8)};
  margin-bottom: ${pxToRem(8)};
  text-transform: capitalize;
`;

const CustomerFeedback = styled.p`
  color: ${(props) => props.theme.colors.colorGrey};
  font-size: ${(props) => props.theme.text.headingFour};
  line-height: ${pxToRem(28.8)};
`;

const PBCustomersFeedbackImageLink = styled.div`
  display: block;
  margin: 0 auto ${(props) => props.theme.spacing.spacingFour};
  text-align: center;
`;

const PBCustomersFeedbackContent = styled.div`
  h3 {
    font-family: ${(props) => props.theme.text.textBlack};
    margin: 0 0 ${(props) => props.theme.spacing.spacingOne};
  }

  .c-card {
    padding: ${pxToRem(48)} ${pxToRem(70)};
  }
`;

const PBCustomersFeedbackContentIcon = styled.span`
  display: inline-block;
  margin: 0 0 ${(props) => props.theme.spacing.spacingOne};
  @media (min-width: 768px) {
    margin: 0 0 ${(props) => props.theme.spacing.spacingThree};
  }
`;

const PBCustomersFeedbackContentRating = styled.div``;
const PBCustomersFeedbackContentRatingItem = styled.div`
  display: inline-block;
  padding: 0 ${pxToRem(4)} 0 0;
`;

const PBCustomersFeedbackContentRatinName = styled.div`
  color: ${(props) => props.theme.colors.colorGrey02};
  font-size: ${(props) => props.theme.text.textOne};
  padding: ${pxToRem(4)} 0;
`;

/* const CustomerIcon = (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    x="0"
    y="0"
    enableBackground="new 0 0 40 40"
    version="1.1"
    width="40"
    height="40"
    viewBox="0 0 60 60"
    xmlSpace="preserve">
    <path
      fill="#14b2a1"
      d="M30 1.5c-16.542 0-30 12.112-30 27 0 5.204 1.646 10.245 4.768 14.604-.591 6.537-2.175 11.39-4.475 13.689a1 1 0 00.846 1.697c.405-.057 9.813-1.411 16.618-5.339C21.621 54.71 25.737 55.5 30 55.5c16.542 0 30-12.112 30-27s-13.458-27-30-27zm-14 31c-2.206 0-4-1.794-4-4s1.794-4 4-4 4 1.794 4 4-1.794 4-4 4zm14 0c-2.206 0-4-1.794-4-4s1.794-4 4-4 4 1.794 4 4-1.794 4-4 4zm14 0c-2.206 0-4-1.794-4-4s1.794-4 4-4 4 1.794 4 4-1.794 4-4 4z"></path>
  </svg>
); */

const RatingStart = (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="24"
    height="24"
    viewBox="0 0 24 24">
    <path
      fill="#284053"
      d="M23.363 8.584l-7.378-1.127L12.678.413c-.247-.526-1.11-.526-1.357 0L8.015 7.457.637 8.584a.75.75 0 00-.423 1.265l5.36 5.494-1.267 7.767a.75.75 0 001.103.777L12 20.245l6.59 3.643a.75.75 0 001.103-.777l-1.267-7.767 5.36-5.494a.75.75 0 00-.423-1.266z"></path>
  </svg>
);

const Stars = ({ n }: any) => {
  const stars = [];
  for (let i = 0; i < n; ++i) {
    stars.push(
      <PBCustomersFeedbackContentRatingItem key={i}>
        {RatingStart}
      </PBCustomersFeedbackContentRatingItem>
    );
  }

  return <>{stars}</>;
};

const PBCustomersFeedback = (props: any) => {
  const { title, idSelector } = props;

  const idPrefix = !isNil(idSelector) ? idSelector : `ph-customers-feedback`;

  const elfUrl = 'https://static.elfsight.com/platform/platform.js';
  const attributes = [
    { name: 'defer', value: true },
    { name: 'data-use-service-core', value: true },
  ];

  const state = useExternalScript(elfUrl, attributes);

  return (
    <PBCustomersFeedbackWrapper
      id={`${idPrefix}-wrapper`}
      data-wrapper={`${idPrefix}-wrapper`}>
      <div className="container" data-container="container">
        <SectionHeading>Testimonials</SectionHeading>
        {title && (
          <PBCustomersFeedbackTitle
            id={`${idPrefix}-wrapper-title`}
            data-title={`${idPrefix}-wrapper-title`}>
            <BlockText key={title._key} blocks={title} />
            {/* <div>
              <a
                target="_blank"
                rel="noreferrer"
                href="https://www.productreview.com.au/listings/uno-home-loans?af=true"
                className="button button--height--md button--alternate-transparent button--rounded">
                See More
              </a>
            </div> */}
          </PBCustomersFeedbackTitle>
        )}
        {state == 'ready' ? (
          <div className="elfsight-app-62a3eb0b-06b4-4818-9a3d-cf15de518a9f"></div>
        ) : null}
      </div>
    </PBCustomersFeedbackWrapper>
  );
};

export default PBCustomersFeedback;
