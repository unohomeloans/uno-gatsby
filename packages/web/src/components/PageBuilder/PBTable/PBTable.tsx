import React from 'react';
import isNil from 'lodash/isNil';
import styled from 'styled-components';
import pxToRem from '../../../utils/pxToRem';
import BlockText from '../../BlockText';
import CTAButton from '../../CTAButton';
import CTALink from '../../CTALink';
import Grid from '../../Grid';

const SVGIcon = (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="34"
    height="34"
    viewBox="0 0 34 34"
  >
    <g fill="none" fillRule="evenodd" stroke="none" strokeWidth="1">
      <g fill="#D21925" transform="translate(-849 -422)">
        <g>
          <g transform="translate(99 80)">
            <g transform="translate(0 136)">
              <g transform="translate(-1 -24)">
                <g transform="translate(690 48)">
                  <g transform="translate(58 179)">
                    <path
                      d="M20 3.333c9.206 0 16.667 7.464 16.667 16.667 0 9.206-7.461 16.667-16.667 16.667-9.205 0-16.667-7.461-16.667-16.667 0-9.203 7.462-16.667 16.667-16.667zm0 8.334c-.92 0-1.667.746-1.667 1.666v5h-5c-.872 0-1.587.67-1.66 1.523l-.006.144c0 .92.746 1.667 1.666 1.667h5v5c0 .872.67 1.587 1.523 1.66l.144.006c.92 0 1.667-.746 1.667-1.666v-5h5c.872 0 1.587-.67 1.66-1.523l.006-.144c0-.92-.746-1.667-1.666-1.667h-5v-5c0-.872-.67-1.587-1.523-1.66z"
                      transform="rotate(45 20 20)"
                    ></path>
                  </g>
                </g>
              </g>
            </g>
          </g>
        </g>
      </g>
    </g>
  </svg>
);

const SVGIconPositive = (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="34"
    height="34"
    viewBox="0 0 34 34"
  >
    <g fill="none" fillRule="evenodd" stroke="none" strokeWidth="1">
      <g fill="#14B2A1" transform="translate(-853 -342)">
        <g>
          <g transform="translate(99 80)">
            <g transform="translate(0 136)">
              <g transform="translate(-1 -24)">
                <g transform="translate(690 48)">
                  <g transform="translate(62 99)">
                    <path d="M14.367 18.832l3.58 4.313 7.408-9.264a1.557 1.557 0 012.131-.228c.654.505.76 1.424.237 2.054l-8.592 10.69a1.552 1.552 0 01-2.368 0L12 20.656a1.424 1.424 0 01.237-2.053 1.556 1.556 0 012.131.228zm5.63 17.835c9.205 0 16.67-7.462 16.67-16.67 0-9.204-7.465-16.664-16.67-16.664-9.205 0-16.664 7.46-16.664 16.665 0 9.207 7.459 16.669 16.664 16.669"></path>
                  </g>
                </g>
              </g>
            </g>
          </g>
        </g>
      </g>
    </g>
  </svg>
);

const PBTableWrapper = styled.div`
  background: ${(props) => props.theme.colors.colorAccent};
  padding: ${(props) => props.theme.spacing.spacingThree} 0;
  @media (min-width: 768px) {
    padding: ${(props) => props.theme.spacing.spacingEight} 0;
  }

  .grid-container {
    @media (max-width: 768px) {
      grid-template-columns: repeat(1, 1fr);
    }
  }
`;

const PBRTableTitle = styled.div`
  text-align: center;
  margin: 0 0 ${(props) => props.theme.spacing.spacingFour};

  span {
    display: block;
  }
`;

const PBTableItemWrapper = styled.div``;

const PBTableItemTable = styled.table`
  background: ${(props) => props.theme.colors.colorWhite};
  width: 100%;
  box-shadow: 0 0.25rem 1.5rem 0 hsla(0, 0%, 80%, 0.3);
  border-radius: ${pxToRem(16)};
  overflow: hidden;
`;

const PBTableItemTableTr = styled.tr`
  border-top: 0.0625rem solid #e9ebed;
  border-left: 0;
  border-right: 0;

  &:first-child {
    td {
      font-family: ${(props) => props.theme.text.textBlack};
      vertical-align: middle;
      text-align: center;
      padding: 8px 12px;
      font-size: 0.75rem;
      line-height: 1;

      @media (min-width: 992px) {
        font-size: 1.25rem;
        padding: 25px 40px;
      }
    }
  }
`;

const PBTableItemTableTd = styled.td`
  border-right: 1px solid #e9ebed;
  font-size: 0.75rem;
  padding: 0.5rem;
  vertical-align: middle;
  text-align: center;
  font-family: ${(props) => props.theme.text.textMedium};

  @media (min-width: 992px) {
    font-size: 1.125rem;
    padding: 0.875rem 32px;
  }

  &:first-child {
    width: 40%;
    text-align: left;
    padding: 0.5rem;

    @media (min-width: 992px) {
      width: 55%;
      font-size: 1.125rem;
      padding: 1.4rem 32px;
    }
  }

  &:last-child {
    border: 0;
    background: rgba(20, 178, 161, 0.1);
    padding: 0.875rem 32px;
  }

  img {
    max-width: 60px;

    @media (min-width: 768px) {
      max-width: 90px;
    }
  }
`;

const PBTableItemTableIcon = styled.div`
  height: ${pxToRem(34)};
  margin: 0 auto;
  width: ${pxToRem(34)};
  overflow: hidden;
`;

const PBTableCTAs = styled.div`
  padding: ${(props) => props.theme.spacing.spacingFour} 0
    ${(props) => props.theme.spacing.spacingThree};
  @media (min-width: 768px) {
    padding: ${(props) => props.theme.spacing.spacingFour} 0
      ${(props) => props.theme.spacing.spacingThree};
  }

  .TableCTAs {
    grid-template-columns: repeat(1, 1fr);
    @media (min-width: 768px) {
      margin: 0 ${pxToRem(200)} 0;
      grid-template-columns: repeat(2, 1fr);
    }
    > div {
      display: flex;
      align-items: center;
      justify-content: center;
    }
  }

  span {
    width: 100%;
    display: inline-block;

    @media (min-width: 768px) {
      display: inherit;
    }
  }
`;

const PBTable = (props: any) => {
  const { title, ctas, idSelector } = props;

  const idPrefix = !isNil(idSelector) ? idSelector : `ph-table`;

  return (
    <PBTableWrapper
      id={`${idPrefix}-wrapper`}
      data-wrapper={`${idPrefix}-wrapper`}
    >
      <div className="container" data-container={`${idPrefix}-container`}>
        {title && (
          <PBRTableTitle
            id={`${idPrefix}-wrapper-title`}
            data-title={`${idPrefix}-wrapper-title`}
          >
            <BlockText key={title._key} blocks={title} />
          </PBRTableTitle>
        )}
        <PBTableItemWrapper
          id={`${idPrefix}-wrapper-table`}
          className="c-home-july__table--table"
          data-item-wrapper={`${idPrefix}-wrapper-table`}
        >
          <PBTableItemTable
            id={`${idPrefix}-wrapper-table-item`}
            data-table={`${idPrefix}-wrapper-table-item`}
          >
            <tbody data-tbody={`${idPrefix}-tbody`}>
              <PBTableItemTableTr data-tr={`${idPrefix}-tr`}>
                <PBTableItemTableTd>&nbsp;</PBTableItemTableTd>
                <PBTableItemTableTd data-td="Lenders or banks">
                  Lenders or banks
                </PBTableItemTableTd>
                <PBTableItemTableTd data-td="Brokers">
                  Brokers
                </PBTableItemTableTd>
                <PBTableItemTableTd data-td="Logos">
                  <img
                    src="https://cdn.unohomeloans.com.au/logos/logo-uno-primary.svg"
                    alt="uno"
                  />
                </PBTableItemTableTd>
              </PBTableItemTableTr>
              <PBTableItemTableTr data-tr={`${idPrefix}-tr`}>
                <PBTableItemTableTd data-td="Support from a dedicated home loan expert">
                  Support from a dedicated home loan expert
                </PBTableItemTableTd>
                <PBTableItemTableTd>
                  <PBTableItemTableIcon className="icon-positive">
                    {SVGIconPositive}
                  </PBTableItemTableIcon>
                </PBTableItemTableTd>
                <PBTableItemTableTd>
                  <PBTableItemTableIcon className="icon-positive">
                    {SVGIconPositive}
                  </PBTableItemTableIcon>
                </PBTableItemTableTd>
                <PBTableItemTableTd>
                  <PBTableItemTableIcon className="icon-positive">
                    {SVGIconPositive}
                  </PBTableItemTableIcon>
                </PBTableItemTableTd>
              </PBTableItemTableTr>
              <PBTableItemTableTr data-tr={`${idPrefix}-tr`}>
                <PBTableItemTableTd data-td="Choice of lenders">
                  Choice of lenders
                </PBTableItemTableTd>
                <PBTableItemTableTd data-td={SVGIcon}>
                  <PBTableItemTableIcon>{SVGIcon}</PBTableItemTableIcon>
                </PBTableItemTableTd>
                <PBTableItemTableTd data-td={SVGIconPositive}>
                  <PBTableItemTableIcon className="icon-positive">
                    {SVGIconPositive}
                  </PBTableItemTableIcon>
                </PBTableItemTableTd>
                <PBTableItemTableTd data-td={SVGIconPositive}>
                  <PBTableItemTableIcon className="icon-positive">
                    {SVGIconPositive}
                  </PBTableItemTableIcon>
                </PBTableItemTableTd>
              </PBTableItemTableTr>
              <PBTableItemTableTr data-tr={`${idPrefix}-tr`}>
                <PBTableItemTableTd data-td="Regular monitoring of your loan against 1000s of deals">
                  Regular monitoring of your loan against 1000s of deals
                </PBTableItemTableTd>
                <PBTableItemTableTd data-td={SVGIcon}>
                  <PBTableItemTableIcon>{SVGIcon}</PBTableItemTableIcon>
                </PBTableItemTableTd>
                <PBTableItemTableTd data-td={SVGIcon}>
                  <PBTableItemTableIcon>{SVGIcon}</PBTableItemTableIcon>
                </PBTableItemTableTd>
                <PBTableItemTableTd data-td={SVGIconPositive}>
                  <PBTableItemTableIcon className="icon-positive">
                    {SVGIconPositive}
                  </PBTableItemTableIcon>
                </PBTableItemTableTd>
              </PBTableItemTableTr>
              <PBTableItemTableTr data-tr={`${idPrefix}-tr`}>
                <PBTableItemTableTd>
                  Proactive alerts when you could save
                </PBTableItemTableTd>
                <PBTableItemTableTd>
                  <PBTableItemTableIcon>{SVGIcon}</PBTableItemTableIcon>
                </PBTableItemTableTd>
                <PBTableItemTableTd>
                  <PBTableItemTableIcon>{SVGIcon}</PBTableItemTableIcon>
                </PBTableItemTableTd>
                <PBTableItemTableTd>
                  <PBTableItemTableIcon className="icon-positive">
                    {SVGIconPositive}
                  </PBTableItemTableIcon>
                </PBTableItemTableTd>
              </PBTableItemTableTr>
              <PBTableItemTableTr data-tr={`${idPrefix}-tr`}>
                <PBTableItemTableTd data-td="Monthly update on your loanScore and potential savings">
                  Monthly update on your loanScore and potential savings
                </PBTableItemTableTd>
                <PBTableItemTableTd data-td={SVGIcon}>
                  <PBTableItemTableIcon>{SVGIcon}</PBTableItemTableIcon>
                </PBTableItemTableTd>
                <PBTableItemTableTd data-td={SVGIcon}>
                  <PBTableItemTableIcon>{SVGIcon}</PBTableItemTableIcon>
                </PBTableItemTableTd>
                <PBTableItemTableTd data-td={SVGIconPositive}>
                  <PBTableItemTableIcon className="icon-positive">
                    {SVGIconPositive}
                  </PBTableItemTableIcon>
                </PBTableItemTableTd>
              </PBTableItemTableTr>
            </tbody>
          </PBTableItemTable>
        </PBTableItemWrapper>
        <PBTableCTAs
          id={`${idPrefix}-wrapper-ctas`}
          data-id={`${idPrefix}-wrapper-ctas`}
        >
          {ctas && (
            <Grid
              width={2}
              gap={30}
              align="flex-start"
              className="TableCTAs"
              data-container={`${idPrefix}-wrapper-ctas`}
              data-container-width={2}
            >
              {ctas.map((ctaItem: any, i: any) => (
                <span key={i}>
                  {ctaItem.kind === 'button' && (
                    <CTAButton key={ctaItem._key} {...ctaItem} />
                  )}
                  {ctaItem.kind === 'link' && (
                    <CTALink key={ctaItem._key} {...ctaItem} />
                  )}
                </span>
              ))}
            </Grid>
          )}
        </PBTableCTAs>
      </div>
    </PBTableWrapper>
  );
};

export default PBTable;
