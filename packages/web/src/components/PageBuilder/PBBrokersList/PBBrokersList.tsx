/* eslint-disable @typescript-eslint/no-var-requires */
import React, { useCallback } from 'react';
import { graphql, StaticQuery } from 'gatsby';
import styled from 'styled-components';
import pxToRem from '../../../utils/pxToRem';
import urlBuilder from '@sanity/image-url';
//import BlockText from '../../BlockText';
import randomAlphaNumeric from '../../../utils/randomAlphaNumeric';

const clientConfig = require('../../../../config/sanity-config');

const urlFor = (source: any) =>
  urlBuilder({ ...clientConfig.sanity }).image(source);

const Txt = styled.p`
  font-family: ${(props) => props.theme.text.textBook};
  font-size: ${pxToRem(18)};
  font-weight: 400;
  margin-bottom: 0;

  a {
    display: block;
    margin-top: 8px;
    text-decoration: capitalize;
    font-weight: bold;
  }

  span {
    display: inline-block;
    font-family: ${(props) => props.theme.text.textBook};
    font-size: ${pxToRem(14)};
    line-height: ${pxToRem(16.8)};
    margin-top: ${pxToRem(16)};
    margin-bottom: ${pxToRem(8)};
    font-weight: normal;

    &:first-child {
      margin-top: 0;
    }
    &:last-child {
      margin-bottom: 0;
    }
  }
`;

const PBBrokersListWrapper = styled.div`
  @media (min-width: 768px) {
    padding: ${pxToRem(60)} 0;
  }

  background: #fff;
`;

const PBBrokersListTitle = styled.div`
  text-align: center;
`;

const PBBrokerName = styled.a`
  display: inline-block;
  font-size: ${pxToRem(32)};
  line-height: ${pxToRem(38.4)};
  font-weight: 600;
  color: ${(props) => props.theme.colors.colorNavy};
`;

const PBBrokerWrapperImage = styled.div`
  border-radius: 50%;
  background: #ebf0f4;
  overflow: hidden;
  height: ${pxToRem(270)};
  width: ${pxToRem(270)};
  max-width: ${pxToRem(270)};

  @media (min-width: 768px) {
    // margin: 0 ${(props) => props.theme.spacing.spacingSix}
    //   ${(props) => props.theme.spacing.spacingFour};
  }

  img {
    display: inline;
    margin-top: 10%;
    width: 270px;
  }

  &.content-image {
    @media (max-width: 992px) {
      order: 1;
      // margin: ${(props) => props.theme.spacing.spacingFour} auto 0;
      // width: 60%;
    }
  }
`;

const PBBrokerContentWrapper = styled.div`
  padding-top: 4rem;
  display: flex;
  flex-flow: column nowrap;
`;

const PBBrokerContentItemWrapper = styled.div`
  display: flex;
  flex-flow: row nowrap;
  gap: ${pxToRem(50)};
  width: 100%;
  margin: ${pxToRem(40)} 0;

  @media (max-width: 992px) {
    flex-flow: column nowrap;
  }
`;
const PBBrokerContentItemInnerWrapper = styled.div`
  display: flex;
  flex-flow: row nowrap;
  justify-content: space-between;

  @media (max-width: 992px) {
    flex-flow: column nowrap;
  }
`;

const PBBrokerInnerContent = styled.div`
  display: flex;
  flex-flow: column nowrap;
  width: 35.29%;
  gap: 50px 43px;

  &:first-child {
    width: 30.59%;
    p {
      line-height: 120%;
    }
  }

  &:nth-child(2) {
    @media (max-width: 992px) {
      margin-bottom: 1rem;
    }
  }

  &:last-of-type {
    flex: 0;
    @media (max-width: 992px) {
      margin: 0 auto;
      width: auto;
    }
  }

  &:first-child,
  &:nth-child(2) {
    @media (max-width: 992px) {
      width: 100%;
    }
  }
`;

const PBBrokerContentItem = styled.div.attrs(
  (props: { $brokerTextAlignment?: string; $display?: string }) => props
)`
  display: flex;
  text-align: ${(props) => props.$brokerTextAlignment || 'center'};

  &:first-child {
    justify-content: center;
  }
  &:last-child {
    gap: ${pxToRem(24)};
    flex-flow: column nowrap;
    width: 100%;
  }

  @media (min-width: 769px) {
    text-align: ${(props) => props.$brokerTextAlignment || 'left'};
  }
`;

const ImagePlaceholder = styled.div`
  display: inline-flex;
  justify-content: center;
  align-items: center;
  color: #141414;
  background: #ebf0f4;
  border-radius: 50%;
  height: 270px;
  width: 270px;
`;

interface BrokerDetailsProps {
  location: string;
  email: string;
  tel: string;
}

const BrokerDetails: React.VFC<BrokerDetailsProps> = ({
  location,
  email,
  tel,
}) => (
  <>
    <Txt>
      <span>Location</span>
      <br />
      {location}
      <br />
      <span>Email</span>
      <br />
      {email}
      <br />
      <span>Phone Number</span>
      <br />
      <a href={`tel:+${tel}`}>{tel}</a>
    </Txt>
  </>
);

interface ListButtonProps {
  title: string;
  link: string;
}

const ListButton: React.FC<ListButtonProps> = ({ title, link }) => {
  const buttonHandler = (e: any) => {
    e.stopPropagation();
    // e.stopImmediatePropagation();
    e.preventDefault();
    (window as any).Calendly.initPopupWidget({
      url: link,
    });
    return false;
  };
  return (
    <button
      className="button button--rounded button--primary"
      onClick={buttonHandler}>
      {title}
    </button>
  );
};

const parseEdge = (arr: any) =>
  arr.map((edge: any) => {
    const mainImage = edge.node._rawContent.main.image;
    const url = edge.node.content.main.slug.current;
    const name = edge.node.content.main.name;
    const BrokerDetailsProps = {
      email: edge.node.content.main.email,
      location: edge.node.content.main.location,
      tel: edge.node.content.main.tel,
    };
    const excerptBio = useCallback(() => {
      if (edge.node._rawContent.main.bio) {
        const getText = () => {
          if (edge.node._rawContent.main?.bio?.length > 1) {
            return edge.node._rawContent.main.bio.reduce(
              (acc: string, val: any) => {
                acc = `${acc}${val.children[0].text}`;
                return acc;
              },
              ''
            );
          } else if (edge.node._rawContent.main?.bio?.length == 1) {
            return edge.node._rawContent.main.bio[0].children[0].text;
          }
          return '';
        };
        const text: string = getText();
        if (text.length > 120) {
          const shortenText = `${text.substring(0, 120)}... `;
          return (
            <Txt>
              <span>About</span>
              <br />
              {shortenText}
              <a href={`/${edge.node.content.main.slug.current}`}>Read More</a>
            </Txt>
          );
        }
        return (
          <Txt>
            <span>About</span>
            <br />
            {text}
          </Txt>
        );
      }
      return null;
    }, [edge.node._rawContent.main.bio, edge.node.content.main.slug]);
    return (
      <PBBrokerContentItemWrapper key={randomAlphaNumeric(6)}>
        <PBBrokerContentItem $display={'flex'}>
          <a href={`/${edge.node.content.main.slug.current}/`}>
            {mainImage?.asset?.url ? (
              <PBBrokerWrapperImage
                className="content-image"
                data-image={mainImage.asset.url}>
                <picture>
                  <img
                    srcSet={`${urlFor(mainImage.asset.url).width(
                      320
                    )} 320w,${urlFor(mainImage.asset.url).width(
                      480
                    )} 480w,${urlFor(mainImage.asset.url).width(800)} 800w`}
                    sizes={`(max-width: 320px) 280px,(max-width: 480px) 440px,800px`}
                    src={urlFor(mainImage.asset.url).toString() || undefined}
                    alt={mainImage.asset.alt}
                  />
                </picture>
              </PBBrokerWrapperImage>
            ) : (
              <ImagePlaceholder>No Image</ImagePlaceholder>
            )}
          </a>
        </PBBrokerContentItem>
        <PBBrokerContentItem>
          <PBBrokerName href={`/${url}/`}>{name}</PBBrokerName>
          <PBBrokerContentItemInnerWrapper>
            <PBBrokerInnerContent>
              <BrokerDetails {...BrokerDetailsProps} />
            </PBBrokerInnerContent>
            <PBBrokerInnerContent>{excerptBio()}</PBBrokerInnerContent>
            <PBBrokerInnerContent>
              {edge.node.content.main.links.calendly && (
                <ListButton
                  title={edge.node.content.main.ctas.listCta}
                  link={edge.node.content.main.links.calendly}
                />
              )}
            </PBBrokerInnerContent>
          </PBBrokerContentItemInnerWrapper>
        </PBBrokerContentItem>
      </PBBrokerContentItemWrapper>
    );
  });

interface PBBrokersListProps {
  chooseBackground?: string;
  title: string;
}

const PBBrokersList: React.FC<PBBrokersListProps> = ({
  chooseBackground,
  title,
}) => {
  return (
    <StaticQuery
      query={graphql`
        query allBrokersQuery {
          allBrokers: allSanityBroker(
            sort: { fields: _createdAt, order: DESC }
          ) {
            edges {
              node {
                content {
                  main {
                    name
                    location
                    slug {
                      current
                    }
                    email
                    tel
                    ctas {
                      listCta
                    }
                    links {
                      referrer
                      calendly
                    }
                  }
                }
                _createdAt
                _rawContent(resolveReferences: { maxDepth: 10 })
              }
            }
          }
        }
      `}
      render={(data) => (
        <>
          <PBBrokersListWrapper>
            <div className="container" data-container="container">
              {title ? (
                <PBBrokersListTitle>
                  <h1>{title}</h1>
                </PBBrokersListTitle>
              ) : null}
              <PBBrokerContentWrapper>
                {parseEdge(data.allBrokers.edges)}
              </PBBrokerContentWrapper>
            </div>
          </PBBrokersListWrapper>
        </>
      )}
    />
  );
};

export default PBBrokersList;
