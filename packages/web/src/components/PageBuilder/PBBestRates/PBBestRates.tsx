/* eslint-disable prettier/prettier */
import React from 'react';
import isNil from 'lodash/isNil';
import axios from 'axios';
import styled from 'styled-components';
import lenders from '../../../constants/lenders.json';
import pxToRem from '../../../utils/pxToRem';
import BlockText from '../../BlockText';

const BASE_API_URL = process.env.GATSBY_PRODUCT_SEARCH_URL;
const SUPPORTED_PROVIDERS =
  '864,ADB,AMP,ANZ,BLU,BOM,BOS,BSA,BWT,CTI,CBA,CUA,FMB,HLL,HPB,ING,LIB,LTB,MAS,MEB,MHO,MQB,NAB,PPR,QML,RZD,STG,SCP,TMB,UNI,WBC';
const PAYMENT_TYPE = 'PrincipalAndInterest';
const PROPERTY_PURPOSE = 'OwnerOccupied';
const DEPOSIT_AMOUNT = 250000;
const PROPERTY_ESTIMATED_VALUE = 750000;
const PRODUCTS_PER_PROVIDER = 20;
const SIZE = 5;
const LOAN_TYPE = 'Purchase';
const PROVIDERS = SUPPORTED_PROVIDERS;
const RATE_TYPE = 'FixedRate';

// const BASE_API_URL_DEFAULT = `${BASE_API_URL}?&paymentType=${PAYMENT_TYPE}&size=${SIZE}&propertyEstimatedValue=${PROPERTY_ESTIMATED_VALUE}&depositAmount=${DEPOSIT_AMOUNT}&propertyPurpose=&rateType=${RATE_TYPE}&providers=${PROVIDERS}&productsPerProvider=${PRODUCTS_PER_PROVIDER}&loanType=${LOAN_TYPE}`;
// const BASE_API_URL_FIXED_RATE = `${BASE_API_URL}?&paymentType=${PAYMENT_TYPE}&size=${SIZE}&propertyEstimatedValue=${PROPERTY_ESTIMATED_VALUE}&depositAmount=${DEPOSIT_AMOUNT}&propertyPurpose=&rateType=${RATE_TYPE}&providers=${PROVIDERS}&productsPerProvider=${PRODUCTS_PER_PROVIDER}&loanType=${LOAN_TYPE}`;
// const BASE_API_URL_VARIABLE_RATE = `${BASE_API_URL}?&paymentType=${PAYMENT_TYPE}&size=${SIZE}&propertyEstimatedValue=${PROPERTY_ESTIMATED_VALUE}&depositAmount=${DEPOSIT_AMOUNT}&propertyPurpose=${PROPERTY_PURPOSE}&rateType=VariableRate&providers=${PROVIDERS}&productsPerProvider=${PRODUCTS_PER_PROVIDER}&loanType=${LOAN_TYPE}`;
// const BASE_API_URL_OWNER_OCCUPIED = `${BASE_API_URL}?&paymentType=${PAYMENT_TYPE}&size=${SIZE}&propertyEstimatedValue=${PROPERTY_ESTIMATED_VALUE}&depositAmount=${DEPOSIT_AMOUNT}&propertyPurpose=${PROPERTY_PURPOSE}rateType=${RATE_TYPE}&&providers=${PROVIDERS}&productsPerProvider=${PRODUCTS_PER_PROVIDER}&loanType=${LOAN_TYPE}`;
// const BASE_API_URL_INVESTMENT = `${BASE_API_URL}?&paymentType=${PAYMENT_TYPE}&size=${SIZE}&propertyEstimatedValue=${PROPERTY_ESTIMATED_VALUE}&depositAmount=${DEPOSIT_AMOUNT}&propertyPurpose=Investment&rateType=${RATE_TYPE}&providers=${PROVIDERS}&productsPerProvider=${PRODUCTS_PER_PROVIDER}`;

// const requestOne = axios.get(BASE_API_URL_DEFAULT);
// const requestTwo = axios.get(BASE_API_URL_FIXED_RATE);
// const requestThree = axios.get(BASE_API_URL_VARIABLE_RATE);
// const requestFour = axios.get(BASE_API_URL_OWNER_OCCUPIED);
// const requestFive = axios.get(BASE_API_URL_INVESTMENT);

const BestHomeLoanRatesLogosTitle = styled.div`
  text-align: center;
  @media (min-width: 768px) {
    padding: ${(props) => props.theme.spacing.spacingSix} 0 0;
  }
`;

const BestHomeLoanRatesLogos = styled.div`
  text-align: center;
  @media (min-width: 768px) {
    margin: ${(props) => props.theme.spacing.spacingSix} ${pxToRem(100)}
      ${(props) => props.theme.spacing.spacingSix};
  }
`;

const BestHomeLoanRatesLogosItem = styled.div`
  padding: ${pxToRem(10)};
  display: inline-block;

  img {
    height: ${pxToRem(25)};
  }
`;

const BestHomeLoanRatesWrapper = styled.div`
  background: ${(props) => props.theme.colors.colorAccent};
  padding: ${(props) => props.theme.spacing.spacingThree} 0
    ${(props) => props.theme.spacing.spacingThree};
  @media (min-width: 768px) {
    padding: ${(props) => props.theme.spacing.spacingSix} 0
      ${(props) => props.theme.spacing.spacingEight};
  }
`;

const BestHomeLoanRatesTitle = styled.h2`
  text-align: center;
  font-family: ${(props) => props.theme.text.textBook};
  font-size: ${(props) => props.theme.text.textSix};
`;

const BestHomeLoanRatesTable = styled.table`
  background: ${(props) => props.theme.colors.colorWhite};
  box-shadow: 0 0 ${pxToRem(20)} ${pxToRem(4)} rgba(0, 0, 0, 0.03),
    0 ${pxToRem(40)} ${pxToRem(40)} 0 rgba(40, 64, 83, 0.03),
    0 ${pxToRem(16)} ${pxToRem(16)} 0 rgba(40, 64, 83, 0.03),
    0 ${pxToRem(8)} ${pxToRem(8)} 0 rgba(40, 64, 83, 0.03),
    0 ${pxToRem(4)} ${pxToRem(4)} 0 rgba(0, 0, 0, 0.05);
  border-radius: ${pxToRem(12)};

  @media (min-width: 768px) {
    margin: 0 0 ${(props) => props.theme.spacing.spacingEight};
  }
`;

const BestHomeLoanRatesTableHeading = styled.thead`
  font-size: ${(props) => props.theme.text.textOne};
  font-family: ${(props) => props.theme.text.textBook};

  th {
    &:nth-child(2) {
      display: none;
      @media (min-width: 768px) {
        display: table-cell;
      }
    }
  }
`;

const BestHomeLoanRatesTableBody = styled.tbody`
  tr {
    td {
      padding: ${(props) => props.theme.spacing.spacingOne};
      ${(props) => props.theme.spacing.spacingTwo};
      @media (min-width: 768px) {
        padding: ${(props) => props.theme.spacing.spacingThree};
        ${(props) => props.theme.spacing.spacingTwo};
      }

      &:nth-child(2) {
        display: none;
        @media (min-width: 768px) {
          display: table-cell;
        }
      }
      &:nth-child(3),
      &:nth-child(4) {
        font-size: ${(props) => props.theme.text.textFive};
        @media (min-width: 768px) {
          font-size: ${(props) => props.theme.text.textEight};
        }
      }

      .button {
        font-size: ${(props) => props.theme.text.textThreee};

        span {
          display: none;
          @media (min-width: 768px) {
            display: block;
            padding: 0 0 0 ${pxToRem(5)};
          }
        }
      }
    }
  }
`;

const BestHomeLoanRatesContent = styled.div`
  margin: 0 auto;
  padding: ${(props) => props.theme.spacing.spacingFour} 0;

  @media (min-width: 768px) {
    padding: ${(props) => props.theme.spacing.spacingSix} 0
      ${(props) => props.theme.spacing.spacingEight};
    max-width: ${pxToRem(700)};
  }
`;

class PBBestRates extends React.Component {
  state = {
    responseOne: [],
    responseTwo: [],
    responseThree: [],
    responseFour: [],
    responseresponseFiveThree: [],
    isLoading: true,
    errors: null,
  };

  // getLenders = () => {
  // axios
  //   .all([requestOne, requestTwo, requestThree, requestFour, requestFive])
  //   .then(
  //     axios.spread((...responses) => {
  //       const responseOne = responses[0];
  //       const responseTwo = responses[1];
  //       const responseThree = responses[2];
  //       const responseFour = responses[3];
  //       const responseFive = responses[4];

  //       this.setState({
  //         responseOne: responseOne.data.products,
  //         responseTwo: responseTwo.data.products,
  //         responseThree: responseThree.data.products,
  //         responseFour: responseFour.data.products,
  //         responseFive: responseFive.data.products,
  //         isLoading: false,
  //       });
  //     })
  //   )
  //   .catch((error) => this.setState({ error, isLoading: false }));
  // };

  // componentDidMount() {
  // this.getLenders();
  // }

  render() {
    const {
      isLoading,
      responseOne,
      responseTwo,
      responseThree,
      responseFour,
      responseFive,
    } = this.state as any;

    const { title, content, idSelector } = this.props as any;

    const idPrefix = !isNil(idSelector) ? idSelector : `ph-best-rates`;

    return (
      <>
        <div
          id={`${idPrefix}-container`}
          data-container="container"
          className="container">
          {title && (
            <BestHomeLoanRatesLogosTitle
              data-title={`${idPrefix}-title`}
              id={`${idPrefix}-title`}>
              <BlockText key={title._key} blocks={title} />
            </BestHomeLoanRatesLogosTitle>
          )}
          <BestHomeLoanRatesLogos data-logos={`${idPrefix}-logos`}>
            {lenders.map((data, key) => {
              return (
                <BestHomeLoanRatesLogosItem
                  data-logo-item={`${idPrefix}-title-${data.id}`}
                  id={`${idPrefix}-title-${data.id}`}
                  key={key}>
                  <img
                    src={data.url}
                    alt={data.id.toString()}
                    data-image={data.url}
                  />
                </BestHomeLoanRatesLogosItem>
              );
            })}
          </BestHomeLoanRatesLogos>
        </div>
        <BestHomeLoanRatesWrapper id={`${idPrefix}-wrapper`}>
          <div className="container" data-container="container">
            <BestHomeLoanRatesTitle
              data-title="Best Home Loan Rates"
              id={`${idPrefix}-wrapper-title-one`}>
              Best Home Loan Rates
            </BestHomeLoanRatesTitle>
            {!isLoading && responseOne && Array.isArray(responseOne) && (
              <BestHomeLoanRatesTable
                id={`${idPrefix}-wrapper-table`}
                className="table c-table"
                data-table={`${idPrefix}-table`}>
                <BestHomeLoanRatesTableHeading
                  id={`${idPrefix}-wrapper-table-heading`}
                  data-table-heading={`${idPrefix}-table-heading`}>
                  <tr>
                    <th>&nbsp;</th>
                    <th data-th="Home loan product" className="u-align-center">
                      Home loan product
                    </th>
                    <th data-th="Interest rate p.a" className="u-align-center">
                      Interest rate p.a
                    </th>
                    <th
                      data-th="Comparison rate p.a*"
                      className="u-align-center">
                      Comparison rate p.a*
                    </th>
                    <th>&nbsp;</th>
                  </tr>
                </BestHomeLoanRatesTableHeading>
                <BestHomeLoanRatesTableBody>
                  {responseOne.map((lender) => {
                    const {
                      lenderLogoUrl,
                      lenderId,
                      lenderName,
                      productName,
                      productId,
                      rateName,
                      interestRate,
                      comparisonRate,
                    } = lender;

                    return (
                      <tr data-tr={productId} key={productId}>
                        <td data-td={lenderId}>
                          <img width="40" src={lenderLogoUrl} alt={lenderId} />
                        </td>
                        <td data-td={lenderId}>
                          {lenderName} - {productName} - {rateName}
                        </td>
                        <td data-td={lenderId}>{interestRate}%</td>
                        <td data-td={lenderId}>{comparisonRate}%</td>
                      </tr>
                    );
                  })}
                </BestHomeLoanRatesTableBody>
              </BestHomeLoanRatesTable>
            )}
            {!isLoading && responseTwo && Array.isArray(responseTwo) && (
              <>
                <BestHomeLoanRatesTitle
                  data-title="Fixed Rates"
                  id={`${idPrefix}-wrapper-title-two`}>
                  Fixed Rates
                </BestHomeLoanRatesTitle>
                <BestHomeLoanRatesTable className="table c-table">
                  <BestHomeLoanRatesTableHeading>
                    <tr>
                      <th>&nbsp;</th>
                      <th
                        data-th="Home loan product"
                        className="u-align-center">
                        Home loan product
                      </th>
                      <th
                        data-th="Interest rate p.a"
                        className="u-align-center">
                        Interest rate p.a
                      </th>
                      <th
                        data-th="Comparison rate p.a*"
                        className="u-align-center">
                        Comparison rate p.a*
                      </th>
                      <th>&nbsp;</th>
                    </tr>
                  </BestHomeLoanRatesTableHeading>
                  <BestHomeLoanRatesTableBody>
                    {responseTwo.map((lender) => {
                      const {
                        lenderLogoUrl,
                        lenderId,
                        lenderName,
                        productName,
                        productId,
                        rateName,
                        interestRate,
                        comparisonRate,
                      } = lender;

                      return (
                        <tr data-tr={productId} key={productId}>
                          <td data-td={lenderId}>
                            <img
                              width="40"
                              src={lenderLogoUrl}
                              alt={lenderId}
                            />
                          </td>
                          <td data-td={lenderId}>
                            {lenderName} - {productName} - {rateName}
                          </td>
                          <td data-td={lenderId}>{interestRate}%</td>
                          <td data-td={lenderId}>{comparisonRate}%</td>
                        </tr>
                      );
                    })}
                  </BestHomeLoanRatesTableBody>
                </BestHomeLoanRatesTable>
              </>
            )}
            {!isLoading && responseThree && Array.isArray(responseThree) && (
              <>
                <BestHomeLoanRatesTitle
                  data-title="Variable Rates"
                  id={`${idPrefix}-wrapper-title-three`}>
                  Variable Rates
                </BestHomeLoanRatesTitle>
                <BestHomeLoanRatesTable className="table c-table">
                  <BestHomeLoanRatesTableHeading>
                    <tr>
                      <th>&nbsp;</th>
                      <th
                        data-th="Home loan product"
                        className="u-align-center">
                        Home loan product
                      </th>
                      <th
                        data-th="Interest rate p.a"
                        className="u-align-center">
                        Interest rate p.a
                      </th>
                      <th
                        data-th="Comparison rate p.a*"
                        className="u-align-center">
                        Comparison rate p.a*
                      </th>
                      <th>&nbsp;</th>
                    </tr>
                  </BestHomeLoanRatesTableHeading>
                  <BestHomeLoanRatesTableBody>
                    {responseThree.map((lender) => {
                      const {
                        lenderLogoUrl,
                        lenderId,
                        lenderName,
                        productName,
                        productId,
                        rateName,
                        interestRate,
                        comparisonRate,
                      } = lender;

                      return (
                        <tr data-tr={productId} key={productId}>
                          <td data-td={lenderId}>
                            <img
                              width="40"
                              src={lenderLogoUrl}
                              alt={lenderId}
                            />
                          </td>
                          <td data-td={lenderId}>
                            {lenderName} - {productName} - {rateName}
                          </td>
                          <td data-td={lenderId}>{interestRate}% </td>
                          <td data-td={lenderId}>{comparisonRate}%</td>
                        </tr>
                      );
                    })}
                  </BestHomeLoanRatesTableBody>
                </BestHomeLoanRatesTable>
              </>
            )}
            {!isLoading && responseFour && Array.isArray(responseFour) && (
              <>
                <BestHomeLoanRatesTitle
                  data-title="Owner Occupied"
                  id={`${idPrefix}-wrapper-title-four`}>
                  Owner Occupied
                </BestHomeLoanRatesTitle>
                <BestHomeLoanRatesTable className="table c-table">
                  <BestHomeLoanRatesTableHeading>
                    <tr>
                      <th>&nbsp;</th>
                      <th
                        data-th="Home loan product"
                        className="u-align-center">
                        Home loan product
                      </th>
                      <th
                        data-th="Interest rate p.a"
                        className="u-align-center">
                        Interest rate p.a
                      </th>
                      <th
                        data-th="Comparison rate p.a*"
                        className="u-align-center">
                        Comparison rate p.a*
                      </th>
                      <th>&nbsp;</th>
                    </tr>
                  </BestHomeLoanRatesTableHeading>
                  <BestHomeLoanRatesTableBody>
                    {responseFour.map((lender) => {
                      const {
                        lenderLogoUrl,
                        lenderId,
                        lenderName,
                        productName,
                        productId,
                        rateName,
                        interestRate,
                        comparisonRate,
                      } = lender;

                      return (
                        <tr data-tr={productId} key={productId}>
                          <td data-td={lenderId}>
                            <img
                              width="40"
                              src={lenderLogoUrl}
                              alt={lenderId}
                            />
                          </td>
                          <td data-td={lenderId}>
                            {lenderName} - {productName} - {rateName}
                          </td>
                          <td data-td={lenderId}>{interestRate}%</td>
                          <td data-td={lenderId}>{comparisonRate}%</td>
                        </tr>
                      );
                    })}
                  </BestHomeLoanRatesTableBody>
                </BestHomeLoanRatesTable>
              </>
            )}
            {!isLoading && responseFive && Array.isArray(responseFive) && (
              <>
                <BestHomeLoanRatesTitle
                  data-title="Investment Property Loans"
                  id={`${idPrefix}-wrapper-title-five`}>
                  Investment Property Loans
                </BestHomeLoanRatesTitle>
                <BestHomeLoanRatesTable className="table c-table">
                  <BestHomeLoanRatesTableHeading>
                    <tr>
                      <th>&nbsp;</th>
                      <th
                        data-th="Home loan product"
                        className="u-align-center">
                        Home loan product
                      </th>
                      <th
                        data-th="Interest rate p.a"
                        className="u-align-center">
                        Interest rate p.a
                      </th>
                      <th
                        data-th="Comparison rate p.a*"
                        className="u-align-center">
                        Comparison rate p.a*
                      </th>
                      <th>&nbsp;</th>
                    </tr>
                  </BestHomeLoanRatesTableHeading>
                  <BestHomeLoanRatesTableBody>
                    {responseFive.map((lender) => {
                      const {
                        lenderLogoUrl,
                        lenderId,
                        lenderName,
                        productName,
                        rateName,
                        interestRate,
                        comparisonRate,
                      } = lender;

                      return (
                        <tr key={lenderId}>
                          <td data-td={lenderId}>
                            <img
                              width="40"
                              src={lenderLogoUrl}
                              alt={lenderId}
                            />
                          </td>
                          <td data-td={lenderId}>
                            {lenderName} - {productName} - {rateName}
                          </td>
                          <td data-td={lenderId}>{interestRate}%</td>
                          <td data-td={lenderId}>{comparisonRate}%</td>
                        </tr>
                      );
                    })}
                  </BestHomeLoanRatesTableBody>
                </BestHomeLoanRatesTable>
              </>
            )}
          </div>
        </BestHomeLoanRatesWrapper>
        {content && (
          <div className="container">
            <BestHomeLoanRatesContent
              data-content={`${idPrefix}-wrapper-content`}
              id={`${idPrefix}-wrapper-content`}>
              <BlockText key={content._key} blocks={content} />
            </BestHomeLoanRatesContent>
          </div>
        )}
      </>
    );
  }
}

export default PBBestRates;
