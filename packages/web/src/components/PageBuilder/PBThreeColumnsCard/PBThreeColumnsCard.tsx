import React from 'react';
import isNil from 'lodash/isNil';
import styled from 'styled-components';
import { Flex, Box } from 'reflexbox';
import pxToRem from '../../../utils/pxToRem';
import removeAccents from '../../../utils/removeAccents';
import BlockText from '../../BlockText';
import Card from '../../Card';

const PBThreeColumnsCard = (props: any) => {
  const { chooseBackground, title, cardContent, idSelector } = props;

  const PBThreeColumnsCardWrapper = styled.div`
    padding: ${(props) => props.theme.spacing.spacingFour} 0;

    @media (min-width: 768px) {
      padding: ${(props) => props.theme.spacing.spacingEight} 0;
    }

    background: ${(props) => {
      switch (chooseBackground) {
        case 'white':
          return '#ffffff';
        case 'accent':
          return '#ebf0f4';
        default:
          return '#284053';
      }
    }};
  `;

  const PBThreeColumnsCardTitle = styled.div`
    text-align: center;
    h2 {
      color: ${(props) => {
        switch (chooseBackground) {
          case 'white':
            return '#284053';
          case 'accent':
            return '#284053';
          default:
            return '#fff';
        }
      }};
    }
  `;

  const PBThreeColumnsCardContent = styled.div`
    padding: ${(props) => props.theme.spacing.spacingThree};

    &.PBThreeColumnsCardContentCenter {
      text-align: center;
    }

    > div {
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;
      max-width: 100%;
    }

    figure {
      margin: -${(props) => props.theme.spacing.spacingThree} -${(props) =>
          // eslint-disable-next-line prettier/prettier
          props.theme.spacing.spacingThree}
        ${(props) => props.theme.spacing.spacingTwo};
      img {
        border-top-left-radius: ${pxToRem(12)};
        border-top-right-radius: ${pxToRem(12)};
      }
    }

    h2,
    h3 {
      font-family: ${(props) => props.theme.text.textBlack};
    }

    object {
      margin: ${(props) => props.theme.spacing.spacingThree}
        ${(props) => props.theme.spacing.spacingThree} 0;
    }
  `;

  const idPrefix = !isNil(idSelector) ? idSelector : `ph-three-columns-card`;

  return (
    <>
      <PBThreeColumnsCardWrapper
        id={`${idPrefix}-wrapper`}
        data-wrapper={`${idPrefix}-wrapper`}>
        <div className="container" data-container="container">
          {title && (
            <PBThreeColumnsCardTitle
              id={`${idPrefix}-wrapper-title`}
              data-title={removeAccents(title)}>
              <h2>{title}</h2>
            </PBThreeColumnsCardTitle>
          )}
          {cardContent && (
            <Flex flexWrap="wrap" justifyContent="center">
              {cardContent.map((cardContentItem: any, i: any) => (
                <Box
                  key={i}
                  id={`${idPrefix}-wrapper-content-${i}`}
                  width={[1, 1, 1, 1 / 3]}
                  p={3}
                  data-box={i}>
                  <Card>
                    {cardContentItem.alignment !== 'center' ? (
                      <PBThreeColumnsCardContent>
                        <BlockText
                          key={cardContentItem._key}
                          blocks={cardContentItem.text}
                        />
                      </PBThreeColumnsCardContent>
                    ) : (
                      <PBThreeColumnsCardContent className="PBThreeColumnsCardContentCenter">
                        <BlockText
                          key={cardContentItem._key}
                          blocks={cardContentItem.text}
                        />
                      </PBThreeColumnsCardContent>
                    )}
                  </Card>
                </Box>
              ))}
            </Flex>
          )}
        </div>
      </PBThreeColumnsCardWrapper>
    </>
  );
};

export default PBThreeColumnsCard;
