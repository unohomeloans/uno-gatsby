import React, { useEffect } from 'react';
import isNil from 'lodash/isNil';
import styled from 'styled-components';
import pxToRem from '../../../utils/pxToRem';
import BlockText from '../../BlockText';

import lenders from '../../../constants/lenders.json';

const PBRatesLenderWrapper = styled.div`
  width: 100%;
  padding: ${(props) => props.theme.spacing.spacingSix} 0;
`;

const PBRatesLenderTitle = styled.div`
  & > span {
    margin: 0 0 24px;
  }
`;

interface PBRatesLenderProps {
  title: any[] | string | null;
  idSelector: string | null;
}

const PBRatesLenders: React.FC<PBRatesLenderProps> = ({
  title,
  idSelector,
}) => {
  const idPrefix = !isNil(idSelector) ? idSelector : `ph-rates-lenders`;

  return (
    <PBRatesLenderWrapper>
      <div className="container">
        {title && (
          <PBRatesLenderTitle
            id={`${idPrefix}-wrapper-title`}
            data-title={`${idPrefix}-wrapper-title`}>
            {Array.isArray(title) ? (
              <BlockText key={title[0]._key} blocks={title} />
            ) : (
              title
            )}
          </PBRatesLenderTitle>
        )}
      </div>
      <div className="slider">
        <div className="slide-track">
          {lenders.map((data: any, key: any) => {
            return (
              <div className="slide" key={key} data-lender-key={key}>
                <img src={data.url} alt={data.id} data-lender-img={data.url} />
              </div>
            );
          })}
        </div>
      </div>
    </PBRatesLenderWrapper>
  );
};
export default PBRatesLenders;
