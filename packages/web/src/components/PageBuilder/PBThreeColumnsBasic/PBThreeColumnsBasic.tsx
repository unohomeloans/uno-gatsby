/* eslint-disable prettier/prettier */
import React, { useEffect } from 'react';
import isNil from 'lodash/isNil';
import { Flex, Box } from 'reflexbox';
import styled from 'styled-components';
import BlockText from '../../BlockText';
import pxToRem from '../../../utils/pxToRem';
import removeAccents from '../../../utils/removeAccents';

const PBThreeColumnsBasic = (props: any) => {
  const { chooseBackground, title, cardContent, idSelector } = props;

  const PBThreeColumnsCardWrapper = styled.div`
    padding: ${(props) => props.theme.spacing.spacingFour} 0;

    @media (min-width: 768px) {
      padding: ${(props) => props.theme.spacing.spacingEight} 0;
    }

    background: ${(props) => {
      switch (chooseBackground) {
        case 'white':
          return '#ffffff';
        case 'accent':
          return '#ebf0f4';
        default:
          return '#284053';
      }
    }};
  `;

  const PBThreeColumnsCardTitle = styled.div`
    text-align: center;
    margin: 0 0 ${(props) => props.theme.spacing.spacingThree};
    @media (min-width: 992px) {
      margin: 0 0 ${(props) => props.theme.spacing.spacingFive};
    }
    h1 {
      font-family: ${(props) => props.theme.text.textBlack};
      font-size: ${(props) => props.theme.text.textEight};
      font-weight: 400;

      @media (max-width: 768px) {
        font-size: ${(props) => props.theme.text.textSix};
      }
      margin: 0;
    }
    h2 {
      color: ${(props) => {
        switch (chooseBackground) {
          case 'white':
            return '#284053';
          case 'accent':
            return '#284053';
          default:
            return '#fff';
        }
      }};
    }
  `;

  const PBThreeColumnsCardContent = styled.div`
    &.PBThreeColumnsCardContentCenter {
      text-align: center;

      object {
        margin: 0 auto;
      }
    }

    > div {
      display: flex;
      flex-direction: column;
    }

    figure {
      margin: 0;
      img {
        border-top-left-radius: ${pxToRem(12)};
        border-top-right-radius: ${pxToRem(12)};
      }
    }

    h3 {
      font-family: ${(props) => props.theme.text.textBlack};
      // eslint-disable-next-line prettier/prettier
      margin: 0 ${(props) => props.theme.spacing.spacingTwo}
        ${(props) => props.theme.spacing.spacingOne};
    }

    p {
      // eslint-disable-next-line prettier/prettier
      padding: 0 ${(props) => props.theme.spacing.spacingThree}
        ${(props) => props.theme.spacing.spacingThree};
    }

    .button {
    }

    object {
      // eslint-disable-next-line prettier/prettier
      margin: ${(props) => props.theme.spacing.spacingThree}
        ${(props) => props.theme.spacing.spacingThree} 0;
    }
  `;

  const idPrefix = !isNil(idSelector) ? idSelector : `ph-three-columns-basic`;

  return (
    <>
      <PBThreeColumnsCardWrapper
        id={`${idPrefix}-wrapper`}
        data-wrapper={`${idPrefix}-wrapper`}>
        <div className="container" data-container="container">
          {title && (
            <PBThreeColumnsCardTitle
              id={`${idPrefix}-wrapper-title`}
              data-title={removeAccents(title)}>
              <h1>{title}</h1>
            </PBThreeColumnsCardTitle>
          )}
          {cardContent && (
            <Flex flexWrap="wrap" justifyContent="center">
              {cardContent.map((cardContentItem: any, i: any) => (
                <Box
                  key={i}
                  id={`${idPrefix}-wrapper-column-${i}`}
                  width={[1, 1, 1, 1 / 3]}
                  p={3}
                  data-box={i}>
                  <>
                    {cardContentItem.alignment !== 'center' ? (
                      <PBThreeColumnsCardContent
                        id={`${idPrefix}-wrapper-column-${i}-content`}>
                        {Array.isArray(cardContentItem.text) ? (
                          <BlockText
                            key={cardContentItem._key}
                            blocks={cardContentItem.text}
                          />
                        ) : (
                          cardContentItem.text
                        )}
                      </PBThreeColumnsCardContent>
                    ) : (
                      <PBThreeColumnsCardContent
                        id={`${idPrefix}-wrapper-column-${i}-content`}
                        className="PBThreeColumnsCardContentCenter">
                        {Array.isArray(cardContentItem.text) ? (
                          <BlockText
                            key={cardContentItem._key}
                            blocks={cardContentItem.text}
                          />
                        ) : (
                          cardContentItem.text
                        )}
                      </PBThreeColumnsCardContent>
                    )}
                  </>
                </Box>
              ))}
            </Flex>
          )}
        </div>
      </PBThreeColumnsCardWrapper>
    </>
  );
};

export default PBThreeColumnsBasic;
