/* eslint-disable @typescript-eslint/no-var-requires */
import React, { useCallback } from 'react';
import isNil from 'lodash/isNil';
import urlBuilder from '@sanity/image-url';
import styled from 'styled-components';
import pxToRem from '../../../utils/pxToRem';
import BlockText from '../../BlockText';
import Grid from '../../Grid';
import CTALink from '../../CTALink';
import CTAButton from '../../CTAButton';
import getVideoSrc from '../../../utils/getVideoSrc';
import YouTube from 'react-youtube';
import getYouTubeID from 'get-youtube-id';
import randomAlphaNumeric from '../../../utils/randomAlphaNumeric';

const clientConfig = require('../../../../config/sanity-config');

const urlFor = (source: any) =>
  urlBuilder({ ...clientConfig.sanity }).image(source);

interface RenderListProps {
  list: any;
}

const ListItem = styled.div`
  display: flex;
  flex-direction: row;
  margin-bottom: 50px;

  &:last-of-type {
    margin-bottom: 0;
  }

  p {
    margin: 0;
  }
`;

const ListItemNumOrIcon = styled.div`
  display: flex;
  align-items: center;
  font-size: ${pxToRem(48)};
  color: ${(props) => props.theme.colors.colorNavy};
  height: ${pxToRem(62)};
  width: ${pxToRem(62)};
  margin-right: ${pxToRem(32)};
`;

const ListItemTitle = styled.div`
  font-size: ${pxToRem(24)};
  font-family: ${(props) => props.theme.text.textMedium};
  color: ${(props) => props.theme.colors.colorNavy};
  margin-bottom: ${pxToRem(8)};
  line-height: ${pxToRem(38.4)};
`;

const RenderList: React.FC<RenderListProps> = ({ list }) => {
  const mapList = useCallback(
    () =>
      list.content.map((val: any, index: number) => {
        return (
          <ListItem key={randomAlphaNumeric(6)}>
            <ListItemNumOrIcon>
              {!list?.type || list?.type == 'numbered'
                ? index > 9
                  ? index + 1
                  : `0${index + 1}`
                : null}
            </ListItemNumOrIcon>
            <div>
              {val?.title ? <ListItemTitle>{val.title}</ListItemTitle> : null}
              <div>
                <BlockText blocks={val.content} />
              </div>
            </div>
          </ListItem>
        );
      }),
    [list]
  );
  return <>{mapList()}</>;
};

interface GridFlexProps {
  children: React.ReactNode;
  alignment: string;
  mainVideoImage: any;
}

const LeadVideoImageWrapper = styled.div.attrs(
  (props: { $isImage?: string }) => props
)`
  display: flex;
  flex-flow: row nowrap;
  gap: 55px;

  @media (max-width: 768px) {
    flex-flow: column nowrap;
  }

  p {
    display: inline-block;
    width: auto;
  }
  & > div {
    display: inline-block;

    &:first-child {
      width: 69.659%;
      flex-shrink: 0;

      ${({ $isImage }) =>
        $isImage && `@media(max-width: 1200px){width: 576px;}`}
      ${({ $isImage }) => $isImage && `@media(max-width: 992px){width: 432px;}`}
      ${({ $isImage }) => $isImage && `@media(max-width: 768px){width: 325px;}`}

      @media (max-width: 768px) {
        width: 100%;
      }
    }

    iframe {
      flex-shrink: 1;
      height: 460px;
      margin: 0;
      @media (max-width: 768px) {
        height: 300px;
      }
    }
  }
`;

const GridFlex: React.FC<GridFlexProps> = ({
  children,
  alignment,
  mainVideoImage,
}) =>
  mainVideoImage?.asset || mainVideoImage?.url ? (
    <LeadVideoImageWrapper $isImage={mainVideoImage?.asset}>
      {children}
    </LeadVideoImageWrapper>
  ) : (
    <Grid
      width={2}
      gap={99}
      align="center"
      className="grid-container"
      data-grid-container="grid-container"
      data-grid-width={2}>
      {children}
    </Grid>
  );

const PBHero = (props: any) => {
  const {
    alignment,
    chooseBackground,
    backgroundImage,
    title,
    text,
    ctas,
    disclaimer,
    mainImage,
    mainList,
    mainVideo,
    embeddedTitle,
    _key,
    idSelector,
  } = props;

  const PBHeroTittle = styled.div`
    text-align: ${alignment == 'center' ? 'center' : 'left'};
    margin-bottom: ${pxToRem(26)};
    align-self: flex-start;

    h1 {
      margin: 0;
      &:last-of-type {
        margin-bottom: 24px;
      }
    }

    h1,
    h2,
    h3,
    h4 {
      color: ${(props) => {
        switch (chooseBackground) {
          case 'white':
            return props.theme.colors.colorNavy;
          case 'accent':
            return props.theme.colors.colorNavy;
          default:
            return '#FFF';
        }
      }};
    }
  `;

  const PBSpan = styled.span`
    @supports (-webkit-touch-callout: none) {
      margin-bottom: 1rem;
    }
  `;

  const PBHeroWrapper = styled.div`
    padding: ${pxToRem(75)} 0;

    text-align: ${(props) => {
      switch (alignment) {
        case 'center':
          return 'center';
        default:
          return 'inherit';
      }
    }};

    table {
      tr {
        border-bottom: 1px solid ${(props) => {
          switch (chooseBackground) {
            case 'default' || null:
              return props.theme.colors.colorWhite;
              break;
            case 'accent':
              return props.theme.colors.colorBlack;
              break;
            default:
              return props.theme.colors.colorAccent;
              break;
          }
        }};
        th {
          border: none;
          color: ${(props) =>
            chooseBackground == 'default'
              ? props.theme.colors.colorWhite
              : props.theme.colors.colorNavy};
        }
        td {
          border: none;
          color: ${(props) =>
            chooseBackground == 'default'
              ? props.theme.colors.colorWhite
              : props.theme.colors.colorBalck};
          }
        }
      }
    }


    background: ${(props) => {
      switch (chooseBackground) {
        case 'white':
          return '#ffffff';
        case 'accent':
          return props.theme.colors.colorAccent;
        case 'image':
          // return `url(${urlFor(backgroundImage.asset.url)})`;
          return props.theme.colors.colorAccent;
        default:
          return props.theme.colors.colorNavy;
      }
    }};

    .grid-container {
      @media (max-width: 992px) {
        // grid-template-columns: repeat(1, 1fr);
      }
    }

    ${(props) =>
      (chooseBackground == 'default' || !chooseBackground) &&
      `button{background: #fff; color: ${props.theme.colors.colorNavy};
      &:hover { background: rgb(242, 242, 242);}`}

    &.PBHeroWrapperWithImage {
      padding: ${(props) => props.theme.spacing.spacingThree} 0
        ${(props) => props.theme.spacing.spacingSix};
      @media (min-width: 768px) {
        padding: ${(props) => props.theme.spacing.spacingSeven} 0
          ${(props) => props.theme.spacing.spacingSix};
      }
    }
  `;

  const PBHeroWrapperContent = styled.div`
    padding: 0;

    @media (min-width: 768px) {
      padding: 0;
    }

    &.align-center {
      align-self: center;
    }

    h1,
    h2,
    h3 {
      color: ${(props) => {
        switch (chooseBackground) {
          case 'white':
            return props.theme.colors.colorNavy;
          case 'accent':
            return props.theme.colors.colorNavy;
          case 'image':
            return props.theme.colors.colorNavy;
          default:
            return '#fff';
        }
      }};
    }

    p {
      display: block;
      color: ${(props) => {
        switch (chooseBackground) {
          case 'white':
            return props.theme.colors.colorBlack;
          case 'accent':
            return props.theme.colors.colorBlack;
          case 'image':
            return props.theme.colors.colorBlack;
          default:
            return '#fff';
        }
      }};

    max-width: ${(props) => {
      if (alignment == `center`)
        return `${pxToRem(900)}; margin: 0 auto 1rem !important`;
      return `auto`;
    }};

    @media (min-width: 1600px) {
      max-width: ${(props) => {
        if (alignment == `center`)
          return `1000px; margin: 0 auto 1rem !important`;
        return `auto`;
      }};
    }

    p {
      display: block;
      font-size: ${(props) => props.theme.text.textThreee};
      sup {
        font-size: 50%;
        top: -0.75rem;
      }
      @media (min-width: 768px) {
        // font-size: ${pxToRem(24)};
      }
    }

    a {
      font-family: ${(props) => props.theme.text.textBlack};
    }

    button {
      display: block;
    }
  `;

  const PBHeroWrapperCtas = styled.div`
    span {
      width: 100%;
      display: inline-block;
      margin: 0 0 ${(props) => props.theme.spacing.spacingFour};

      @media (min-width: 768px) {
        display: inline;
        margin: 0;
      }
    }
    a,
    button {
      margin: 0 0 ${(props) => props.theme.spacing.spacingTwo};
      font-family: ${(props) => props.theme.text.textBlack};
      @media (min-width: 768px) {
        margin: 0 ${(props) => props.theme.spacing.spacingTwo};
      }

      &.button {
        color: ${(props) => {
          switch (chooseBackground) {
            case 'white':
              return '#fff';
            case 'accent':
              return props.theme.colors.colorNavy;
            default:
              return '#fff';
          }
        }};
      }

      color: ${(props) => {
        switch (chooseBackground) {
          case 'white':
            return props.theme.colors.colorAccent;
          case 'accent':
            return props.theme.colors.colorAccent;
          default:
            return '#fff';
        }
      }};

      &:last-child {
        margin: 0;
        @media (min-width: 768px) {
          margin: 0 ${(props) => props.theme.spacing.spacingTwo};
        }
      }
    }
  `;

  const PBHeroWrapperDisclaimer = styled.div`
    padding: ${(props) => props.theme.spacing.spacingTwo} 0;
    p {
      margin: 0;
      @media (min-width: 768px) {
        margin: inherit;
      }
      font-size: ${(props) => props.theme.text.textOne};
      color: ${(props) => {
        switch (chooseBackground) {
          case 'white':
            return props.theme.colors.colorNavy;
          case 'accent':
            return props.theme.colors.colorNavy;
          case 'image':
            return props.theme.colors.colorNavy;
          default:
            return '#fff';
        }
      }};

      a {
        text-decoration: underline;
      }
    }
  `;

  const PBHeroWrapperImage = styled.div`
    padding: 0;

    &.list {
      margin: 0;

      @media (min-width: 768px) {
        margin: 0;
      }
    }

    img {
      display: block;
      padding: 0;
    }

    &.rounded--accent {
      &:first-child {
        order: 0;
      }
      background: ${(props) => props.theme.colors.colorAccent};
      border-radius: 50%;
      overflow: hidden;
      height: 815px;

      @media (max-width: 1200px) {
        height: 576px;
      }
      @media (max-width: 992px) {
        height: 432px;
        width: 432px;
      }
      @media (max-width: 768px) {
        height: 325px;
        width: 325px !important;
      }

      img {
        margin-top: 10%;
      }
    }

    &.content-image {
      @media (max-width: 768px) {
        order: 1;
        margin: ${(props) => props.theme.spacing.spacingFour} auto 0;
        width: 100%;
      }
    }
  `;
  const PBHeroWrapperVideo = styled.div`
    iframe {
      width: 100%;
      height: 300px;
      margin: 0;

      @media (max-width: 768px) {
        height: 200px;
      }
      @media (max-width: 912px) and (min-height: 720px) and (min-width: 540px) {
        height: 380px;
      }
    }
  `;

  const PBVideo = styled.video`
    margin: ${(props) => props.theme.spacing.spacingFour} 0;
    margin-top: 2.5rem;
    width: 100%;
    height: 350px;
  `;

  const PBHeroImageBackgroundWrapper = styled.div`
    margin-top: ${pxToRem(64)};
  `;
  const idPrefix = !isNil(idSelector) ? idSelector : `ph-hero`;

  const videoSrc = Array.isArray(text) ? getVideoSrc(text) : null;
  // const filteredWithoutVideoText: any[] = [];

  // text &&
  //   text.forEach((textBlock: any) => {
  //     if (textBlock.children) {
  //       textBlock.children.forEach((textString: any) => {
  //         if (!textString.text.includes('--video')) {
  //           filteredWithoutVideoText.push(textBlock);
  //         }
  //       });
  //     } else {
  //       filteredWithoutVideoText.push(textBlock);
  //     }
  //   });

  return (
    <>
      {alignment === 'center' && (
        <PBHeroWrapper
          id={`${idPrefix}-${alignment}-wrapper`}
          data-id={`${idPrefix}-${alignment}-wrapper`}
          style={{
            backgroundRepeat: 'no-repeat',
            backgroundPosition: '50%',
            backgroundSize: 'cover',
          }}
          className={
            (mainImage && mainImage.asset) || mainVideo?.url
              ? 'PBHeroWrapperWithImage'
              : ''
          }>
          <div className="container" data-container="container">
            {mainVideo?.url ? (
              <YouTube videoId={getYouTubeID(mainVideo.url) || ''} />
            ) : null}
            {title && (
              <PBHeroTittle
                id={`${idPrefix}-${alignment}-title`}
                data-title={`${idPrefix}-${alignment}-title`}>
                {Array.isArray(title) ? <BlockText blocks={title} /> : title}
              </PBHeroTittle>
            )}
            {text && (
              <PBHeroWrapperContent
                id={`${idPrefix}-${alignment}-content`}
                data-content={`${idPrefix}-${alignment}-content`}>
                {Array.isArray(text) ? <BlockText blocks={text} /> : text}
                {videoSrc && (
                  <PBVideo width={400} controls>
                    <source type="video/mp4" src={videoSrc} />
                  </PBVideo>
                )}
              </PBHeroWrapperContent>
            )}
            {ctas && (
              <PBHeroWrapperCtas
                id={`${idPrefix}-${alignment}-ctas`}
                data-ctas={`${idPrefix}-${alignment}-ctas`}>
                {ctas.map((cta: any, i: any) => (
                  <span key={i}>
                    {cta.kind === 'button' && (
                      <CTAButton data-key={_key} key={_key} {...cta} />
                    )}
                    {cta.kind === 'link' && (
                      <CTALink data-key={_key} key={_key} {...cta} />
                    )}
                  </span>
                ))}
              </PBHeroWrapperCtas>
            )}
            {disclaimer && (
              <PBHeroWrapperDisclaimer
                id={`${idPrefix}-${alignment}-disclaimer`}
                data-disclaimer={`${idPrefix}-${alignment}-disclaimer`}>
                <BlockText blocks={disclaimer} />
              </PBHeroWrapperDisclaimer>
            )}
            {mainImage && mainImage.asset && (
              <PBHeroWrapperImage
                id={`${idPrefix}-${alignment}-image`}
                data-image={mainImage.asset.url}
                {...(mainImage?.roundedAccentImage
                  ? { className: 'rounded--accent' }
                  : Object.create(null))}>
                <picture>
                  <img
                    style={{ width: '100%' }}
                    srcSet={`${urlFor(mainImage.asset.url).width(
                      320
                    )} 320w,${urlFor(mainImage.asset.url).width(
                      480
                    )} 480w,${urlFor(mainImage.asset.url).width(800)} 800w`}
                    sizes={`(max-width: 320px) 280px,(max-width: 480px) 440px,800px`}
                    src={urlFor(mainImage.asset.url).url.toString()}
                    alt={mainImage.asset.alt}
                  />
                </picture>
              </PBHeroWrapperImage>
            )}
          </div>
        </PBHeroWrapper>
      )}
      {alignment === 'left' && (
        <PBHeroWrapper
          id={`${idPrefix}-${alignment}-wrapper`}
          data-wrapper={`${idPrefix}-${alignment}-wrapper}`}
          style={{
            backgroundRepeat: 'no-repeat',
            backgroundPosition: '50%',
            backgroundSize: 'cover',
          }}
          className={
            (mainImage && mainImage.asset) || mainVideo?.url
              ? 'PBHeroWrapperWithImage'
              : ''
          }>
          <div className="container" data-container="container">
            {mainVideo?.url ? (
              <YouTube videoId={getYouTubeID(mainVideo.url) || ''} />
            ) : null}
            {title && (
              <PBHeroTittle
                id={`${idPrefix}-${alignment}-title`}
                data-title={`${idPrefix}-${alignment}-title`}>
                {Array.isArray(title) ? <BlockText blocks={title} /> : title}
              </PBHeroTittle>
            )}
            <Grid
              width={2}
              gap={32}
              align="center"
              className="grid-container"
              data-grid-container="grid-container"
              data-grid-width={2}>
              <PBHeroWrapperContent
                id={`${idPrefix}-${alignment}-content`}
                className="conten-align-middle">
                {Array.isArray(text) ? <BlockText blocks={text} /> : text}
                {ctas && (
                  <Grid
                    width={2}
                    gap={32}
                    align="center"
                    className="grid-container"
                    data-grid-container="grid-container"
                    data-grid-width={2}>
                    {ctas.map((cta: any, i: any) => (
                      <PBSpan key={i}>
                        {cta.kind === 'button' && (
                          <CTAButton data-cta={_key} key={_key} {...cta} />
                        )}
                        {cta.kind === 'link' && (
                          <CTALink data-ctas={_key} key={_key} {...cta} />
                        )}
                      </PBSpan>
                    ))}
                  </Grid>
                )}
              </PBHeroWrapperContent>
              {mainImage && mainImage.asset && (
                <PBHeroWrapperImage
                  id={`${idPrefix}-${alignment}-image`}
                  data-image={mainImage.asset.url}
                  className={`content-image${
                    mainImage?.roundedAccentImage ? ' rounded--accent' : ''
                  }`}>
                  <picture>
                    <img
                      style={{ width: '100%' }}
                      srcSet={`${urlFor(mainImage.asset.url).width(
                        320
                      )} 320w,${urlFor(mainImage.asset.url).width(
                        480
                      )} 480w,${urlFor(mainImage.asset.url).width(800)} 800w`}
                      sizes={`(max-width: 320px) 280px,(max-width: 480px) 440px,800px`}
                      src={urlFor(mainImage.asset.url).toString() || undefined}
                      alt={mainImage.asset.alt}
                    />
                  </picture>
                </PBHeroWrapperImage>
              )}
            </Grid>
          </div>
        </PBHeroWrapper>
      )}
      {alignment === 'right' && (
        <PBHeroWrapper
          id={`${idPrefix}-${alignment}-wrapper`}
          data-id={`${idPrefix}-${alignment}-wrapper`}
          style={{
            backgroundRepeat: 'no-repeat',
            backgroundPosition: '50%',
            backgroundSize: 'cover',
          }}
          {...((mainImage && mainImage.asset) || mainVideo?.url
            ? { className: 'PBHeroWrapperWithImage' }
            : Object.create(null))}>
          <div className="container" data-container="container">
            {title && (
              <PBHeroTittle
                id={`${idPrefix}-${alignment}-title`}
                data-title={`${idPrefix}-${alignment}-title`}>
                {Array.isArray(title) ? <BlockText blocks={title} /> : title}
              </PBHeroTittle>
            )}
            <GridFlex
              mainVideoImage={mainImage || mainVideo}
              alignment={'right'}>
              {mainImage && mainImage.asset && (
                <PBHeroWrapperImage
                  id={`${idPrefix}-${alignment}-image`}
                  className={`content-image${
                    mainImage?.roundedAccentImage ? ' rounded--accent' : ''
                  }`}
                  data-image={mainImage.asset.url}>
                  <picture>
                    <img
                      style={{ width: '100%' }}
                      srcSet={`${urlFor(mainImage.asset.url).width(
                        320
                      )} 320w,${urlFor(mainImage.asset.url).width(
                        480
                      )} 480w,${urlFor(mainImage.asset.url).width(800)} 800w`}
                      sizes={`(max-width: 320px) 280px,(max-width: 480px) 440px,800px`}
                      src={urlFor(mainImage.asset.url).toString() || undefined}
                      alt={mainImage.asset.alt}
                    />
                  </picture>
                </PBHeroWrapperImage>
              )}
              {mainVideo?.url ? (
                <PBHeroWrapperVideo
                  id={`${idPrefix}-${alignment}-video`}
                  className="content-image ml-0 mb-0 main-video"
                  data-image={mainVideo.url}>
                  <YouTube videoId={getYouTubeID(mainVideo.url) || ''} />
                </PBHeroWrapperVideo>
              ) : null}
              {mainList && (
                <PBHeroWrapperImage
                  id={`${idPrefix}-${alignment}-list`}
                  className="content-image ml-0 list">
                  <RenderList list={mainList} />
                </PBHeroWrapperImage>
              )}
              {embeddedTitle && (
                <PBHeroTittle
                  id={`${idPrefix}-${alignment}-title`}
                  data-title={`${idPrefix}-${alignment}-title`}>
                  {Array.isArray(embeddedTitle) ? (
                    <BlockText blocks={embeddedTitle} />
                  ) : (
                    embeddedTitle
                  )}
                </PBHeroTittle>
              )}
              <PBHeroWrapperContent
                id={`${idPrefix}-${alignment}-content`}
                data-content={`${idPrefix}-${alignment}-content`}
                className={`conten-align-middle${
                  mainVideo?.url || mainImage?.asset ? ' align-center' : ''
                }`}>
                {Array.isArray(text) ? <BlockText blocks={text} /> : text}
                {ctas && (
                  <Grid
                    width={2}
                    gap={32}
                    align="center"
                    className="grid-container"
                    data-grid-container="grid-container"
                    data-grid-width={2}>
                    {ctas.map((cta: any, i: any) => (
                      <span key={i}>
                        {cta.kind === 'button' && (
                          <CTAButton data-cta={_key} key={_key} {...cta} />
                        )}
                        {cta.kind === 'link' && (
                          <CTALink data-cta={_key} key={_key} {...cta} />
                        )}
                      </span>
                    ))}
                  </Grid>
                )}
              </PBHeroWrapperContent>
            </GridFlex>
          </div>
        </PBHeroWrapper>
      )}
      {/* backgroundImage?.asset?.url ? (
        <div className="container">
          <div className="disclaimerWrapperContainer">
            <PBHeroImageBackgroundWrapper>
              <picture>
                <img
                  style={{ width: '100%' }}
                  srcSet={`${urlFor(backgroundImage.asset.url).width(
                    320
                  )} 320w,${urlFor(backgroundImage.asset.url).width(
                    480
                  )} 480w,${urlFor(backgroundImage.asset.url).width(800)} 800w`}
                  sizes={`(max-width: 320px) 280px,(max-width: 480px) 440px,800px`}
                  src={
                    urlFor(backgroundImage.asset.url).toString() || undefined
                  }
                  alt={backgroundImage.asset.alt}
                />
              </picture>
            </PBHeroImageBackgroundWrapper>
          </div>
        </div>
                ) : null */}
    </>
  );
};

export default PBHero;
