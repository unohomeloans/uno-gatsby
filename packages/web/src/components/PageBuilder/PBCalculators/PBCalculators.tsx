/* eslint-disable prettier/prettier */
import React from 'react';
import CumulativeInterestCalculator from '../../CalculatorForms/CumulativeInterestCalculator';
import StampDutyCalculator from '../../CalculatorForms/StampDutycalculator';

interface Props {
  type: string | null;
  rbaRate: number | null;
}

const Calculators: React.FC<Props> = ({ type, rbaRate }) => {
  switch (type) {
    case 'cumulativeInterest':
      if (typeof rbaRate == 'number')
        return <CumulativeInterestCalculator rbaRate={rbaRate} />;
      return null;
    case 'stampDuty':
      return <StampDutyCalculator />;
    default:
      return <p>No calculator has been picked</p>;
  }
};

export default Calculators;
