import React from 'react';
import isNil from 'lodash/isNil';
import { Flex, Box } from 'reflexbox';
import styled from 'styled-components';
import BlockText from '../../BlockText';
import pxToRem from '../../../utils/pxToRem';
import removeAccents from '../../../utils/removeAccents';

const PBTwoColumnsBasic = (props: any) => {
  const { chooseBackground, title, cardContent, idSelector } = props;
  const idPrefix = !isNil(idSelector) ? idSelector : `ph-two-columns-basic`;

  const PBTwoColumnsCardWrapper = styled.div`
    padding: ${(props) => props.theme.spacing.spacingFour} 0;

    @media (min-width: 768px) {
      padding: ${(props) => props.theme.spacing.spacingEight} 0;
    }

    background: ${(props) => {
      switch (chooseBackground) {
        case 'white':
          return '#ffffff';
        case 'accent':
          return '#ebf0f4';
        default:
          return '#284053';
      }
    }};

    .grid-container {
      @media (max-width: 992px) {
        grid-template-columns: repeat(1, 1fr);
      }
    }
  `;

  const PBTwoColumnsCardTitle = styled.div`
    text-align: center;
    margin: 0 0 ${(props) => props.theme.spacing.spacingTwo};
    @media (min-width: 992px) {
      margin: 0 0 ${(props) => props.theme.spacing.spacingFive};
    }
    h2 {
      color: ${(props) => {
        switch (chooseBackground) {
          case 'white':
            return '#284053';
          case 'accent':
            return '#284053';
          default:
            return '#fff';
        }
      }};
    }
  `;

  const PBTwoColumnsCardContent = styled.div`
    &.PBTwoColumnsCardContentCenter {
      text-align: center;

      object {
        margin: 0 auto;
      }
    }

    > div {
      display: flex;
      flex-direction: column;
    }

    figure {
      margin: 0;
      img {
        border-top-left-radius: ${pxToRem(12)};
        border-top-right-radius: ${pxToRem(12)};
      }
    }

    h3 {
      font-family: ${(props) => props.theme.text.textBlack};
      margin: ${(props) => props.theme.spacing.spacingTwo};
      ${(props) => props.theme.spacing.spacingTwo};
    }

    p {
      margin: 0 ${(props) => props.theme.spacing.spacingTwo};
      ${(props) => props.theme.spacing.spacingTwo};
    }

    .button {
      margin: 0 ${(props) => props.theme.spacing.spacingTwo};
      ${(props) => props.theme.spacing.spacingTwo};
    }

    object {
      margin: ${(props) => props.theme.spacing.spacingTwo}
        ${(props) => props.theme.spacing.spacingTwo} 0;
    }
  `;

  return (
    <>
      <PBTwoColumnsCardWrapper
        id={`${idPrefix}-wrapper`}
        data-wrapper={`${idPrefix}-wrapper`}>
        <div className="container" data-container="container">
          {title && (
            <PBTwoColumnsCardTitle
              id={`${idPrefix}-wrapper-title`}
              data-title={removeAccents(title)}>
              <h2>{title}</h2>
            </PBTwoColumnsCardTitle>
          )}
          {cardContent && (
            <Flex flexWrap="wrap" justifyContent="center">
              {cardContent.map((cardContentItem: any, i: any) => (
                <Box
                  key={i}
                  id={`${idPrefix}-wrapper-column-${i}`}
                  width={[1, 1, 1, 1 / 2]}
                  p={3}
                  data-box={i}>
                  <>
                    {cardContentItem.alignment !== 'center' ? (
                      <PBTwoColumnsCardContent
                        id={`${idPrefix}-wrapper-column-${i}-content`}>
                        <BlockText
                          key={cardContentItem._key}
                          blocks={cardContentItem.text}
                        />
                      </PBTwoColumnsCardContent>
                    ) : (
                      <PBTwoColumnsCardContent
                        id={`${idPrefix}-wrapper-column-${i}-content`}
                        className="PBTwoColumnsCardContentCenter">
                        <BlockText
                          key={cardContentItem._key}
                          blocks={cardContentItem.text}
                        />
                      </PBTwoColumnsCardContent>
                    )}
                  </>
                </Box>
              ))}
            </Flex>
          )}
        </div>
      </PBTwoColumnsCardWrapper>
    </>
  );
};

export default PBTwoColumnsBasic;
