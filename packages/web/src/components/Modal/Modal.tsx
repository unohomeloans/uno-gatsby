import React, { MouseEventHandler, useContext, useState } from 'react';
import styled from 'styled-components';
import { BrokerContext } from '../../context/BrokerContext';
import { BrokerLinksContext } from '../../context/BrokerLinksContext';
import CloseIcon from '../../images/svg/close.svg';

const ModalWrapper = styled.div`
  display: flex;
  position: fixed;
  overflow: hidden;
  opacity: 0;
  visibility: hidden;
  top: 0;
  left: 0;
  justify-content: center;
  align-items: center;
  z-index: 10000;
  width: 100vw;
  height: calc(100vh + 0px);
`;

const Overlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  z-index: -1;
  height: calc(100vh + 0px);
  width: 100%;
  background: rgba(31, 31, 31, 0.4);
`;

const CloseWrapper = styled.label`
  position: absolute;
  top: 25px;
  right: 25px;

  svg {
    cursor: pointer;
  }
`;

const ModalBody = styled.div`
  width: 450px;
  border-radius: 2rem;
  background: #fff;

  @media (max-width: 768px) {
    width: 360px;
  }
`;

const ModalHeader = styled.div`
  padding: 4rem 2rem 2rem 2rem;
  text-align: center;

  h1,
  h2,
  h3 {
    color: ${(props) => props.theme.colors.colorNavy};
  }
`;

const ModalContent = styled.div`
  display: flex;
  flex-flow: column wrap;
  align-items: center;
  width: 100%;
  padding: 2rem 2rem 4rem 2rem;
  text-align: center;

  .button {
    margin-top: 0;
    margin-bottom: 2rem;
    max-width: fit-content;
    font-size: inherit;
  }

  .button:last-child {
    margin-top: 2rem;
    margin-bottom: 0;
  }

  @media (max-width: 768px) {
    .button {
      white-space: normal;
      word-wrap: break-word;
    }
  }
`;

interface FirstModalProps {
  links?: any;
  name?: string;
  onlineApplicationHandle: MouseEventHandler;
  bookBrokerHandle: MouseEventHandler;
  showBrokerButton: boolean;
}

const FirstModal: React.FC<FirstModalProps> = ({
  links,
  name,
  onlineApplicationHandle,
  bookBrokerHandle,
  showBrokerButton,
}) => (
  <>
    <ModalHeader>
      <h2>Get started</h2>
    </ModalHeader>
    <ModalContent>
      <button
        className="button button--md button--height--md button--primary button--rounded"
        onClick={onlineApplicationHandle}>
        Start Online Application
      </button>
      {showBrokerButton && (
        <button
          className="button button--md button--height--md button--primary button--rounded"
          onClick={bookBrokerHandle}>
          Book time with {links?.calendly && name ? name : `a broker`}
        </button>
      )}
    </ModalContent>
  </>
);

interface SecondModalProps {
  referrer?: string;
}

const SecondModal: React.FC<SecondModalProps> = ({ referrer }) => (
  <>
    <ModalHeader>
      <h3>Online application</h3>
    </ModalHeader>
    <ModalContent>
      <a
        className="button button--md button--height--md button--primary button--rounded"
        href={`https://app.unohomeloans.com.au/new-loan${
          referrer ? '?uno-referrer=' + referrer : '/'
        }`}>
        I want to buy a property
      </a>
      <a
        className="button button--md button--height--md button--primary button--rounded"
        href={`https://app.unohomeloans.com.au/address${
          referrer ? '?uno-referrer=' + referrer : '/'
        }`}>
        I want to refinance my current loan
      </a>
    </ModalContent>
  </>
);

interface ModalProps {
  showBrokerButton?: boolean;
}

const Modal: React.FC<ModalProps> = ({ showBrokerButton = true }) => {
  const [showModal, setShowModal] = useState(false);
  const [showOnline, setShowOnline] = useState(false);
  const [broker] = useContext(BrokerContext);
  const [links] = useContext(BrokerLinksContext);

  const handleClose = (e: any) => {
    e.preventDefault();
    setShowModal(false);
    setShowOnline(false);
  };

  const handleInput = (e: any) => {
    // e.preventDefault();
    setShowModal(!showModal);
  };

  const onlineApplicationHandle = (e: any) => {
    e.preventDefault();
    setShowOnline(true);
  };

  const bookBrokerHandle = (e: any) => {
    e.stopPropagation();
    // e.stopImmediatePropagation();
    e.preventDefault();
    setShowModal(false);
    (window as any).Calendly.initPopupWidget({
      url: links?.calendly
        ? links.calendly
        : 'https://calendly.com/uno-customer-care/uno-quick-call/',
    });
    return false;
  };
  return (
    <>
      <input
        type="radio"
        id="show"
        name="modal-control"
        value="show"
        hidden
        onChange={handleInput}
        checked={showModal}
      />
      <ModalWrapper className="modal">
        <Overlay onClick={handleClose} />
        <ModalBody>
          {!showOnline ? (
            <FirstModal
              links={links}
              name={broker}
              bookBrokerHandle={bookBrokerHandle}
              onlineApplicationHandle={onlineApplicationHandle}
              showBrokerButton={showBrokerButton}
            />
          ) : (
            <SecondModal referrer={links?.referrer ? links.referrer : ''} />
          )}
        </ModalBody>
        <CloseWrapper htmlFor="hide" onClick={handleClose}>
          <CloseIcon />
        </CloseWrapper>
      </ModalWrapper>
    </>
  );
};

export default Modal;
