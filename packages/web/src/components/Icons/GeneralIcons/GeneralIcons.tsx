import React from 'react';

interface SVGProp {
  color: string;
  svgClasses: string;
}

export const ArrowTopRight: React.FC<SVGProp> = ({
  color,
  svgClasses = '',
}) => (
  <svg
    width="15"
    height="15"
    viewBox="0 0 15 15"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...(svgClasses ? { className: svgClasses } : Object.create(null))}>
    <path
      d="M2 1H14V13"
      stroke={color}
      strokeWidth="2"
      strokeLinecap="square"
    />
    <path
      d="M13.4 1.59998L2 13"
      stroke={color}
      strokeWidth="2"
      strokeLinecap="square"
      strokeLinejoin="round"
    />
  </svg>
);

export const Trophy: React.FC<SVGProp> = ({ color, svgClasses = '' }) => (
  <svg
    width="25"
    height="37"
    viewBox="0 0 25 37"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...(svgClasses ? { className: svgClasses } : Object.create(null))}>
    <path
      d="M23.4545 34.8182H1.63634M12.5454 25.7273C10.1344 25.7273 7.82206 24.7695 6.11718 23.0646C4.41231 21.3598 3.45452 19.0475 3.45452 16.6364V2.09094H21.6363V16.6364C21.6363 19.0475 20.6785 21.3598 18.9737 23.0646C17.2688 24.7695 14.9565 25.7273 12.5454 25.7273V25.7273Z"
      stroke={color}
      strokeWidth="3"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);

export const Person: React.FC<SVGProp> = ({ color, svgClasses = '' }) => (
  <svg
    width="33"
    height="37"
    viewBox="0 0 33 37"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...(svgClasses ? { className: svgClasses } : Object.create(null))}>
    <path
      d="M25.6363 11.1819C25.6363 12.9799 25.1032 14.7375 24.1042 16.2325C23.1053 17.7275 21.6855 18.8927 20.0244 19.5808C18.3632 20.2688 16.5353 20.4489 14.7719 20.0981C13.0084 19.7473 11.3886 18.8815 10.1172 17.6101C8.84579 16.3387 7.97997 14.7189 7.62919 12.9554C7.27842 11.1919 7.45845 9.36406 8.14652 7.70291C8.83459 6.04176 9.99979 4.62196 11.4948 3.62304C12.9898 2.62411 14.7474 2.09094 16.5454 2.09094C18.9565 2.09094 21.2688 3.04873 22.9737 4.75361C24.6785 6.45848 25.6363 8.77079 25.6363 11.1819ZM11.0909 20.2728C8.67982 20.2728 6.36751 21.2305 4.66264 22.9354C2.95776 24.6403 1.99997 26.9526 1.99997 29.3637V29.3637V31.1819C1.99997 31.1819 5.63633 34.8182 16.5454 34.8182C27.4545 34.8182 31.0909 31.1819 31.0909 31.1819V29.3637C31.0909 26.9526 30.1331 24.6403 28.4282 22.9354C26.7233 21.2305 24.411 20.2728 22 20.2728H11.0909Z"
      stroke={color}
      strokeWidth="3.5"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);

export const Heart: React.FC<SVGProp> = ({ color, svgClasses = '' }) => (
  <svg
    width="37"
    height="34"
    viewBox="0 0 37 34"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...(svgClasses ? { className: svgClasses } : Object.create(null))}>
    <path
      d="M32.3091 5.34548C33.9756 7.01864 34.9113 9.28395 34.9113 11.6455C34.9113 14.007 33.9756 16.2723 32.3091 17.9455L18.5454 31.8182L4.7818 17.9455C3.54406 16.6994 2.70139 15.1159 2.35927 13.3932C2.01715 11.6705 2.19079 9.88511 2.85844 8.26062C3.52609 6.63612 4.6581 5.24466 6.11275 4.26041C7.56741 3.27617 9.28007 2.74288 11.0363 2.72729C13.3848 2.74154 15.6326 3.6825 17.2909 5.34548C17.7745 5.82407 18.196 6.36162 18.5454 6.94548C18.8948 6.36162 19.3163 5.82407 19.8 5.34548C20.617 4.51649 21.5907 3.85819 22.6645 3.40887C23.7382 2.95954 24.8906 2.72815 26.0545 2.72815C27.2185 2.72815 28.3708 2.95954 29.4446 3.40887C30.5183 3.85819 31.492 4.51649 32.3091 5.34548V5.34548Z"
      stroke={color}
      strokeWidth="3.5"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);
