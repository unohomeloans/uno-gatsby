import React, { FormEvent, useEffect, useState } from 'react';
// import { graphql, useStaticQuery } from 'gatsby';
// import randomAlphaNumeric from '../../../utils/randomAlphaNumeric';
import styled from 'styled-components';
import pxToRem from '../../../utils/pxToRem';
const { runFlankLego } = require('sdk-flank');

interface StampDutyProps {
  children?: null;
}
interface StampDutyState {
  PROPVAL: number;
  POSTCODE: number;
  LOANTYPE: string;
  FHB: boolean;
  PROPSTATUS: string;
  NODEPEND: number;
  TOTGROSINC: number;
  PSECSTATE: string;
}

type StampDutyKeyTypes =
  | 'PROPVAL'
  | 'POSTCODE'
  | 'LOANTYPE'
  | 'FHB'
  | 'PROPSTATUS'
  | 'NODEPEND'
  | 'TOTGROSINC'
  | 'PSECSTATE';

// const STAMPDUTYQUERY = graphql`
//   query AllPostCodesQuery {
//     allDataJson(
//       filter: {
//         schema: { fields: { elemMatch: { name: { eq: "postcodes" } } } }
//       }
//     ) {
//       edges {
//         node {
//           data {
//             state
//             postcodes
//           }
//         }
//       }
//     }
//   }
// `;

const StampDutyCalculatorWrapper = styled.div`
  padding: 150px 0 130px 0;
  @media screen and (max-width: 768px) {
    padding: 60px 0;
  }
`;

const StampContainerWrapper = styled.div`
  display: flex;
  flex-direction: row;
  gap: 20px;
  & > div {
    min-width: 33.33%;
  }

  @media screen and (max-width: 768px) {
    flex-direction: column;
  }
`;

const StampDutyWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;

  @media screen and (max-width: 768px) {
    max-width: 380px;
    flex-direction: column-reverse;
  }
`;

const StampDutyInfo = styled.div`
  padding: 30px;
  width: auto;
  border-radius: 8px;
  background: #ebf0f4;
`;

const StampHeader = styled.div`
  line-height: 120%;
  font-size: 72px;
  font-family: 'Nunito Sans', sans-serif;
  font-weight: 800;
  color: #284053;
  margin-bottom: 20px;

  @media screen and (max-width: 768px) {
    font-size: 60px;
  }
`;

const CostsSavings = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  postion: relative;
  margin-bottom: 1rem;
  border-radius: 30px;
  overflow: hidden;
  transition: width 0.5s ease-in-out;
`;

const CostsBar = styled.div.attrs((props: { $width?: string }) => props)`
  height: 60px;
  border-radius: 30px;
  width: ${({ $width }) => $width};
  box-shadow: 30px 0 0 0 ${({ theme }) => theme.colors.colorAccent};
  background-color: ${({ theme }) => theme.colors.colorNavy};
  transition: width 0.5s ease-in-out;
`;

const SavingsBar = styled.div.attrs((props: { $width?: string }) => props)`
  height: 60px;
  border-radius: 0 30px 30px 0;
  width: ${({ $width }) => $width};
  background-color: ${({ theme }) => theme.colors.colorAccent};
`;

const BarTitlesWrapper = styled.div`
  display: flex;
  flex-direction: row;

  & > * {
    width: 50%
    display: flex;
    flex-direction: row;
    &:first-child, &:first-child h3  {
      color: ${({ theme }) => theme.colors.colorNavy};
    }
    &:last-child, &:last-child h3 {
      color: ${({ theme }) => theme.colors.colorGrey};
    }
  }
`;

const SectionHead = styled.span`
  display: block;
  position: relative;
  font-family: ${(props) => props.theme.text.textBook};
  font-size: ${pxToRem(15)};
  line-height: ${pxToRem(18)};
  letter-spacing: 0.15em;
  text-transform: uppercase;
  color: ${(props) => props.theme.text.colorBlack};
  margin-bottom: ${pxToRem(16)};
`;

const BreakDownWrapper = styled.div`
  & > *:first-child {
    cursor: pointer;
  }
`;

const ArrowUp = styled.i`
  display: inline-block;
  position: absolute;
  top: 7px;
  margin-left: ${pxToRem(16)};
  border: solid black;
  border-width: 0 1px 1px 0;
  padding: 3px;
  transform: rotate(-135deg);
  -webkit-transform: rotate(-135deg);
`;

const ArrowDown = styled.i`
  display: inline-block;
  position: absolute;
  top: 2px;
  margin-left: ${pxToRem(16)};
  border: solid black;
  border-width: 0 1px 1px 0;
  padding: 3px;
  transform: rotate(45deg);
  -webkit-transform: rotate(45deg);
`;

const BreakDownContent = styled.div`
  & > div {
    h4 {
      margin-bottom: 0;
    }
    div:last-child {
      span {
        font-weight: 700;
      }
    }
    &:first-child {
      color: ${({ theme }) => theme.colors.colorNavy};
      margin-bottom: 30px;
    }
    &:last-child {
      color: ${({ theme }) => theme.colors.colorGrey};
    }
  }
`;

const Row = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

const StampDutyCalculator: React.FC = () => {
  const [state, setState] = useState({
    PROPVAL: 500000,
    PROPSTATUS: 'ESTABLISHED',
    LOANTYPE: 'OO',
    FHB: 0,
  } as any);
  const [showBreakdown, setShowBreakdown] = useState(true);
  const [sDutyBreakdown, setSDutyBreakdown] = useState([0, 0, 0]);
  const [estimatedCosts, setEstimatedCosts] = useState(0);
  const [mySavings, setMySavings] = useState(50000);

  useEffect(() => {
    handleSubmit();
  }, [
    state.PROPSTATUS,
    state.LOANTYPE,
    state.FHB,
    state?.PSECSTATE,
    state?.NODEPEND,
  ]);

  useEffect(() => {
    const timer = setTimeout(() => handleSubmit(), 1000);
    return () => {
      clearTimeout(timer);
    };
  }, [state]);

  const handleChange = (field: StampDutyKeyTypes, value: number | string) => {
    const newValue = Object.create(null);
    newValue[field] = value;
    setState({ ...state, ...newValue });
  };

  const callStampDutyLego = async (payload: {}) => {
    payload = JSON.stringify(payload);
    const clientId = process.env.GATSBY_FLANK_ID;
    const clientSecret = process.env.GATSBY_FLANK_SECRET;
    const res = await runFlankLego(
      clientId,
      clientSecret,
      'autologic-1',
      488,
      payload
    );
    return res;
  };

  // const renderPostCodes = useCallback(
  //   () =>
  //     allPostCodesPerState
  //       .filter((el: any) => el.state == state?.PSECSTATE)
  //       .map((val: any) =>
  //         val.postcodes
  //           .sort((a: number, b: number) => a - b)
  //           .map((pc: any) => (
  //             <option key={randomAlphaNumeric(5)} value={pc}>
  //               {pc}
  //             </option>
  //           ))
  //       ),
  //   [state?.PSECSTATE]
  // );

  const handleSubmit = async () => {
    // e.preventDefault();
    // console.log(state);
    if (Object.entries(state).length < 5) return;
    const newState = state;
    if (newState?.PSECSTATE != 'ACT') {
      delete newState?.NODEPEND;
      delete newState?.TOTGROSINC;
    }
    newState.FHB = newState.FHB == 1;
    newState.PROPVAL = parseInt(newState.PROPVAL);
    const res = await callStampDutyLego(newState);
    const autologicRes = JSON.parse(res?.payload);
    let stampDutyBreakdown = autologicRes?.products
      .filter((pd: any) => pd.id == 'SDCALC')[0]
      ?.properties.filter(
        (prp: any) =>
          // prp.id == 'DUTISDVAL' ||
          prp.id == 'TRANSD' || prp.id == 'REGMORTFEE' || prp.id == 'REGTRANFEE'
      );
    const TRANSD =
      stampDutyBreakdown?.filter((prp: any) => prp.id == 'TRANSD')[0]
        ?.value[0] || 0;
    const REGMORTFEE =
      stampDutyBreakdown?.filter((prp: any) => prp.id == 'REGMORTFEE')[0]
        ?.value[0] || 0;
    const REGTRANFEE =
      stampDutyBreakdown?.filter((prp: any) => prp.id == 'REGTRANFEE')[0]
        ?.value[0] || 0;
    setSDutyBreakdown([TRANSD, REGMORTFEE, REGTRANFEE]);
    setEstimatedCosts(TRANSD + REGMORTFEE + REGTRANFEE);
    return null;
  };

  return (
    <StampDutyCalculatorWrapper>
      <div className="disclaimerWrapperContainer">
        <div className="container">
          <StampContainerWrapper>
            <div>
              <div className="form-row">
                <div className="form-control">
                  <label htmlFor="my-savings">My Savings</label>
                  <div className="input-container">
                    <div className="prefix">&#36;</div>
                    <input
                      type="number"
                      id="my-savings"
                      value={mySavings}
                      onChange={(e) => setMySavings(parseInt(e.target.value))}
                    />
                  </div>
                </div>
              </div>
              <div className="form-row">
                <div className="form-control">
                  <label htmlFor="prop-val">Property Value</label>
                  <div className="input-container">
                    <div className="prefix">&#36;</div>
                    <input
                      type="number"
                      id="prop-val"
                      value={state?.PROPVAL}
                      onChange={(e) => handleChange('PROPVAL', e.target.value)}
                    />
                  </div>
                </div>
              </div>
              <div className="form-row">
                <div className="form-control">
                  <label htmlFor="state">State</label>
                  <div className="input-container">
                    <select
                      id="state"
                      value={state?.PSECSTATE || ''}
                      onChange={(e) =>
                        handleChange('PSECSTATE', e.target.value)
                      }>
                      <option value="">Select One</option>
                      <option value="QLD">QLD</option>
                      <option value="NSW">NSW</option>
                      <option value="VIC">VIC</option>
                      <option value="WA">WA</option>
                      <option value="ACT">ACT</option>
                      <option value="SA">SA</option>
                      <option value="TAS">TAS</option>
                      <option value="NT">NT</option>
                    </select>
                  </div>
                </div>
              </div>
              {state?.PSECSTATE == 'ACT' && (
                <>
                  <div className="form-row">
                    <div className="form-control">
                      <label htmlFor="no-depend">
                        ACT ONLY - Number of Dependent Children
                      </label>
                      <div className="input-container">
                        <input
                          type="number"
                          id="no-depend"
                          value={state?.NODEPEND || 0}
                          onChange={(e) =>
                            handleChange('NODEPEND', e.target.value)
                          }
                          disabled={state?.PSECSTATE != 'ACT'}
                        />
                      </div>
                    </div>
                  </div>
                  <div className="form-row">
                    <div className="form-control">
                      <label htmlFor="total-gros-inc">
                        ACT ONLY - Total Gross Income
                      </label>
                      <div className="input-container">
                        <div className="prefix">&#36;</div>
                        <input
                          type="number"
                          id="tot-gros-inc"
                          value={state?.TOTGROSINC || 0}
                          onChange={(e) =>
                            handleChange('TOTGROSINC', e.target.value)
                          }
                          disabled={state?.PSECSTATE != 'ACT'}
                        />
                      </div>
                    </div>
                  </div>
                </>
              )}
              {/* <div className="form-row">
          <div className="form-control">
            <label htmlFor="postcode">Postcode</label>
            <div className="input-container">
              <select
                id="postcode"
                value={state?.POSTCODE || ''}
                onChange={(e) => handleChange('POSTCODE', e.target.value)}>
                <option>
                  Please pick a
                  {(!state?.PSECSTATE && ' state first') || ' postcode'}
                </option>
                {renderPostCodes()}
              </select>
            </div>
          </div>
        </div> */}
              <div className="form-row">
                <div className="form-control">
                  <label htmlFor="loan-type">Property Use</label>
                  <div className="options-bar">
                    <span
                      onClick={(e) => handleChange('LOANTYPE', 'OO')}
                      {...(state?.LOANTYPE == 'OO'
                        ? { className: 'active' }
                        : Object.create(null))}>
                      A Home to Live In
                    </span>
                    <span
                      onClick={(e) => handleChange('LOANTYPE', 'INV')}
                      {...(state?.LOANTYPE == 'INV'
                        ? { className: 'active' }
                        : Object.create(null))}>
                      Investment
                    </span>
                  </div>
                </div>
              </div>
              <div className="form-row">
                <div className="form-control">
                  <label htmlFor="fhb" className="checkbox-container">
                    <input
                      type="checkbox"
                      id="fhb"
                      onChange={(e) =>
                        handleChange('FHB', state?.FHB == 1 ? 0 : 1)
                      }
                    />
                    <span className="checkmark" />
                    First Home Buyer
                  </label>
                </div>
              </div>
              <div className="form-row">
                <div className="form-control">
                  <label htmlFor="prop-status">Property Type</label>
                  <div className="options-bar">
                    <span
                      onClick={(e) => handleChange('PROPSTATUS', 'ESTABLISHED')}
                      {...(state?.PROPSTATUS == 'ESTABLISHED' ||
                      !state?.PROPSTATUS
                        ? { className: 'active' }
                        : Object.create(null))}>
                      Existing
                    </span>
                    <span
                      onClick={(e) => handleChange('PROPSTATUS', 'NEWHOME')}
                      {...(state?.PROPSTATUS == 'NEWHOME'
                        ? { className: 'active' }
                        : Object.create(null))}>
                      New
                    </span>
                    <span
                      onClick={(e) => handleChange('PROPSTATUS', 'VACANTLAND')}
                      {...(state?.PROPSTATUS == 'VACANTLAND'
                        ? { className: 'active' }
                        : Object.create(null))}>
                      Land
                    </span>
                  </div>
                  {/* <div className="input-container">
                      <option value="CONSTRUCTION">Construction</option>
                  </div> */}
                </div>
              </div>
              {/* <div className="form-row">
                <button
                  type="submit"
                  className="button button--primary button--rounded">
                  Calculate
                </button>
              </div> */}
            </div>
            <StampDutyWrapper>
              {/* <StampHeader>Calculated Stamp Duty</StampHeader> */}
              {/* <CostsSavings>
                <CostsBar
                  $width={`${
                    (mySavings - estimatedCosts >= 0 &&
                      (estimatedCosts /
                        (estimatedCosts + (mySavings - estimatedCosts))) *
                        100) ||
                    100
                  }%`}
                />
                <SavingsBar
                  $width={`${
                    (mySavings - estimatedCosts >= 0 &&
                      ((mySavings - estimatedCosts) /
                        (estimatedCosts + (mySavings - estimatedCosts))) *
                        100) ||
                    0
                  }%`}
                />
              </CostsSavings> */}
              <BarTitlesWrapper>
                <div>
                  <SectionHead>ESTIMATED UPFRONT COSTS</SectionHead>
                  <h3>${estimatedCosts.toFixed(2)}</h3>
                </div>
                <div>
                  <SectionHead>SAVINGS LEFT FOR DEPOSIT</SectionHead>
                  <h3>${(mySavings - estimatedCosts).toFixed(2)}</h3>
                </div>
              </BarTitlesWrapper>
              <BreakDownWrapper>
                <SectionHead onClick={(_) => setShowBreakdown(!showBreakdown)}>
                  {showBreakdown ? 'COLLAPSE' : 'VIEW'} BREAKDOWN{' '}
                  {showBreakdown ? <ArrowUp /> : <ArrowDown />}
                </SectionHead>
                {showBreakdown ? (
                  <BreakDownContent>
                    <div>
                      <h4>Estimated upfront costs</h4>
                      <Row>
                        <span>Stamp duty</span>
                        <span>${sDutyBreakdown[0].toFixed(2)}</span>
                      </Row>
                      <Row>
                        <span>Mortgage fee</span>
                        <span>${sDutyBreakdown[1].toFixed(2)}</span>
                      </Row>
                      <Row>
                        <span>Transfer fee</span>
                        <span>${sDutyBreakdown[2].toFixed(2)}</span>
                      </Row>
                      <Row>
                        <span>Total</span>
                        <span>${estimatedCosts.toFixed(2)}</span>
                      </Row>
                    </div>
                    <div>
                      <h4>Savings left for deposit</h4>
                      <Row>
                        <span>My savings</span>
                        <span>${mySavings}</span>
                      </Row>
                      <Row>
                        <span>- Estimated upfront cost</span>
                        <span>
                          -$
                          {estimatedCosts.toFixed(2)}
                        </span>
                      </Row>
                      <Row>
                        <span>Total</span>
                        <span>
                          ${(mySavings - (estimatedCosts || 0)).toFixed(2)}
                        </span>
                      </Row>
                    </div>
                  </BreakDownContent>
                ) : null}
              </BreakDownWrapper>
              {/* <StampDutyInfo>
                <h3>$ {sDuty}</h3>
              </StampDutyInfo> */}
            </StampDutyWrapper>
          </StampContainerWrapper>
        </div>
      </div>
    </StampDutyCalculatorWrapper>
  );
};

export default StampDutyCalculator;
