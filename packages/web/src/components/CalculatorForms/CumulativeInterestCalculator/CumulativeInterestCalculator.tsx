import React, { FormEvent, useState } from 'react';
import axios from 'axios';

interface IntialState {
  MORTBAL: number | null;
  REMAINMON: number | null;
  VARIABLERATE: number | null;
  FIXEDRATE: number | null;
  FREQRISES: number | null;
  AMOUNTRIS: number | null;
}
interface ErrorState {
  MORTBAL: string | null;
  REMAINMON: string | null;
  VARIABLERATE: string | null;
  FIXEDRATE: string | null;
  FREQRISES: string | null;
  AMOUNTRIS: string | null;
}

interface Props {
  rbaRate: number;
}

interface CalcState {
  variable: number | null;
  fixed: number | null;
}

type InputFields =
  | 'MORTBAL'
  | 'REMAINMON'
  | 'VARIABLERATE'
  | 'FIXEDRATE'
  | 'FREQRISES'
  | 'AMOUNTRIS';

const CumulativeInterestCalculator: React.FC<Props> = ({ rbaRate }) => {
  const [formState, setFormState] = useState({} as IntialState);
  const [calcState, setCalcState] = useState({} as CalcState);
  const [errorState, setErrorState] = useState({} as ErrorState);

  const handleChange = (name: InputFields, value: string) => {
    const newState = { ...formState };
    newState[name] = parseFloat(value);
    setFormState(newState);
  };
  const handleSubmit = async (e: FormEvent) => {
    e.preventDefault();
    const {
      MORTBAL,
      REMAINMON,
      VARIABLERATE,
      FIXEDRATE,
      FREQRISES,
      AMOUNTRIS,
    } = formState;
    if (typeof REMAINMON != 'number') return;

    const reqBody = {
      schema: 'REFICALC',
      params: [
        {
          ids: ['-FREQRISES'],
          value: [FREQRISES, FREQRISES],
        },
        {
          ids: ['-AMOUNTRIS'],
          value: [AMOUNTRIS, 0],
        },
        {
          ids: ['-MORTBAL'],
          value: [MORTBAL, MORTBAL],
        },
        {
          ids: ['-REMAINMON'],
          value: [REMAINMON * 12, REMAINMON * 12],
        },
        {
          ids: ['-VARRATE'],
          value: [VARIABLERATE, FIXEDRATE],
        },
        {
          ids: ['-INITRBA'],
          value: rbaRate,
        },
      ],
    };

    const getCumulativeInterest = await axios
      .post('https://poe.unohomeloans.com.au/search-api/search', reqBody, {
        headers: {
          'x-api-key': process.env.GATSBY_CALC_API_KEY || '',
        },
      })
      .then((res) => res.data);
    const cumulativeInterest = getCumulativeInterest.products[0].properties
      .filter((val: any) => val.id == 'CUMULINT1')[0]
      ?.value.at(-1);

    setCalcState({
      variable: cumulativeInterest[0],
      fixed: cumulativeInterest[1],
    });
  };
  return (
    <div className="container">
      <h1>Cumulative Interest Calculator</h1>
      <form onSubmit={handleSubmit}>
        <div className="form-row">
          <div className="form-control">
            <label htmlFor="rba-rate">Current RBA Rate</label>
            <div className="input-container calculated">
              <input
                className="prefix"
                type="number"
                id="rba-rate"
                name="rbaRate"
                value={rbaRate}
                readOnly
              />
              <div className="input-icon suffix">&#37;</div>
            </div>
          </div>
        </div>
        <div className="form-row">
          <div className="form-control">
            <label htmlFor="mortbal">Current Loan Amount</label>
            <div className="input-container">
              <div className="prefix">&#36;</div>
              <input
                type="number"
                className="suffix"
                id="mortbal"
                name="MORTBAL"
                min={1}
                value={formState?.MORTBAL || ''}
                onChange={(e) => handleChange('MORTBAL', e.target.value)}
                required
              />
            </div>
          </div>
          <div className="form-control">
            <label htmlFor="remainmon">Years Left on Loan</label>
            <div className="input-container">
              <input
                type="number"
                id="remainmon"
                name="remainmon"
                value={formState?.REMAINMON || ''}
                step=".5"
                min={0.5}
                onChange={(e) => handleChange('REMAINMON', e.target.value)}
                required
              />
            </div>
          </div>
        </div>
        <div className="form-row">
          <div className="form-control">
            <label htmlFor="variablerate">Variable Rate</label>
            <div className="input-container">
              <input
                type="number"
                className="prefix"
                id="variablerate"
                name="variablerate"
                value={formState?.VARIABLERATE || ''}
                min={0.01}
                step=".01"
                onChange={(e) => handleChange('VARIABLERATE', e.target.value)}
                required
              />
              <div className="input-icon suffix">&#37;</div>
            </div>
          </div>
          <div className="form-control">
            <label htmlFor="fixedrate">2 Year Fixed Rate</label>
            <div className="input-container">
              <input
                type="number"
                className="prefix"
                id="fixedrate"
                name="fixedrate"
                value={formState?.FIXEDRATE || ''}
                step=".01"
                min={0.01}
                onChange={(e) => handleChange('FIXEDRATE', e.target.value)}
                required
              />
              <div className="input-icon suffix">&#37;</div>
            </div>
          </div>
        </div>
        <div className="form-row">
          <div className="form-control">
            <label htmlFor="freqrises">Frequency of Rate Rises</label>
            <div className="input-container">
              <input
                type="number"
                className="prefix--wd"
                id="freqrises"
                name="freqrises"
                value={formState?.FREQRISES || ''}
                min={3}
                max={8}
                onChange={(e) => handleChange('FREQRISES', e.target.value)}
                required
              />
              <div className="suffix--wd">Months</div>
            </div>
          </div>
          <div className="form-control">
            <label htmlFor="amountris">Amount of Raise</label>
            <div className="input-container">
              <input
                type="number"
                id="amountris"
                className="prefix"
                name="amountris"
                value={formState?.AMOUNTRIS || ''}
                step=".01"
                min={0.01}
                onChange={(e) => handleChange('AMOUNTRIS', e.target.value)}
                required
              />
              <div className="input-icon suffix">&#37;</div>
            </div>
          </div>
        </div>
        <div className="form-row">
          <button
            type="submit"
            className="button button--md button--primary"
            disabled={
              Object.keys(formState).length != 6 &&
              Object.entries(formState).filter((val) => typeof val == 'number')
                .length != 6
            }>
            Calculate
          </button>
        </div>
      </form>
      <div>
        <div className="form-row">
          <div className="form-control">
            <label htmlFor="variable-rate">Calculated Variable Rate</label>
            <div className="input-container calculated">
              <input
                className="prefix"
                type="number"
                id="variable-rate"
                name="variableRate"
                value={calcState?.variable ? calcState.variable : ''}
                readOnly
              />
              <div className="input-icon suffix">&#37;</div>
            </div>
            <div className="form-control">
              <label htmlFor="fixed-rate">Calculated Fixed Rate</label>
              <div className="input-container calculated">
                <input
                  className="prefix"
                  type="number"
                  id="fixed-rate"
                  name="fixedRate"
                  value={calcState?.fixed ? calcState.fixed : ''}
                  readOnly
                />
                <div className="input-icon suffix">&#37;</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CumulativeInterestCalculator;
