import { useStaticQuery, graphql } from 'gatsby';

export default (): any => {
  const { site } = useStaticQuery(
    graphql`
      query SiteMetaData {
        site {
          siteMetadata {
            title
            siteUrl
            menuLinks {
              name
              link
              items {
                name
                link
              }
            }
            footerLinks {
              uno {
                name
                link
              }
              calculators {
                name
                link
              }
              compare {
                name
                link
              }
              resources {
                name
                link
              }
            }
            socialLinks {
              name
              title
              link
              icon
            }
            footerCopyright
          }
        }
      }
    `
  );

  return site.siteMetadata;
};
