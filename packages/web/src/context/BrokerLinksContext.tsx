import React, { createContext, useState } from 'react';
import randomAlphaNumeric from '../utils/randomAlphaNumeric';

const initialValue: Array<any> = [{}, () => null];
const BrokerLinksContext = createContext(initialValue);

interface BrokerLinksProviderProps {
  children: React.ReactNode;
}

const BrokerLinksProvider: React.FC<BrokerLinksProviderProps> = ({
  children,
}) => {
  const [links, setLinks] = useState({});

  return (
    <BrokerLinksContext.Provider
      key={randomAlphaNumeric(5)}
      value={[links, setLinks]}>
      {children}
    </BrokerLinksContext.Provider>
  );
};

export { BrokerLinksContext, BrokerLinksProvider };
