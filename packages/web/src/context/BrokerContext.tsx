// connect to NEAR
import React, { createContext, useState } from 'react';
import randomAlphaNumeric from '../utils/randomAlphaNumeric';

const initialValue: Array<any> = ['', () => null];
const BrokerContext = createContext(initialValue);

interface BrokerProviderProps {
  children: React.ReactNode;
}

const BrokerProvider: React.FC<BrokerProviderProps> = ({ children }) => {
  const [broker, setBroker] = useState('');

  return (
    <BrokerContext.Provider
      key={randomAlphaNumeric(5)}
      value={[broker, setBroker]}>
      {children}
    </BrokerContext.Provider>
  );
};

export { BrokerContext, BrokerProvider };
