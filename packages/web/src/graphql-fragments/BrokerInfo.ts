import { graphql } from 'gatsby';

export const BrokerInfo = graphql`
  fragment BrokerInfo on SanityBroker {
    id
    _rawContent(resolveReferences: { maxDepth: 10 })
    content {
      main {
        name
        email
        location
        tel
        slug {
          current
        }
        ctas {
          listCta
          modalCta
        }
        links {
          referrer
          calendly
        }
      }
      meta {
        metaDescription
        metaTitle
        openImage {
          asset {
            gatsbyImageData(formats: WEBP, fit: FILLMAX, placeholder: BLURRED)
          }
        }
      }
    }
  }
`;
