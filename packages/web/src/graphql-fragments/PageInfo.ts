import { graphql } from 'gatsby';

export const PageInfo = graphql`
  fragment PageInfo on SanityPage {
    id
    _rawContent(resolveReferences: { maxDepth: 10 })
    content {
      main {
        slug {
          current
        }
        title
      }
      meta {
        metaDescription
        metaTitle
        openImage {
          asset {
            gatsbyImageData(formats: WEBP, fit: FILLMAX, placeholder: BLURRED)
          }
        }
      }
    }
  }
`;
