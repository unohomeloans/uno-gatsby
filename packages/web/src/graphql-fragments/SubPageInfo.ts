import { graphql } from 'gatsby';

export const SubPageInfo = graphql`
  fragment SubPageInfo on SanityPage {
    id
    _rawParent(resolveReferences: { maxDepth: 10 })
    content {
      main {
        slug {
          current
        }
        title
      }
      meta {
        metaDescription
        metaTitle
      }
    }
  }
`;
