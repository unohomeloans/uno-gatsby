/* eslint-disable prettier/prettier */
import React from 'react';
import { graphql, Link } from 'gatsby';
import styled from 'styled-components';
import { motion } from 'framer-motion';
import isNil from 'lodash/isNil';
import sortArray from 'sort-array';
import { Flex, Box } from 'reflexbox';
import pxToRem from '../utils/pxToRem';
import BlogPost from '../components/Blog/BlogPost';
import BlogPostFeatured from '../components/Blog/BlogPostFeatured';
import Disclaimer from '../components/Disclaimer';
import GraphQLErrorList from '../components/GraphQLErrorList';
import HeroImage from '../components/HeroImage';
import Layout from '../components/Layout';
import SEO from '../components/SEO';

import defatulSocialImage from '../images/uno-shared-default.jpg';

export const query = graphql`
  query CategoryTemplateQuery($id: String!) {
    category: sanityCategory(id: { eq: $id }) {
      id
      title
      description
      categoryImage {
        asset {
          title
          gatsbyImageData(fit: FILLMAX, placeholder: BLURRED)
        }
      }
      featuredPosts
      posts {
        _id
        title
        excerpt
        publishedAt
        categories {
          _id
          title
        }
        author {
          name
        }
        mainImage {
          asset {
            gatsbyImageData(formats: WEBP, fit: FILLMAX, placeholder: BLURRED)
          }
        }
        slug {
          current
        }
        featured
      }
    }
    categories: allSanityCategory {
      nodes {
        title
        id
        slug {
          current
        }
      }
    }
    site: sanitySiteSettings(_id: { regex: "/(drafts.|)siteSettings/" }) {
      title
      description
      _rawDisclaimer(resolveReferences: { maxDepth: 10 })
    }
  }
`;

const CategoryHeader = styled.div`
  background: ${(props) => props.theme.colors.colorAccent};
`;
const CategoryHeaderContent = styled.div`
  h1 {
    // letter-spacing: -1px;
    color: ${(props) => props.theme.colors.colorNavy};
    // font-family: ${(props) => props.theme.text.textHeavy};
    margin: 0;
  }

  p {
    font-size: ${(props) => props.theme.text.textThree};
    color: ${(props) => props.theme.colors.colorBlack};
    // margin: 0;

    // @media (min-width: 768px) {
    //   font-size: ${(props) => props.theme.text.textFive};
    // }
  }
`;

const CategoryHeaderContentWrapper = styled.div`
  padding-top: ${pxToRem(90)};
  padding-bottom: ${pxToRem(83)};
  margin: 0 auto;
  text-align: center;

  @media (min-width: 769px) {
    max-width: 700px;
  }
`;

const CategoryHeaderNav = styled.div`
  position: relative;
  background: ${(props) => props.theme.colors.colorNavy};
  text-align: center;
`;

const CategoryHeaderList = styled.ul`
  position: relative;
  padding: ${pxToRem(8)} ${(props) => props.theme.spacing.spacingTwo};
  margin: 0;

  /* 👇 Let's hack!! */
  &::-webkit-scrollbar {
    display: none;
  }

  @media (min-width: 768px) {
    justify-content: start;
    padding: ${pxToRem(8)} ${(props) => props.theme.spacing.spacingTwo};
  }

  @media (min-width: 992px) {
    margin: 0 auto;
    max-width: 73rem;
  }
`;
const CategoryHeaderListItem = styled.li`
  margin: 0;
  list-style-type: none;
  padding: ${(props) => props.theme.spacing.spacingOne};
  font-size: ${(props) => props.theme.text.textTwo};
  display: inline-block;

  /* @media (min-width: 768px) {
    flex: 1 1 100%;
    text-align: center;
    &:not(:first-child) {
      padding: inherit;
    }
  } */

  a {
    font-family: ${(props) => props.theme.text.textMedium};
    color: ${(props) => props.theme.colors.colorWhite};
    font-size: ${(props) => props.theme.text.textTwo};
    text-decoration: none;
    font-weight: normal;
  }
`;
const CategoryContentTopStories = styled.div`
  display: flex;
  flex-direction: row;
  padding: ${(props) => props.theme.spacing.spacingTwo}
    ${(props) => props.theme.spacing.spacingTwo};
  background: ${(props) => props.theme.colors.colorAccent};

  @media (min-width: 768px) {
    padding: ${(props) => props.theme.spacing.spacingEight};
  }

  .container {
    padding: 0;
    @media (min-width: 768px) {
      padding: 0 ${pxToRem(15)};
    }
  }

  .CategoryContentTopStoriesGrid {
    @media (max-width: 768px) {
      grid-template-columns: repeat(1, 1fr);
    }
  }
`;

const CategoryContentTopStoriesTitle = styled.div`
  text-align: center;
  margin: ${(props) => props.theme.spacing.spacingTwo} 0
    ${(props) => props.theme.spacing.spacingFour};

  @media (min-width: 768px) {
    margin: 0 0 ${(props) => props.theme.spacing.spacingFour};
  }
`;

const CategoryContentPosts = styled(motion.div)`
  display: flex;
  flex-flow: column nowrap;
  gap: 80px;
  padding-top: ${pxToRem(66)};
  padding-bottom: ${pxToRem(120)};
  @media (min-width: 768px) {
    padding: ${(props) => props.theme.spacing.spacingSeven} 0;
  }
`;

const CategoryContentNoPosts = styled.div`
  padding: ${(props) => props.theme.spacing.spacingFour} 0
    ${(props) => props.theme.spacing.spacingTwo};
  text-align: center;
`;

const listVariants = {
  visible: {
    opacity: 1,
    transition: {
      staggerChildren: 0.4,
    },
  },
  hidden: {
    opacity: 0,
    transition: {
      when: 'afterChildren',
      staggerChildren: 0.4,
      staggerDirection: -1,
    },
  },
};

const itemVariants = {
  visible: { opacity: 1, y: 20 },
  hidden: { opacity: 0, y: 0 },
};

const Category = (props: any) => {
  const { data = {}, errors, idSelector } = props;
  const { title, description, posts, categoryImage, featuredPosts } =
    data.category || {};
  const site = (data || {}).site;

  const categoriesNodes = data.categories.nodes.map((category: any) => ({
    ...category,
    slug: category.slug.current,
  }));

  const pageDisclaimer = site;

  const getSocialImage =
    categoryImage?.asset?.gatsbyImageData || defatulSocialImage;

  const idPrefix = !isNil(idSelector)
    ? idSelector
    : `page-category-${title.toLowerCase()}`;

  return (
    <Layout headerBackground="#EBF0F4">
      <>
        {errors && <GraphQLErrorList errors={errors} />}
        {!data.category && <p>No category data</p>}
        {categoryImage && (
          <CategoryHeader
            data-category-header={`${idPrefix}-header-image`}
            id={`${idPrefix}-header-image`}>
            {/* <HeroImage
              title={title}
              fluid={categoryImage.asset.gatsbyImageData}
              height="252px"
              mobileHeight="350px"
              data-category-image={categoryImage.asset.gatsbyImageData}> */}
            <CategoryHeaderContent className="container">
              <CategoryHeaderContentWrapper>
                <h1 data-category-h1={title}>{title}</h1>
                {description && (
                  <p data-category-description={description}>{description}</p>
                )}
              </CategoryHeaderContentWrapper>
            </CategoryHeaderContent>
            {/* </HeroImage> */}
            {categoriesNodes && (
              <CategoryHeaderNav id={`${idPrefix}-header-nav`}>
                <CategoryHeaderList>
                  {categoriesNodes.map((category: any, key: any) => (
                    <CategoryHeaderListItem
                      data-category-key={category.id}
                      key={key}>
                      <Link
                        data-category-link={category.slug}
                        to={`/categories/${category.slug}/`}>
                        {category.title}
                      </Link>
                    </CategoryHeaderListItem>
                  ))}
                </CategoryHeaderList>
              </CategoryHeaderNav>
            )}
          </CategoryHeader>
        )}
        {posts ? (
          <>
            {featuredPosts && (
              <CategoryContentTopStories id={`${idPrefix}-top-stories`}>
                <div className="container">
                  <CategoryContentTopStoriesTitle
                    id={`${idPrefix}-top-stories-title`}>
                    <div className="container">
                      <h2 data-key={posts.id} key={posts.id}>
                        Top Stories
                      </h2>
                    </div>
                  </CategoryContentTopStoriesTitle>
                  <Flex flexWrap="wrap" justifyContent="center">
                    {sortArray(posts, { by: 'publishedAt', order: 'desc' }).map(
                      (post: any, key: any) => (
                        <React.Fragment key={key}>
                          {post.featured && (
                            <Box
                              width={[1, 1, 1, 1 / 3]}
                              p={3}
                              key={post._id}
                              data-box={post._id}>
                              {post && <BlogPostFeatured {...post} />}
                            </Box>
                          )}
                        </React.Fragment>
                      )
                    )}
                  </Flex>
                </div>
              </CategoryContentTopStories>
            )}
            <CategoryContentPosts
              id={`${idPrefix}-posts`}
              className="container"
              initial="hidden"
              animate="visible"
              exit="hidden"
              variants={listVariants}>
              {sortArray(posts, { by: 'publishedAt', order: 'desc' }).map(
                (post: any) => (
                  <motion.div
                    data-key={post._id}
                    key={post._id}
                    variants={itemVariants}>
                    {post && <BlogPost {...post} />}
                  </motion.div>
                )
              )}
            </CategoryContentPosts>
          </>
        ) : (
          <CategoryContentNoPosts
            id={`${idPrefix}-no-posts`}
            className="container">
            <h2>Sorry no posts available!</h2>
          </CategoryContentNoPosts>
        )}
      </>
      <Disclaimer {...pageDisclaimer} />
    </Layout>
  );
};

export default Category;

export const Head = (props: any) => {
  const { data = {} } = props;
  const { title, description, categoryImage } = data.category || {};
  const getSocialImage =
    categoryImage?.asset?.gatsbyImageData || defatulSocialImage;
  return (
    <SEO
      title={title}
      description={description}
      meta={[
        {
          property: 'og:image',
          content: getSocialImage?.images?.fallback?.src || getSocialImage,
        },
      ]}
    />
  );
};
