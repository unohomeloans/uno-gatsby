import React from 'react';
import { graphql } from 'gatsby';
import Layout from '../components/Layout';
import Disclaimer from '../components/Disclaimer';
import SEO from '../components/SEO';
import GraphQLErrorList from '../components/GraphQLErrorList';
import ModalButton from '../components/ModalButton/ModalButton';
import PBHero from '../components/PageBuilder/PBHero';
import PBThreeColumnsCard from '../components/PageBuilder/PBThreeColumnsCard';
import PBThreeColumnsBasic from '../components/PageBuilder/PBThreeColumnsBasic';
import PBTwoColumnsBasic from '../components/PageBuilder/PBTwoColumnsBasic';
import PBYellowBar from '../components/PageBuilder/PBYellowBar';
import PBCustomersFeedback from '../components/PageBuilder/PBCustomersFeedback';
import PBDisclaimer from '../components/PageBuilder/PBDisclaimer';
import PBTable from '../components/PageBuilder/PBTable';
import PBRatesLenders from '../components/PageBuilder/PBRatesLenders';
import PBBestRates from '../components/PageBuilder/PBBestRates';
import defatulSocialImage from '../images/uno-shared-default.jpg';
import PBBrokersList from '../components/PageBuilder/PBBrokersList';
import PBCalculators from '../components/PageBuilder/PBCalculators';
import PBArticle from '../components/PageBuilder/PBArticle';

export const query = graphql`
  query SubPageTemplateQuery($id: String!) {
    route: sanityRoute(id: { eq: $id }) {
      slug {
        current
      }
      page {
        ...PageInfo
      }
      _rawParent(resolveReferences: { maxDepth: 10 })
      useSiteTitle
    }
    site: sanitySiteSettings(_id: { regex: "/(drafts.|)siteSettings/" }) {
      title
      description
      disclaimer: _rawDisclaimer(resolveReferences: { maxDepth: 10 })
    }
  }
`;

const SubPage = (props: any) => {
  const { data, errors } = props;

  if (errors) {
    return (
      <Layout>
        <GraphQLErrorList errors={errors} />
      </Layout>
    );
  }

  const site = (data || {}).site;

  if (!site) {
    throw new Error(
      'Missing "Site settings". Open the studio at http://localhost:3333 and add some content to "Site settings" and restart the development server.'
    );
  }

  const page = data.page || data.route.page;

  const contentType = (page._rawContent.main.modules || [])
    .filter((c: any) => !c.disabled)
    .map((c: any) => c._type);

  const content = (page._rawContent.main.modules || [])
    .filter((c: any) => !c.disabled)
    .map((c: any, i: any) => {
      let el = null;
      switch (c._type) {
        case 'alert':
          el = <PBYellowBar key={c._key} {...c} />;
          break;
        case 'hero':
          el = <PBHero key={c._key} {...c} />;
          break;
        case 'threeColumnsCard':
          el = <PBThreeColumnsCard key={c._key} {...c} />;
          break;
        case 'threeColumnsBasic':
          el = <PBThreeColumnsBasic key={c._key} {...c} />;
          break;
        case 'twoColumnsBasic':
          el = <PBTwoColumnsBasic key={c._key} {...c} />;
          break;
        case 'ctaColumns':
          el = <PBHero key={c._key} {...c} />;
          break;
        case 'disclaimer':
          el = <PBDisclaimer key={c._key} {...c} />;
          break;
        case 'customerFeedback':
          el = <PBCustomersFeedback key={c._key} {...c} />;
          break;
        case 'ratesLenders':
          el = <PBRatesLenders key={c._key} {...c} />;
          break;
        case 'oldTable':
          el = <PBTable key={c._key} {...c} />;
          break;
        case 'bestRates':
          el = <PBBestRates key={c._key} {...c} />;
          break;
        case 'brokersList':
          el = <PBBrokersList key={c._key} {...c} />;
          break;
        case 'calculator':
          el = <PBCalculators key={c._key} {...c} />;
          break;
        case 'article':
          el = <PBArticle key={c._key} {...c} />;
          break;
        default:
          el = null;
      }
      return el;
    });
  const pageDisclaimer = site;

  return (
    <Layout
      showCalendlyWidget={contentType.includes('brokersList') ? false : true}
      headerBackground={
        contentType.indexOf('hero') == 0
          ? content[0]?.props?.chooseBackground == 'accent' ||
            content[0]?.props?.chooseBackground == 'image'
            ? '#EBF0F4'
            : '#fff'
          : '#fff'
      }>
      {content}
      <Disclaimer {...pageDisclaimer} />
    </Layout>
  );
};

export default SubPage;

export const Head = (props: any) => {
  const { data } = props;
  const page = data.page || data.route.page;

  const pageTitle =
    page.content && page.content.meta
      ? page.content.meta.metaTitle
      : page.title;
  const pageDescription = data.route && page.content.meta.metaDescription;
  const getSocialImage = page.content.meta?.openImage?.asset?.gatsbyImageData
    ? page.content.meta.openImage.asset.gatsbyImageData.images.fallback.src
    : defatulSocialImage;

  const checkForFeedbackComponent = (page._rawContent.main.modules || [])
    .filter((c: any) => !c.disabled)
    .map((c: any) => c._type)
    .filter((val: any) => val == 'customerFeedback');

  return (
    <SEO
      title={pageTitle}
      description={pageDescription}
      meta={[
        {
          property: 'og:image',
          content: getSocialImage,
        },
      ]}
      {...(checkForFeedbackComponent.length
        ? {
            script: [
              {
                src: 'https://cdn.productreview.com.au/assets/widgets/loader.js',
              },
            ],
          }
        : Object.create(null))}
    />
  );
};
