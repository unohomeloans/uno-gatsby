import React from 'react';
import { graphql } from 'gatsby';
import styled from 'styled-components';
import { GatsbyImage } from 'gatsby-plugin-image';
import pxToRem from '../utils/pxToRem';
import useSiteMetadata from '../hooks/useSiteMetadata';
import BlockText from '../components/BlockText';
import Disclaimer from '../components/Disclaimer';
import Layout from '../components/Layout';
import Hero from '../components/Hero';
// import Thumbs from '../components/Thumbs';
import SEO from '../components/SEO';
import GraphQLErrorList from '../components/GraphQLErrorList';
import CategoryNav from '../components/CategoryNav';

// import IcoTickSVG from '../images/ico-tick.svg';
import defatulSocialImage from '../images/uno-shared-default.jpg';
// import {
//   FaceBookIcon,
//   LinkedInIcon,
//   TwitterIcon,
// } from '../components/Icons/SocialIcons/SocialIcons';

const PostTitle = styled.div`
  text-align: center;
  background: #ebf0f4;
  padding-top: ${pxToRem(150)};
  padding-bottom: ${pxToRem(75)};

  h1 {
    font-family: ${(props) => props.theme.text.textBlack};
    font-size: ${pxToRem(72)};
    font-weight: 400;
    color: ${(props) => props.theme.colors.colorNavy};
    line-height: ${pxToRem(86)};
    margin: 0;
  }
`;

const PostTitleExcerpt = styled.p`
  font-family: ${(props) => props.theme.text.textMedium};
  font-size: ${pxToRem(24)};
  line-height: ${pxToRem(38.4)};
  color: ${(props) => props.theme.colors.colorBlack};
`;

const ArticleWrapper = styled.article`
  padding: ${pxToRem(150)} 0 ${pxToRem(133)};
`;

const ArticleContent = styled.div`
  // border-bottom: 1px solid ${(props) => props.theme.colors.colorGrey02};
  // border-top: 1px solid ${(props) => props.theme.colors.colorGrey02};
  // padding: ${(props) => props.theme.spacing.spacingThree} 0;
  position: relative;
  max-width: 48.125rem;
  margin: 0 auto;

  ol {
    counter-reset: fancy-counter;
    list-style: none;
    padding-left: 20px;
  }

  ol li {
    margin: 0 0 1.5rem 0;
    counter-increment: fancy-counter;
    position: relative;
    line-height: 1.5;
  }

  ol li::before {
    content: counter(fancy-counter);
    color: ${(props) => props.theme.colors.colorWhite};
    font-family: ${(props) => props.theme.text.textHeavy};
    font-size: 1.5rem;
    position: absolute;
    --size: 32px;
    left: calc(-1 * var(--size) - 10px);
    line-height: var(--size);
    width: var(--size);
    height: var(--size);
    top: 0;
    background: ${(props) => props.theme.colors.colorNavy};
    border-radius: 50%;
    text-align: center;
  }

  .custom-banner {
    & > div {
      padding: 32.5px 24px;
    }
    .bg-circle {
      img {
        right: -60px;
      }
    }
    .custom-banner-cta {
      width: 70%;

      @media (max-width: 768px) {
        width: 100%;
      }
    }
  }
`;

const SideBarSocialShare = styled.div`
  display: flex;
  position: absolute;
  flex-flow: column nowrap;
  gap: 1.875rem;
  z-index: 1;
  left: -3rem;

  @media (max-width: 768px) {
    position: static;
    flex-flow: row nowrap;
    margin-bottom: ${pxToRem(24)};
    left: auto;
    float: right;
  }

  @media (max-width: 992px) {
    left: 0;
  }

  @media (max-width: 1200px) {
    left: 0;
  }
`;

const PostFooterSocialShare = styled.div`
  display: flex;
  flex-flow: row nowrap;
  gap: ${pxToRem(30)};
`;

const ArticleContentMeta = styled.div`
  padding: ${(props) => props.theme.spacing.spacingThree} 0;
  @media (min-width: 992px) {
    padding: ${(props) => props.theme.spacing.spacingThree} ${pxToRem(100)};
  }
`;

const ArticleCategoryNavBar = styled.div`
  position: relative;
  background: ${(props) => props.theme.colors.colorBlack};
  text-align: center;

  &.no-hero {
    margin: 0 0 ${(props) => props.theme.spacing.spacingSix};
  }
`;

const ArticleCategoryNavBarList = styled.ul`
  position: relative;
  padding: ${pxToRem(20)} ${(props) => props.theme.spacing.spacingTwo};
  margin: 0;

  /* 👇 Let's hack!! */
  &::-webkit-scrollbar {
    display: none;
  }

  @media (min-width: 768px) {
    justify-content: start;
    padding: ${pxToRem(10)} ${(props) => props.theme.spacing.spacingTwo};
  }

  @media (min-width: 992px) {
    margin: 0 auto;
    max-width: 73rem;
  }
`;

const ArticleCategoryNavBarListItem = styled.li`
  margin: 0;
  list-style-type: none;
  padding: ${(props) => props.theme.spacing.spacingOne};
  font-size: ${(props) => props.theme.text.textTwo};
  display: inline-block;

  a {
    font-family: ${(props) => props.theme.text.textMedium};
    color: ${(props) => props.theme.colors.colorWhite};
  }
`;

export const query = graphql`
  query BlogPostTemplate($id: String!) {
    post: sanityPost(id: { eq: $id }) {
      id
      title
      _rawBody(resolveReferences: { maxDepth: 5 })
      excerpt
      mainImage {
        asset {
          gatsbyImageData(
            formats: WEBP
            fit: FILLMAX
            placeholder: BLURRED
            layout: CONSTRAINED
            width: 600
          )
        }
      }
      heroImage {
        asset {
          gatsbyImageData(
            formats: WEBP
            fit: FILLMAX
            placeholder: BLURRED
            layout: CONSTRAINED
            width: 1520
          )
        }
        layout
      }
      author {
        name
        image {
          asset {
            gatsbyImageData(
              formats: WEBP
              fit: FILLMAX
              placeholder: BLURRED
              layout: CONSTRAINED
              width: 120
            )
          }
        }
      }
      publishedAt(formatString: "Do MMMM YYYY")
      _updatedAt(formatString: "Do MMMM YYYY")
      slug {
        current
      }
      categories {
        _id
        title
      }
      seoDescription
      seoTitle
    }
    categories: allSanityCategory {
      nodes {
        title
        id
        slug {
          current
        }
      }
    }
    related: allSanityPost(
      filter: { slug: { current: { ne: null } } }
      limit: 2
      sort: { fields: [publishedAt], order: DESC }
    ) {
      nodes {
        title
        slug {
          current
        }
        categories {
          _id
          title
        }
      }
    }
    site: sanitySiteSettings(_id: { regex: "/(drafts.|)siteSettings/" }) {
      title
      description
      _rawDisclaimer(resolveReferences: { maxDepth: 10 })
    }
  }
`;

const Post = (props: any) => {
  const { data, errors } = props;
  const post = data && data.post;
  const site = (data || {}).site;

  const categoriesNodes = data.categories.nodes.map((category: any) => ({
    ...category,
    slug: category.slug.current,
  }));
  const { siteUrl } = useSiteMetadata();

  // Icons Here 👇
  // const icons = [
  //   {
  //     name: 'fb',
  //     svg: <FaceBookIcon svgClasses="icn-5" />,
  //     link: `https://www.facebook.com/sharer/sharer.php?u=${
  //       siteUrl + '/' + encodeURIComponent(post.slug.current)
  //     }`,
  //   },
  //   {
  //     name: 'twitter',
  //     svg: <TwitterIcon svgClasses="icn-5" />,
  //     link: `https://twitter.com/intent/tweet/?text=${post.title}&url=${
  //       siteUrl + '/' + encodeURIComponent(post.slug.current)
  //     }%2F&via=unohomeloans`,
  //   },
  //   {
  //     name: 'linkedIn',
  //     svg: <LinkedInIcon svgClasses="icn-5" />,
  //     link: `https://www.linkedin.com/shareArticle?mini=true&url=${
  //       siteUrl + '/' + encodeURIComponent(post.slug.current)
  //     }&title=${encodeURIComponent(post.title)}&source=${encodeURIComponent(
  //       post.title
  //     )}`,
  //   },
  // ];

  const metaTitle = (
    <div className="article__header--meta">
      <div className="article__header--meta__wrapper">
        {post.author && post.author.image && (
          <div className="article__header--meta__wrapper__image">
            <GatsbyImage
              image={post.author.image.asset.gatsbyImageData}
              alt={post.author.name}
            />
          </div>
        )}
        <div className="article__header--meta__wrapper__content">
          {post.author && post.author.name && (
            <div className="article__header--meta__wrapper__content--name">
              {post.author.name}
            </div>
          )}
          {post.publishedAt && (
            <div className="article__header--meta__wrapper__content--date">
              {post.publishedAt}
            </div>
          )}
        </div>
      </div>
    </div>
  );

  if (errors) {
    return (
      <Layout>
        <GraphQLErrorList errors={errors} />
      </Layout>
    );
  }

  const postTitle = post.seoTitle ? post.seoTitle : post.title;

  return (
    <div>
      <Layout headerBackground="#EBF0F4">
        <PostTitle>
          <div className="container">
            <h1>{postTitle}</h1>
            <ArticleContentMeta id={`post-content-meta`}>
              {metaTitle}
            </ArticleContentMeta>
            <PostTitleExcerpt>{post.excerpt}</PostTitleExcerpt>
          </div>
        </PostTitle>
        {/*post.heroImage ? (
          <>
            {/*categoriesNodes && (
              <ArticleCategoryNavBar id={`navbar`}>
                <ArticleCategoryNavBarList id={`navbar-list`}>
                  {categoriesNodes.map((category: any, i: any) => (
                    <ArticleCategoryNavBarListItem
                      data-navbar-item={`navbar-item-${i}`}
                      id={`navbar-item-${i}`}
                      key={category.id}
                    >
                      {category && <CategoryNav {...category} />}
                    </ArticleCategoryNavBarListItem>
                  ))}
                </ArticleCategoryNavBarList>
              </ArticleCategoryNavBar>
                  ) }
            {{post && <Hero {...post} />}}
          </>
        ) : (
          <>
            {categoriesNodes && (
              <ArticleCategoryNavBar id={`navbar`} className="no-hero">
                <ArticleCategoryNavBarList id={`navbar-list`}>
                  {categoriesNodes.map((category: any, i: any) => (
                    <ArticleCategoryNavBarListItem
                      data-navbar-item={`navbar-item-${i}`}
                      id={`navbar-item-${i}`}
                      key={category.id}>
                      {category && <CategoryNav {...category} />}
                    </ArticleCategoryNavBarListItem>
                  ))}
                </ArticleCategoryNavBarList>
              </ArticleCategoryNavBar>
            )}
            {{post && <Hero {...post} />}}
          </>
        )*/}
        <div className="container">
          <ArticleWrapper id={`post-content-wrapper`}>
            <ArticleContent data-post-content="post-content" id="post-content">
              {/* <SideBarSocialShare>
                {icons.map((icon) => (
                  <a
                    key={icon.link}
                    href={encodeURI(icon.link)}
                    target="_blank"
                    rel="noopener noreferrer">
                    {icon.svg}
                  </a>
                ))}
              </SideBarSocialShare> */}
              {/* postImage ? (
                <GatsbyImage
                  className="card--img"
                  imgClassName="card--img"
                  image={postImage}
                  alt={postTitle}
                />
              ) : null */}
              {post._rawBody && <BlockText blocks={post._rawBody || []} />}
              <div className="post--footer">
                {post.author && post.author.image && (
                  <div className="post--footer--wrapper">
                    <div className="post--footer--author">
                      <div className="post--footer--author--image">
                        <GatsbyImage
                          image={post.author.image.asset.gatsbyImageData}
                          alt={post.author.name}
                        />
                      </div>
                      <div className="post--footer--author--details">
                        <span>{post.author.name}</span>
                        <span>{post.publishedAt}</span>
                      </div>
                    </div>
                    {/* <div className="post--footer--social">
                      <PostFooterSocialShare>
                        {icons.map((icon) => (
                          <a
                            key={icon.link}
                            href={encodeURI(icon.link)}
                            target="_blank"
                            rel="noopener noreferrer">
                            {icon.svg}
                          </a>
                        ))}
                      </PostFooterSocialShare>
                    </div> */}
                  </div>
                )}
              </div>
            </ArticleContent>
            {/* <Thumbs /> */}
          </ArticleWrapper>
        </div>
        {/* <Disclaimer {...pageDisclaimer} /> */}
      </Layout>
    </div>
  );
};

export default Post;

export const Head = (props: any) => {
  const { data } = props;
  const post = data && data.post;

  const postTitle = post.seoTitle ? post.seoTitle : post.title;

  const postHeroImage =
    post.heroImage &&
    post.heroImage.asset &&
    post.heroImage.asset.gatsbyImageData;

  const postMainImage =
    post.mainImage &&
    post.mainImage.asset &&
    post.mainImage.asset.gatsbyImageData;

  const postImage = postHeroImage || postMainImage;

  const getSocialImage = postImage || defatulSocialImage;

  return (
    <SEO
      title={postTitle}
      description={post.seoDescription}
      meta={[
        {
          property: 'og:image',
          content: getSocialImage,
        },
      ]}
    />
  );
};
