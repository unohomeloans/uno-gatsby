/* eslint-disable prettier/prettier */
// eslint-disable-next-line react-hooks/exhaustive-deps
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { graphql } from 'gatsby';
import styled from 'styled-components';
import BlockText from '../components/BlockText';
import Layout from '../components/Layout';
import Disclaimer from '../components/Disclaimer';
import SEO from '../components/SEO';
import GraphQLErrorList from '../components/GraphQLErrorList';
import pxToRem from '../utils/pxToRem';
import defatulSocialImage from '../images/uno-shared-default.jpg';

export const query = graphql`
  query LenderTemplate($id: String!) {
    lender: sanityLenders(id: { eq: $id }) {
      id
      title
      _rawSubtitle(resolveReferences: { maxDepth: 5 })
      _rawBody(resolveReferences: { maxDepth: 5 })
      seoDescription
      seoTitle
      slug {
        current
      }
      lendersCode
      loanAmount
      propertyValue
      deposit
      loanType
      rateType
    }
    site: sanitySiteSettings(_id: { regex: "/(drafts.|)siteSettings/" }) {
      title
      description
      _rawDisclaimer(resolveReferences: { maxDepth: 10 })
    }
  }
`;

const BASE_API_URL = process.env.GATSBY_PRODUCT_SEARCH_URL;
const PAYMENT_TYPE = 'PrincipalAndInterest';
const DEPOSIT_AMOUNT = 550000;
const PROPERTY_ESTIMATED_VALUE = 1000000;
const PRODUCTS_PER_PROVIDER = 100;
const SIZE = 100;
const LOAN_TYPE = 'Purchase';
const RATE_TYPE = 'FixedRate';

const BASE_API_URL_INVESTMENT = `${BASE_API_URL}?&paymentType=${PAYMENT_TYPE}&size=${SIZE}&propertyEstimatedValue=${PROPERTY_ESTIMATED_VALUE}&depositAmount=${DEPOSIT_AMOUNT}&propertyPurpose=Investment&rateType=${RATE_TYPE}&productsPerProvider=${PRODUCTS_PER_PROVIDER}`;

const LendersHeader = styled.div`
  text-align: center;
  padding: ${(props) => props.theme.spacing.spacingTwo} 0
    ${(props) => props.theme.spacing.spacingThree};
  @media (min-width: 768px) {
    padding: ${(props) => props.theme.spacing.spacingSix} 0
      ${(props) => props.theme.spacing.spacingEight};
  }
`;

const LendersHeaderLogo = styled.img`
  padding: 0 0 ${(props) => props.theme.spacing.spacingOne};
  @media (min-width: 768px) {
    padding: 0 0 ${(props) => props.theme.spacing.spacingTwo};
  }
`;
const LendersTitle = styled.div``;
const LendersSubtitle = styled.div``;

const LenderTitleH1 = styled.h1`
  font-family: ${(props) => props.theme.text.textBlack};
  letter-spacing: ${pxToRem(-0.78)};
`;

const LendersTableWrapper = styled.div`
  background: ${(props) => props.theme.colors.colorAccent};
  padding: ${(props) => props.theme.spacing.spacingThree} 0
    ${(props) => props.theme.spacing.spacingThree};
  @media (min-width: 768px) {
    padding: ${(props) => props.theme.spacing.spacingSix} 0
      ${(props) => props.theme.spacing.spacingEight};
  }
`;

const LendersTable = styled.table`
  background: ${(props) => props.theme.colors.colorWhite};
  box-shadow: 0 0 ${pxToRem(20)} ${pxToRem(4)} rgba(0, 0, 0, 0.03),
    0 ${pxToRem(40)} ${pxToRem(40)} 0 rgba(40, 64, 83, 0.03),
    0 ${pxToRem(16)} ${pxToRem(16)} 0 rgba(40, 64, 83, 0.03),
    0 ${pxToRem(8)} ${pxToRem(8)} 0 rgba(40, 64, 83, 0.03),
    0 ${pxToRem(4)} ${pxToRem(4)} 0 rgba(0, 0, 0, 0.05);
  border-radius: ${pxToRem(12)};

  @media (min-width: 768px) {
    margin: 0 0 ${(props) => props.theme.spacing.spacingEight};
  }
`;

const LendersTableTitle = styled.h2`
  text-align: center;
  font-family: ${(props) => props.theme.text.textBook};
  font-size: ${(props) => props.theme.text.textSix};
`;

const LendersTableHeading = styled.thead`
  font-size: ${(props) => props.theme.text.textOne};
  font-family: ${(props) => props.theme.text.textBook};

  th {
    &:nth-child(2) {
      display: none;
      @media (min-width: 768px) {
        display: table-cell;
      }
    }
  }
`;

const LendersTableBody = styled.tbody`
  tr {
    td {
      padding: ${(props) => props.theme.spacing.spacingOne}
        ${(props) => props.theme.spacing.spacingTwo};
      @media (min-width: 768px) {
        padding: ${(props) => props.theme.spacing.spacingThree}
          ${(props) => props.theme.spacing.spacingTwo};
      }

      &:nth-child(2) {
        display: none;
        @media (min-width: 768px) {
          display: table-cell;
        }
      }
      &:nth-child(3),
      &:nth-child(4) {
        font-size: ${(props) => props.theme.text.textFive};
        @media (min-width: 768px) {
          font-size: ${(props) => props.theme.text.textEight};
        }
      }

      .button {
        font-size: ${(props) => props.theme.text.textThreee};

        span {
          display: none;
          @media (min-width: 768px) {
            display: block;
            padding: 0 0 0 ${pxToRem(5)};
          }
        }
      }
    }
  }
`;

const LendersBody = styled.div`
  padding: ${(props) => props.theme.spacing.spacingTwo} 0
    ${(props) => props.theme.spacing.spacingThree};
  @media (min-width: 768px) {
    padding: ${(props) => props.theme.spacing.spacingSix} 0
      ${(props) => props.theme.spacing.spacingEight};
  }
`;

interface Response {
  data: any;
  products: any;
  productsInvestment: any;
}

const Lender = (props: any) => {
  const { data, errors } = props;
  const lender = data && data.lender;
  const site = (data || {}).site;

  const [response, setResponse] = useState<Response>({
    products: null,
    data: null,
    productsInvestment: null,
  });

  const pageDisclaimer = site;
  const depositAmount = lender.deposit ? lender.deposit : DEPOSIT_AMOUNT;
  const propertyEstimatedValue = lender.propertyValue
    ? lender.propertyValue
    : PROPERTY_ESTIMATED_VALUE;
  const loanType = lender.loanType ? lender.loanType : LOAN_TYPE;
  const rateType = lender.rateType ? lender.rateType : RATE_TYPE;

  useEffect(() => {
    const fetchLender = async () => {
      const [resultOwnerOcuppied, resultInvestment] = await Promise.all([
        axios(
          `${BASE_API_URL}?&paymentType=PrincipalAndInterest&size=${SIZE}&loanType=${LOAN_TYPE}&propertyEstimatedValue=${propertyEstimatedValue}&depositAmount=${depositAmount}&propertyPurpose=OwnerOccupied&rateType=${rateType}&providers=${lender.lendersCode}&productsPerProvider=${PRODUCTS_PER_PROVIDER}&loanRemaining=&loanType=${loanType}`
        ),
        axios(
          `${BASE_API_URL_INVESTMENT}?&paymentType=PrincipalAndInterest&size=${SIZE}&loanType=${LOAN_TYPE}&propertyEstimatedValue=${propertyEstimatedValue}&depositAmount=${depositAmount}&propertyPurpose=Investment&rateType=${rateType}&providers=${lender.lendersCode}&productsPerProvider=${PRODUCTS_PER_PROVIDER}&loanRemaining=&loanType=${loanType}`
        ),
      ]);

      setResponse({
        data: resultOwnerOcuppied.data,
        productsInvestment: resultInvestment.data,
        products: null,
      });
    };

    fetchLender();
  }, [
    lender.lendersCode,
    depositAmount,
    loanType,
    propertyEstimatedValue,
    rateType,
  ]);

  if (errors) {
    return (
      <Layout>
        <GraphQLErrorList errors={errors} />
      </Layout>
    );
  }

  return (
    <div>
      <Layout>
        {lender && (
          <div className="container">
            <LendersHeader>
              <LendersHeaderLogo
                data-lender-logo={`https://cdn.unohomeloans.com.au/lenders/logo/${lender.lendersCode}.svg`}
                width="64"
                alt={`${lender.lendersCode[0]} Logo`}
                src={`https://cdn.unohomeloans.com.au/lenders/logo/${lender.lendersCode}.svg`}
              />
              <LendersTitle>
                <LenderTitleH1 data-lender-title={lender.title}>
                  {lender.title}
                </LenderTitleH1>
              </LendersTitle>
              {lender._rawSubtitle && (
                <LendersSubtitle>
                  {lender._rawSubtitle && (
                    <BlockText blocks={lender._rawSubtitle || []} />
                  )}
                </LendersSubtitle>
              )}
            </LendersHeader>
          </div>
        )}
        {response.data && (
          <LendersTableWrapper>
            <div className="container">
              <LendersTableTitle data-table-title="Home loans for owner occupiers">
                Home loans for owner occupiers
              </LendersTableTitle>
              <LendersTable>
                <LendersTableHeading>
                  <tr>
                    <th>&nbsp;</th>
                    <th data-th="Home loan product" className="u-align-center">
                      Home loan product
                    </th>
                    <th data-th="Interest rate p.a" className="u-align-center">
                      Interest rate p.a
                    </th>
                    <th
                      data-th="Comparison rate p.a*"
                      className="u-align-center">
                      Comparison rate p.a*
                    </th>
                    <th>&nbsp;</th>
                  </tr>
                </LendersTableHeading>
                <LendersTableBody>
                  {response?.data?.products.map((lender: any) => {
                    const {
                      lenderLogoUrl,
                      lenderId,
                      lenderName,
                      productName,
                      productId,
                      rateName,
                      interestRate,
                      rateDiscount,
                      comparisonRate,
                    } = lender;

                    return (
                      <tr data-tr={productId} key={productId}>
                        <td data-td={lenderId}>
                          <img width="40" src={lenderLogoUrl} alt={lenderId} />
                        </td>
                        <td data-td={lenderId}>
                          {lenderName} - {productName} - {rateName}
                        </td>
                        <td>
                          {interestRate}%{' '}
                          {rateDiscount.length > 1
                            ? `${rateDiscount} % - included`
                            : null}{' '}
                        </td>
                        <td data-td={lenderId}>{comparisonRate}%</td>
                      </tr>
                    );
                  })}
                </LendersTableBody>
              </LendersTable>
              <LendersTableTitle data-table-title="Home loans for investors">
                Home loans for investors
              </LendersTableTitle>
              <LendersTable>
                <LendersTableHeading>
                  <tr>
                    <th>&nbsp;</th>
                    <th data-th="Home loan product" className="u-align-center">
                      Home loan product
                    </th>
                    <th data-th="Interest rate p.a" className="u-align-center">
                      Interest rate p.a
                    </th>
                    <th
                      data-th="Comparison rate p.a*"
                      className="u-align-center">
                      Comparison rate p.a*
                    </th>
                    <th>&nbsp;</th>
                  </tr>
                </LendersTableHeading>
                <LendersTableBody>
                  {response.productsInvestment.products.map((lender: any) => {
                    const {
                      lenderLogoUrl,
                      lenderId,
                      lenderName,
                      productName,
                      productId,
                      rateName,
                      interestRate,
                      rateDiscount,
                      comparisonRate,
                    } = lender;

                    return (
                      <tr data-tr={productId} key={productId}>
                        <td data-td={lenderId}>
                          <img width="40" src={lenderLogoUrl} alt={lenderId} />
                        </td>
                        <td data-td={lenderId}>
                          {lenderName} - {productName} - {rateName}
                        </td>
                        <td>
                          {interestRate}%{' '}
                          {rateDiscount.length > 1
                            ? `${rateDiscount} % - included`
                            : null}{' '}
                        </td>
                        <td data-td={lenderId}>{comparisonRate}%</td>
                        {/* <td><a href={url} className="button button--green button--sm btn button--primary btn-details">View <span>Details</span></a></td> */}
                      </tr>
                    );
                  })}
                </LendersTableBody>
              </LendersTable>
              <div className="container">
                <small>
                  Based on: Deposit ${depositAmount}, Property value $
                  {propertyEstimatedValue}, Principal & interest, Purchase or
                  Refinance. WARNING: This comparison rate is true only for the
                  examples given and may not include all fees and charges.
                  Different terms, fees or other loan amounts might result in a
                  different comparison rate. The comparison rate is calculated
                  on the basis of a loan of $150,000 over a term of 25 years.
                </small>
              </div>
            </div>
          </LendersTableWrapper>
        )}
        {lender._rawBody && (
          <div className="container">
            <LendersBody>
              {lender._rawBody && <BlockText blocks={lender._rawBody || []} />}
            </LendersBody>
          </div>
        )}
        <Disclaimer {...pageDisclaimer} />
      </Layout>
    </div>
  );
};

export default Lender;

export const Head = (props: any) => {
  const { data } = props;
  const lender = data && data.lender;

  const lenderTitle = lender.seoTitle ? lender.seoTitle : lender.title;

  return (
    <SEO
      title={lenderTitle}
      description={lender.seoDescription}
      meta={[
        {
          property: 'og:image',
          content: defatulSocialImage,
        },
      ]}
    />
  );
};
