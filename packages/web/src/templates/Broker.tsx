import React, { useContext, useEffect } from 'react';
import styled from 'styled-components';
import { graphql } from 'gatsby';
import GraphQLErrorList from '../components/GraphQLErrorList';
import Layout from '../components/Layout';
import SEO from '../components/SEO';
import PBHero from '../components/PageBuilder/PBHero';
import PBThreeColumnsBasic from '../components/PageBuilder/PBThreeColumnsBasic';
import randomAlphaNumeric from '../utils/randomAlphaNumeric';
import pxToRem from '../utils/pxToRem';

import defatulSocialImage from '../images/uno-shared-default.jpg';
import ModalButton from '../components/ModalButton';
import { BrokerContext } from '../context/BrokerContext';
import { BrokerLinksContext } from '../context/BrokerLinksContext';

export const query = graphql`
  query BrokerTemplateQuery($id: String!) {
    broker: sanityBroker(id: { eq: $id }) {
      ...BrokerInfo
    }
    site: sanitySiteSettings(_id: { regex: "/(drafts.|)siteSettings/" }) {
      title
      description
      disclaimer: _rawDisclaimer(resolveReferences: { maxDepth: 10 })
    }
  }
`;

const Txt2 = styled.p`
  font-family: ${(props) => props.theme.text.textBook};
  font-size: ${(props) => props.theme.text.textTwo};
  line-height: ${pxToRem(21)};
`;

const Txt3 = styled.p`
  font-family: ${(props) => props.theme.text.textBook};
  font-size: ${(props) => props.theme.text.textThree};
  line-height: ${pxToRem(24)};
`;

const BasicCards = (props: any) => {
  const { title, body } = props;
  return (
    <>
      <h3>{title}</h3>
      <Txt3>{body}</Txt3>
    </>
  );
};

const Content = (props: any) => {
  const { data } = props;
  const rawData = data.broker._rawContent.main;
  const heroText = (
    <>
      <h1>{data.broker.content.main.name}</h1>
      <Txt2>{data.broker.content.main.location}</Txt2>
      <Txt2>{data.broker.content.main.email}</Txt2>
      <Txt2>
        <a href={`tel:+${data.broker.content.main.tel}`}>
          {data.broker.content.main.tel}
        </a>
      </Txt2>
      <ModalButton title={data.broker.content.main.ctas.modalCta} />
    </>
  );
  const mainHeroProps = {
    alignment: `right`,
    chooseBackground: `white`,
    mainImage: { ...rawData.image, roundedAccentImage: true },
    text: heroText,
  };
  const reviewHeroProps = {
    alignment: `center`,
    chooseBackground: `white`,
    title: <h1>{rawData?.review?.title}</h1>,
    text: rawData?.review?.content,
  };
  const aboutMeHeroProps = {
    alignment: `center`,
    chooseBackground: `white`,
    title: <h1>About me</h1>,
    text: rawData.bio,
  };
  const bestDealText = (
    <>
      <h1>Are you getting the best deal&#63;</h1>
      <p>
        Take 2 minutes to test your home loan with loanScore<sup>TM</sup>
      </p>
      <a
        href={`https://loanscore.unohomeloans.com.au/existing-loan?uno-referrer=${data.broker.content.main.links.referrer}`}
        rel="noreferrer"
        target="_blank"
        className="button button--md button--height--md button--primary">
        Check your loan with loanScore<sup>TM</sup>
      </a>
    </>
  );
  const bestDealImage = {
    asset: {
      url: 'https://cdn.sanity.io/images/86mn51w1/staging/11726140c2d961a435b8be9c4c06b34c2256ae6d-998x1010.png',
      alt: 'loan score calculation image',
    },
  };
  const bestDealHeroProps = {
    alignment: `left`,
    chooseBackground: `white`,
    mainImage: bestDealImage,
    text: bestDealText,
  };
  const aboutCardsTitle = [
    `Here for the long haul`,
    `Fully supported end to end`,
    `Powered by uno technology`,
  ];
  const aboutCardsBody = [
    `I have partnered with uno to build a long term business relationship to support my customers, you`,
    `UNO runs the entire support and customer care program ensuring consistently delivered, unparalleled service`,
    `UNO's technology is unrivalled in the industry, it helps me deliver better, faster results for you`,
  ];
  const cardContent = aboutCardsTitle
    .map((val, idx) => (
      <BasicCards
        key={randomAlphaNumeric(5 + idx)}
        title={val}
        body={aboutCardsBody[idx]}
      />
    ))
    .map((val, idx) => ({
      alignment: !idx ? `left` : idx == 1 ? `center` : ``,
      _key: randomAlphaNumeric(5 + idx),
      text: val,
    }));
  const aboutServiceProps = {
    chooseBackground: `white`,
    title: `About My Service`,
    cardContent: cardContent,
  };
  return (
    <>
      <PBHero {...mainHeroProps} />
      {rawData?.review ? <PBHero {...reviewHeroProps} /> : null}
      <PBThreeColumnsBasic {...aboutServiceProps} />
      {rawData?.bio ? <PBHero {...aboutMeHeroProps} /> : null}
      <PBHero {...bestDealHeroProps} />
    </>
  );
};

const Broker = (props: any) => {
  const { errors, data } = props;
  const [, setBroker] = useContext(BrokerContext);
  const [links, setLinks] = useContext(BrokerLinksContext);

  useEffect(() => {
    const brokerObject = Object.create(null);
    brokerObject['links'] = data.broker.content.main.links;
    setBroker(data.broker.content.main.name);
    setLinks(data.broker.content.main.links);
    return () => {
      setBroker(Object.create(null));
      setLinks(Object.create(null));
    };
  }, []);

  if (errors) {
    return (
      <Layout>
        <GraphQLErrorList errors={errors} />
      </Layout>
    );
  }

  const site = (data || {}).site;

  if (!site) {
    throw new Error(
      'Missing "Site settings". Open the studio at http://localhost:3333 and add some content to "Site settings" and restart the development server.'
    );
  }

  const page = data.broker;

  return (
    <Layout
      showCalendlyWidget={false}
      showBrokerModalButton={data.broker.content.main.links.calendly != null}
      {...props}>
      <Content data={data} />
      {/* <Disclaimer {...pageDisclaimer} /> */}
      {props.children}
    </Layout>
  );
};

export default Broker;

export const Head = (props: any) => {
  const { data } = props;

  const page = data.broker;

  const pageTitle =
    page.content && page.content.meta && page.content.meta.metaTitle
      ? page.content.meta.metaTitle
      : page.title;
  const pageDescription = page.content.meta.metaDescription;
  // const pageDisclaimer = site;
  const getSocialImage = page.content.meta?.openImage?.asset?.gatsbyImageData
    ? page.content.meta.openImage.asset.gatsbyImageData.images.fallback.src
    : defatulSocialImage;

  return (
    <SEO
      title={pageTitle}
      description={pageDescription}
      meta={[
        {
          property: 'og:image',
          content: getSocialImage,
        },
        {
          name: 'referrer',
          content: 'unsafe-url',
        },
      ]}
    />
  );
};
