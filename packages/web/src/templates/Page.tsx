import React from 'react';
import { graphql } from 'gatsby';
import Layout from '../components/Layout';
import Disclaimer from '../components/Disclaimer';
import Thumbs from '../components/Thumbs';
import SEO from '../components/SEO';
import GraphQLErrorList from '../components/GraphQLErrorList';
import ModalButton from '../components/ModalButton/ModalButton';
import PBHero from '../components/PageBuilder/PBHero';
import PBThreeColumnsCard from '../components/PageBuilder/PBThreeColumnsCard';
import PBThreeColumnsBasic from '../components/PageBuilder/PBThreeColumnsBasic';
import PBTwoColumnsBasic from '../components/PageBuilder/PBTwoColumnsBasic';
import PBYellowBar from '../components/PageBuilder/PBYellowBar';
import PBCustomersFeedback from '../components/PageBuilder/PBCustomersFeedback';
import PBDisclaimer from '../components/PageBuilder/PBDisclaimer';
import PBRatesLenders from '../components/PageBuilder/PBRatesLenders';
import PBTable from '../components/PageBuilder/PBTable';
import PBBestRates from '../components/PageBuilder/PBBestRates';

import defatulSocialImage from '../images/uno-shared-default.jpg';
import PBBrokersList from '../components/PageBuilder/PBBrokersList';
import PBCalculators from '../components/PageBuilder/PBCalculators';
import PBArticle from '../components/PageBuilder/PBArticle';

export const query = graphql`
  query PageTemplateQuery($id: String!) {
    route: sanityRoute(id: { eq: $id }) {
      slug {
        current
      }
      page {
        ...PageInfo
      }
      useSiteTitle
    }
    site: sanitySiteSettings(_id: { regex: "/(drafts.|)siteSettings/" }) {
      title
      description
      disclaimer: _rawDisclaimer(resolveReferences: { maxDepth: 10 })
    }
  }
`;

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
const Page = (props: any) => {
  const { errors, data } = props as any;
  if (errors) {
    return (
      <Layout>
        <GraphQLErrorList errors={errors} />
      </Layout>
    );
  }

  const site = (data || {}).site;

  if (!site) {
    throw new Error(
      'Missing "Site settings". Open the studio at http://localhost:3333 and add some content to "Site settings" and restart the development server.'
    );
  }

  const page = data.page || data.route.page;

  const contentType = (page._rawContent.main.modules || [])
    .filter((c: any) => !c.disabled)
    .map((c: any) => c._type);

  const content = (page._rawContent.main.modules || [])
    .filter((c: any) => !c.disabled)
    .map((c: any, i: any) => {
      let el = null;
      switch (c._type) {
        case 'alert':
          el = (
            <div id={`pb-${c._type}-${i}`} key={c._key}>
              <PBYellowBar key={c._key} {...c} />
            </div>
          );
          break;
        case 'hero':
          el = (
            <div id={`pb-${c._type}-${i}`} key={c._key}>
              <PBHero key={c._key} {...c} />
            </div>
          );
          break;
        case 'threeColumnsCard':
          el = (
            <div id={`pb-${c._type}-${i}`} key={c._key}>
              <PBThreeColumnsCard key={c._key} {...c} />
            </div>
          );
          break;
        case 'threeColumnsBasic':
          el = (
            <div id={`pb-${c._type}-${i}`} key={c._key}>
              <PBThreeColumnsBasic key={c._key} {...c} />
            </div>
          );
          break;
        case 'twoColumnsBasic':
          el = (
            <div id={`pb-${c._type}-${i}`} key={c._key}>
              <PBTwoColumnsBasic key={c._key} {...c} />
            </div>
          );
          break;
        case 'disclaimer':
          el = (
            <div id={`pb-${c._type}-${i}`} key={c._key}>
              <PBDisclaimer key={c._key} {...c} />
            </div>
          );
          break;
        case 'customerFeedback':
          el = (
            <div id={`pb-${c._type}-${i}`} key={c._key}>
              <PBCustomersFeedback key={c._key} {...c} />
            </div>
          );
          break;
        case 'ratesLenders':
          el = (
            <div id={`pb-${c._type}-${i}`} key={c._key}>
              <PBRatesLenders key={c._key} {...c} />
            </div>
          );
          break;
        case 'oldTable':
          el = (
            <div id={`pb-${c._type}-${i}`} key={c._key}>
              <PBTable key={c._key} {...c} />
            </div>
          );
          break;
        case 'bestRates':
          el = (
            <div id={`pb-${c._type}-${i}`} key={c._key}>
              <PBBestRates key={c._key} {...c} />
            </div>
          );
          break;
        case 'brokersList':
          el = (
            <div id={`pb-${c._type}-${i}`} key={c._key}>
              <PBBrokersList key={c._key} {...c} />
            </div>
          );
          break;
        case 'calculator':
          el = (
            <div id={`pb-${c._type}-${i}`} key={c._key}>
              <PBCalculators key={c._key} {...c} />
            </div>
          );
          break;
        case 'article':
          el = (
            <div id={`pb-${c._type}-${i}`} key={c._key}>
              <PBArticle key={c._key} {...c} />
            </div>
          );
          break;
        default:
          el = null;
      }
      return el;
    });

  const isHome = page.content.main.slug.current === 'home';

  return (
    <Layout
      showCalendlyWidget={contentType.includes('brokersList') ? false : true}
      headerBackground={
        contentType.indexOf('hero') == 0
          ? content[0]?.props?.chooseBackground == 'accent' ||
            content[0]?.props?.chooseBackground == 'image'
            ? '#EBF0F4'
            : '#fff'
          : '#fff'
      }
      {...props}>
      {content}
      {!isHome && <Thumbs />}
      {/* <Disclaimer {...pageDisclaimer} /> */}
    </Layout>
  );
};

export default Page;

export const Head = (props: any) => {
  const { data } = props;
  const page = data.page || data.route.page;

  const pageTitle =
    page.content && page.content.meta && page.content.meta.metaTitle
      ? page.content.meta.metaTitle
      : page.title;
  const pageDescription = data.route && page.content.meta.metaDescription;
  const getSocialImage = page.content.meta?.openImage?.asset?.gatsbyImageData
    ? page.content.meta.openImage.asset.gatsbyImageData.images.fallback.src
    : defatulSocialImage;

  const checkForFeedbackComponent = (page._rawContent.main.modules || [])
    .filter((c: any) => !c.disabled)
    .map((c: any) => c._type)
    .filter((val: any) => val == 'customerFeedback');

  return (
    <SEO
      title={pageTitle}
      description={pageDescription}
      meta={[
        {
          property: 'og:image',
          content: getSocialImage,
        },
        {
          name: 'referrer',
          content: 'unsafe-url',
        },
      ]}
      {...(checkForFeedbackComponent.length
        ? {
            script: [
              {
                src: 'https://cdn.productreview.com.au/assets/widgets/loader.js',
              },
            ],
          }
        : Object.create(null))}
    />
  );
};
