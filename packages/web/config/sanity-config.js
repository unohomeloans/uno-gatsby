module.exports = {
  sanity: {
    projectId: process.env.GATSBY_MY_SANITY_PROJECT_ID,
    dataset: process.env.GATSBY_MY_SANITY_DATASET,
    token: process.env.GATSBY_MY_SANITY_TOKEN,
  },
};
