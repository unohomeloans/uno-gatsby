/* eslint-disable @typescript-eslint/no-var-requires */
const dotenv = require('dotenv');
const siteConfig = require('./config/site-config.js');
const sanityConfig = require('./config/sanity-config.js');

dotenv.config({
  path: `.env.${process.env.NODE_ENV || 'development'}`,
});

const pathPrefix = siteConfig.pathPrefix === '/' ? '' : siteConfig.pathPrefix;

const isProd = process.env.NODE_ENV === 'production';

module.exports = {
  pathPrefix: siteConfig.pathPrefix,
  siteMetadata: {
    siteUrl: process.env.GATSBY_SITE_URL + pathPrefix,
    title: siteConfig.title,
    description: siteConfig.description,
    siteLanguage: siteConfig.siteLanguage,
    author: siteConfig.author,
    siteFBappID: siteConfig.siteFBappID,
    facebook: siteConfig.facebook,
    twitter: siteConfig.twitter,
    twitterHandle: siteConfig.userTwitter,
    menuLinks: siteConfig.menuLinks,
    footerLinks: siteConfig.footerLinks,
    socialLinks: siteConfig.socialLinks,
    footerCopyright: siteConfig.footerCopyright,
    siteImage: '',
    linkedin: '',
  },
  plugins: [
    'gatsby-plugin-sass',
    'gatsby-plugin-styled-components',
    'gatsby-plugin-netlify',
    'gatsby-plugin-image',
    'gatsby-plugin-sharp',
    'gatsby-transformer-json',
    'gatsby-transformer-sharp',
    'gatsby-plugin-advanced-sitemap',
    {
      resolve: 'gatsby-plugin-canonical-urls',
      options: {
        siteUrl: process.env.GATSBY_SITE_URL,
      },
    },
    {
      resolve: 'gatsby-plugin-react-svg',
      options: {
        rule: {
          include: /\.svg$/,
        },
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: 'uno Home Loans',
        short_name: 'uno',
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/uno-icon.png`,
      },
    },
    // {
    //   resolve: 'gatsby-plugin-segment-js',
    //   options: {
    //     prodKey: process.env.GATSBY_SEGMENTKEY,
    //     devKey: process.env.GATSBY_SEGMENTKEY,
    //     trackPage: false,
    //     loadOnRender: false,
    //   },
    // },
    {
      resolve: `gatsby-plugin-google-fonts`,
      options: {
        fonts: [
          `Montserrat\:400,500,700,900`, // you can also specify font weights and styles
        ],
        display: 'swap',
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'data',
        path: `${__dirname}/src/data/`,
        ignore: [`**/\.*`],
      },
    },
    {
      resolve: 'gatsby-source-sanity',
      options: {
        ...sanityConfig.sanity,
        watchMode: !isProd,
        overlayDrafts: !isProd,
      },
    },
    {
      resolve: 'gatsby-plugin-robots-txt',
      options: {
        host: process.env.GATSBY_SITE_URL,
        sitemap: `${process.env.GATSBY_SITE_URL}/sitemap.xml`,
        env: {
          development: {
            policy: [{ userAgent: '*', disallow: '/' }],
          },
          staging: {
            policy: [{ userAgent: '*', disallow: '/' }],
          },
          production: {
            policy: [{ userAgent: '*', allow: '/' }],
          },
        },
      },
    },
  ],
};
